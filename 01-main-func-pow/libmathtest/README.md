# Uso libm (math)



## Ejemplo 1:

Requiere la biblioteca, usa una variable y el compilador no puede reemplazar el código.


```
 gcc need-math.c 
/tmp/ccj38iR9.o: In function `main':
need-math.c:(.text+0x2f): undefined reference to `pow'
collect2: error: ld returned 1 exit status

 gcc need-math.c -lm
 
 ldd a.out 
	linux-vdso.so.1 (0x00007fffc59e5000)
	libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f75bf84c000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f75bf45b000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f75bfdec000)

 gcc -S need-math.c

cat need-math.s 
	.file	"need-math.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movsd	.LC0(%rip), %xmm0
	movsd	%xmm0, -16(%rbp)
	movsd	-16(%rbp), %xmm0
	movq	.LC1(%rip), %rax
	movapd	%xmm0, %xmm1
	movq	%rax, -24(%rbp)
	movsd	-24(%rbp), %xmm0
	call	pow@PLT              <--------------- LLAMADA
	movq	%xmm0, %rax
	movq	%rax, -8(%rbp)
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
	.align 8
.LC0:
	.long	0
	.long	1074266112
	.align 8
.LC1:
	.long	0
	.long	1073741824
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
```


## Ejemplo 2:

```
 gcc no-need-math.c

 ldd a.out 
	linux-vdso.so.1 (0x00007ffe1b381000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f12c5eb9000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f12c64ac000)


 gcc -S no-need-math.c

 cat no-need-math.s 
	.file	"no-need-math.c"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movsd	.LC0(%rip), %xmm0
	movsd	%xmm0, -8(%rbp)
	movl	$0, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.section	.rodata
	.align 8   
.LC0:
	.long	0
	.long	1075838976 <----------- ya calculo el valor
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
```