/*
 * Power test program
 *
 */

#include <stdio.h>
#include "ipower.h"

int main() 
{
  printf("%ld %ld %ld %ld\n",
          ipower(2,2), ipower(2,5), ipower(2,10), ipower(2,15));
  return 0;
}

/*** EOF ***/
