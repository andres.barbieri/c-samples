# Aprendiendo compilaci�n por pasos de un programa "C"

Ejemplo de programa "C" que muestra compilaci�n por pasos y dependencias. En este caso se tienen 2 (tres) implementaciones posibles de la funci�n potencia entera en `p1.c` y `p2.c`. Sirven para ilustrar como se puede cambiar de una a otra y como una implementaci�n puede estar basada en una biblioteca, en este caso `p2.c` depende de `libmath` o `libm`, parte del lenguaje "C".

### Pre-requisitos

Compilador GCC, `make`, `sleep`, `file`, `objdump`, `cat`, `ldd` ...
B�sicamente un entorno de desarrollo adem�s de la herramienta `dot` para generar gr�ficos y la herramienta de chequeos est�ticos `splint`.

## Pruebas
 
Probar ejecutando `make` y ver como se contruye. En este caso ver como se indica que se utiliza la biblioteca de funciones matem�ticas `libm`.

```
 make clean
 make 
 ldd main
 ...
 ./main; echo $?
```

### Cambios para cambiar implementaci�n

Ver de usar la implementaci�n de `p1.c` o `p2.c`. Qu� cambios se deber�an hacer ? Se debe recompilar todo el c�digo ?

## Otras cuestiones �tiles

El ejemplo tambi�n se acompa�a de el chequeo de dependencias de inclusiones con el flag `-MM`y el gr�fico `graph.png` que muestra como son generado a partir de la herramienta `dot`. Para el chequeo de dependencias y chequeo est�tico se puede ejecutar:

```
 make showdeps
 make check
```

Para ejecutar tambi�n se puede correr:

```
 make test
```

### Dir libmathtest

Ejemplo de como se referencia o no la libmath de acuerdo al c�digo desde el compilador.