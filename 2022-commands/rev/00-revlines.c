// Like rev(1)

#include <stdio.h>

#define MAX_BUF   4096

#define CR    '\n'

int main() 
{
  char buf[MAX_BUF];
  int  i   = 0;
  int  fin = 0;
  
  buf[i] = getchar();
  while ( !(fin) ) {
    if ( (buf[i] == CR) || (buf[i] == EOF) )
      {
	// show line
	for(int j=(i-1);j>=0;j--) { putchar(buf[j]); }
        if (buf[i] != EOF) { putchar('\n'); }
	else               { fin = 1; }
	i = 0;
      }
    else if (i< MAX_BUF-1) 
      {
	i++;
      }
    if (i < MAX_BUF)  buf[i] = getchar();
    else buf[MAX_BUF-1] = getchar();
  } 
  return (0);
}
      
/*** EOF ***/

    
