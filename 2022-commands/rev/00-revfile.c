#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int usage(char *cmd)
{
  fprintf(stderr, " usage %s INPUT-FILE\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  FILE *fp;
  char *fname;
  long int fpos;
  int  c;
  long int i = 0;
  
  if (argc<2) usage(argv[0]);

  fname = argv[1]; 
  if ((fp = fopen(fname,"r")) == NULL)
    {
      perror("at opening file");
      exit(2);
    }
  while (fseek(fp,-(i+1),SEEK_END)==0)
      {
	if ((c = fgetc(fp)) !=EOF) putchar(c);
	i++;
      }
     
    fclose(fp);
    return 0;
}
  
     




