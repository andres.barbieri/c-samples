// Like seq

#include <stdio.h>
#include <stdlib.h>

#define SEP '\n'

static int usage(char *cmd)
{
  fprintf(stderr, "Usage: %s [INCREMENT] FIRST LAST\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  long int inc = 1;
  long int first, last;
  
  if (argc<3) usage(argv[0]);
  if (argc==3)
    {
      first = atol(argv[1]);
      last  = atol(argv[2]);
    }
  else if (argc==4)
    {
      inc   = atoi(argv[1]);
      first = atol(argv[2]);
      last  = atol(argv[3]);
    }
  else
    {
      usage(argv[0]);
    }
  for(long int i=first;i<=last;i+=inc)
    {
      fprintf(stdout,"%ld%c",i,SEP);
    }
return 0;
}
  
     




