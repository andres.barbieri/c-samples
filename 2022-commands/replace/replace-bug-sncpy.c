#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BLEN 1024

static int usage(char *cmd)
{
  printf("usage %s input set1 set2\n",cmd);
  exit(1);
}

static  int replace(const char *line, const char *set1, const char *set2, char **newline)
{
  char *r;
  int   i,j;
  
  if (r = strstr(line,set1))
    {
      (*newline) = malloc(strlen(line)-strlen(set1)+strlen(set2)+1);
      j = 0;
      for(i=0;i<(r-line);i++)
	{
	  (*newline)[j] = line[i];
	  j++;
	}
      for(i=0;i<strlen(set2);i++)
	{
	  (*newline)[j] = set2[i];
	  j++;
	}
      for(i=(r-line)+strlen(set1);i<strlen(line);i++)
	{
	  (*newline)[j] = line[i];
	  j++;
	}
      (*newline)[j] = '\0';      
      return 0;
    }
  else
    {
      (*newline) = malloc(strlen(line)+1);
      strcpy((*newline),line);
      return 1;
    }
}

static  int replace_all(const char *line, const char *set1, const char *set2, char **newline)
{
  char *r;
  int   i,j;
  int   n = 0;
  char buf[BLEN]; // = {0};
  const char *aux = line;

  j = 0;
  while (r = strstr(aux,set1))
    {
      n++;
      for(i=0;i<(r-aux);i++)
	{
	  buf[j] = aux[i];
	  j++;
	}
      for(i=0;i<strlen(set2);i++)
	{
	  buf[j] = set2[i];
	  j++;
	}
      aux = &(aux[(r-aux)+strlen(set1)]);
    }
  for(i=0;i<strlen(aux);i++)
    {
      buf[j] = aux[i];
      j++;
    }
  buf[j] = '\0';
  i = strlen(line) + (n*(strlen(set2)-strlen(set1))) + 1;
  (*newline) = malloc(i*sizeof(char));
  // BUG strncpy ??
  /***
   ./a.out ALGO11sdsd1111sfsfsF111SDSFSfsf111safsafsa 11 @@
 a.out: malloc.c:2379: sysmalloc: Assertion `(old_top == initial_top (av) && old_size == 0) || ((unsigned long) (old_size) >= MINSIZE && prev_inuse (old_top) && ((unsigned long) old_end & (pagesize - 1)) == 0)' failed.
  ***/
  strncpy((*newline), buf, BLEN);
  return n;
}

int main(int argc, char *argv[])
{
  int i;
  char *line, *set1, *set2, *r, *newline;
  
  if (argc<4) usage(argv[0]);
  line  = argv[1];
  set1  = argv[2];
  set2  = argv[3];
  //replace(line,set1,set2,&newline);
  replace_all(line,set1,set2,&newline);
  printf("%s\n%s\n",line,newline);
  free(newline);
  return 0;
}


  




