#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BLEN 1024

static int usage(char *cmd)
{
  printf("usage %s INPUT-LINE EXP1 EXP2\n",cmd);
  exit(1);
}

static  int replace(const char *line, const char *exp1, const char *exp2, char **newline)
{
  char *r;
  int   i,j;
  
  if (r = strstr(line,exp1))
    {
      (*newline) = malloc(strlen(line)-strlen(exp1)+strlen(exp2)+1);
      j = 0;
      for(i=0;i<(r-line);i++)
	{
	  (*newline)[j] = line[i];
	  j++;
	}
      for(i=0;i<strlen(exp2);i++)
	{
	  (*newline)[j] = exp2[i];
	  j++;
	}
      for(i=(r-line)+strlen(exp1);i<strlen(line);i++)
	{
	  (*newline)[j] = line[i];
	  j++;
	}
      (*newline)[j] = '\0';      
      return 0;
    }
  else
    {
      (*newline) = malloc(strlen(line)+1);
      strcpy((*newline),line);
      return 1;
    }
}

static  int replace_all(const char *line, const char *exp1, const char *exp2, char **newline)
{
  char *r;
  int   i,j;
  int   n = 0;
  char buf[BLEN]; // = {0};
  const char *aux = line;

  j = 0;
  while (r = strstr(aux,exp1))
    {
      n++;
      for(i=0;i<(r-aux);i++)
	{
	  buf[j] = aux[i];
	  j++;
	}
      for(i=0;i<strlen(exp2);i++)
	{
	  buf[j] = exp2[i];
	  j++;
	}
      aux = &(aux[(r-aux)+strlen(exp1)]);
    }
  for(i=0;i<strlen(aux);i++)
    {
      buf[j] = aux[i];
      j++;
    }
  buf[j] = '\0';
  i = strlen(line) + (n*(strlen(exp2)-strlen(exp1))) + 1;
  (*newline) = malloc(i*sizeof(char));
  strcpy((*newline), buf);
  return n;
}

int main(int argc, char *argv[])
{
  int i;
  char *line, *exp1, *exp2, *r, *newline;
  
  if (argc<4) usage(argv[0]);
  line  = argv[1];
  exp1  = argv[2];
  exp2  = argv[3];
  //replace(line,exp1,exp2,&newline);
  replace_all(line,exp1,exp2,&newline);
  printf("%s\n%s\n",line,newline);
  free(newline);
  return 0;
}


  




