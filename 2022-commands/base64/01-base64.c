#include <stdio.h>
#include <inttypes.h>

#define LINE 76

int main()
{
  int count = 0;
  int c[3];
  int i;
  int end = 0;
  uint32_t n = 0;
  uint8_t n0, n1, n2, n3;
  const char base64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  
  while (!end)
    {
      i = 0;
      while ( ((c[i]=getchar())!=EOF) && (i<2) )
	{
	  i++;
	}
      if (c[i]!=EOF)  
	{
	  fprintf(stderr,"[%c%c%c]\n",c[0],c[1],c[2]);
	  /* three 8-bit (ASCII) characters, "c[0],c[1],c[2]," become one 24-bit number, "n" */
	  n = ((uint32_t)c[0]) << 16; 
          n += ((uint32_t)c[1]) << 8;
          n += c[2];
	  /* this 24-bit number, "n",  gets separated into four 6-bit numbers, "n0,n1,n2,n3" */
	  n0 = (uint8_t)(n >> 18) & 63;
	  n1 = (uint8_t)(n >> 12) & 63;
	  n2 = (uint8_t)(n >> 6) & 63;
	  n3 = (uint8_t)n & 63;
          printf("%c%c%c%c",base64chars[n0],base64chars[n1],base64chars[n2],base64chars[n3]);
	  // Line newline
	  count=count+4; if (count==LINE) { printf("\n");count=0;};
	}
      else
	{
	  fprintf(stderr,"[");
	  for(i=0;c[i]!=EOF;i++)
	    {
	      fprintf(stderr,"%c",c[i]);
	    }
	  fprintf(stderr,"]\n");
	  if ((c[0]!=EOF)&&(c[1]==EOF))
	    {
	      n = ((uint32_t)c[0]) << 16; 
	      //n += ((uint32_t)c[1]) << 8;
              //n += c[2];
	      n0 = (uint8_t)(n >> 18) & 63;
  	      n1 = (uint8_t)(n >> 12) & 63;
	      //n2 = (uint8_t)(n >> 6) & 63;
	      //n3 = (uint8_t)n & 63;
	      //printf("%c%c%c%c",base64chars[n0],base64chars[n1],base64chars[n2]base64chars[n3]);
	      // '=' stuff char
	      printf("%c%c%c%c",base64chars[n0],base64chars[n1],'=','=');
	      // Line newline
	      count+=4; if (count==LINE) { printf("\n");count=0;};
	    }
	  else if ((c[0]!=EOF)&&(c[1]!=EOF))
	    {
	      n = ((uint32_t)c[0]) << 16; 
	      n += ((uint32_t)c[1]) << 8;
              //n += c[2];
	      n0 = (uint8_t)(n >> 18) & 63;
  	      n1 = (uint8_t)(n >> 12) & 63;
	      n2 = (uint8_t)(n >> 6) & 63;
	      //n3 = (uint8_t)n & 63;
	      // '=' stuff char
	      printf("%c%c%c%c",base64chars[n0],base64chars[n1],base64chars[n2],'=');
	      // Line newline
	      count+=4; if (count==LINE) { printf("\n");count=0;};
	    }	    
	  end = 1;
	}
      i = 0;
    }
  printf("\n");
  return 0;
}

