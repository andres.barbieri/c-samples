// Like head

#include <stdio.h>

#define CR    '\n'

int main() 
{
     char c;
     int  n_lines = 0;
     int  top     = 10;
     
     c = getchar();
     while ((c != EOF)&&(n_lines < top)) {
       if ( (c == CR) || (c == EOF) ) {
	 n_lines++;
       } 
       if (c != EOF) {
	 putchar(c);
       }
       c = getchar();
     }
     return (0);
}
      
/*** EOF ***/

    
