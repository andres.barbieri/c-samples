// Like head -cN|-N 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define CR     '\n'
#define DEFTOP  10

static int usage(char *cmd)
{
  fprintf(stderr, "Usage: %s [-LINES|-cCHARS] \n",cmd);
  exit(1);
}

int main(int argc, char *argv[]) 
{
     char c;
     int  n         = 0;
     int  top       = DEFTOP;
     int  charcount = 0;

     
     if (argc > 1)
       {
	 top = atoi(argv[1]);
       }
     
     c = getchar();
     while ((c != EOF)&&(n < top)) {
	 n++;
	 if (c != EOF) {
	   putchar(c);
	 }
	 c = getchar();
     }
     return (0);
}
      
/*** EOF ***/

    
