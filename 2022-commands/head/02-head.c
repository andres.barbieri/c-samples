// Like head -cN|-N 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define CR     '\n'
#define DEFTOP  10

static int usage(char *cmd)
{
  fprintf(stderr, "Usage: %s [-LINES|-cCHARS] \n",cmd);
  exit(1);
}

int main(int argc, char *argv[]) 
{
     char c;
     int  n         = 0;
     int  top       = DEFTOP;
     int  charcount = 0;

     
     if (argc > 1)
       {
	 if ( (!strncmp(argv[1],"-c",2)) && (strlen(argv[1])>2) && (atoi(&(argv[1][2]))>=0) )
	   {
	     top = atoi(&(argv[1][2]));
	     charcount = 1;
	   }
         else if ( (!strncmp(argv[1],"-",1)) && (atoi(argv[1])<=0)  )
	   {
	     top = -atoi(argv[1]);
	   }
       }
     
     c = getchar();
     while ((c != EOF)&&(n < top)) {
       if ( (!charcount) && ((c == CR) || (c == EOF) ) ) {
	 n++;
       }
       if (charcount) {
	 n++;
       }
       if (c != EOF) {
	 putchar(c);
       }
       c = getchar();
     }
     return (0);
}
      
/*** EOF ***/

    
