// Like tr -d|--delete SET1 SET2 < input (Basic, no expressions are used as SETs)
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DELOPT          "-d"
#define DELOPTSZ        2

static int usage(char *cmd)
{
  fprintf(stderr," %s : missing operand\n"
	  " usage %s [-d|--delete] CHAR-SET1 [CHAR-SET2]\n",cmd,cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  int c;
  int new_c;
  int i;
  char *set1, *set2;
  int del;
  
  if (argc<3) usage(argv[0]);

  if (strncmp(argv[1],DELOPT,DELOPTSZ))
    {
      set1 = argv[1];
      set2 = argv[2];
    }
  else
    {
      set1 = argv[2];
      set2 = NULL;
    }
       
  while ((c = getchar()) != EOF)
    {
      i = 0;
      new_c = c;
      del = 0;
      while (set1[i])
      {	
	if ((set1[i]==c)&&(set2))
	  {
	    if (i<strlen(set2))
	      {
		new_c = set2[i];
	      }
	    else
	      {
		new_c = set2[strlen(set2)-1];
	      }
	  }
	else if (set1[i]==c) // && set2=NULL
	  {
	    del = 1; 
	  }
	i++;
      }
      if (!del) putchar(new_c);
    }
return 0;
}
  
     




