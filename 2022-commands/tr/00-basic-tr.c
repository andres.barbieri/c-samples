// Like tr SET1 SET2 < input (Basic, no expressions are used as SETs)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int usage(char *cmd)
{
  fprintf(stderr, "usage %s CHAR-SET1 CHAR-SET2\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  int c;
  int new_c;
  int i;
  char *set1, *set2;
  
  if (argc<3) usage(argv[0]);
  set1 = argv[1];
  set2 = argv[2];

  while ((c = getchar()) != EOF)
    {
      i = 0;
      new_c = c;
      while (set1[i])
      {	
	if (set1[i]==c)
	  {
	    if (i<strlen(set2))
	      {
		new_c = set2[i];
	      }
	    else
	      {
		new_c = set2[strlen(set2)-1];
	      }
	  }
	i++;
      }
      putchar(new_c);
    }
return 0;
}
  
     




