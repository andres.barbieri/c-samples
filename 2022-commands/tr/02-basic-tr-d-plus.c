// Like tr -d|--delete SET1 SET2 < input (Basic, no expressions are used as SETs)
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DELOPT_LONG     "--delete"
#define DELOPT_LONG_SZ  20

#define DELOPT          "-d"
#define DELOPTSZ        20

#define HLPOPT_LONG     "--help"
#define HLPOPT_LONG_SZ  20

#define HLPOPT          "-h"
#define HLPOPTSZ        20

static int help(char *cmd)
{
  fprintf(stdout,"usage %s [-d|--delete] CHAR-SET1 [CHAR-SET2]\n",cmd);
  fprintf(stdout,"Translate or delete characters from standard input,\n"
                  "writing to standard output.\n"
	          "Translation occurs if -d is not given and both SET1 and SET2 appear.\n"
                  "SET2 is extended to length of SET1 by repeating its last character\n"
	          "as necessary.  Excess characters of SET2 are ignored.\n"
                  "-d|--delete option: delete chars found in CHAR-SET1, CHAR-SET2 is ignored\n");
  exit(0);
}

static int usage(char *cmd)
{
  fprintf(stderr," %s : missing operand\n"
 	         " Try '%s --help' for more information.\n",cmd,cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  int c;
  int new_c;
  int i;
  char *set1, *set2;
  int del;
  
  if (  (argc>=2)&&
        ((!strncmp(argv[1],HLPOPT,HLPOPTSZ))||(!strncmp(argv[1],HLPOPT_LONG,HLPOPT_LONG_SZ))) ) help(argv[0]);

  if (argc<3) usage(argv[0]);
      
  if ( (strncmp(argv[1],DELOPT,DELOPTSZ))&&(strncmp(argv[1],DELOPT_LONG,DELOPT_LONG_SZ)) )
    {
      set1 = argv[1];
      set2 = argv[2];
    }
  else
    {
      set1 = argv[2];
      set2 = NULL;
    }
       
  while ((c = getchar()) != EOF)
    {
      i = 0;
      new_c = c;
      del = 0;
      while (set1[i])
      {	
	if ((set1[i]==c)&&(set2))
	  {
	    if (i<strlen(set2))
	      {
		new_c = set2[i];
	      }
	    else
	      {
		new_c = set2[strlen(set2)-1];
	      }
	  }
	else if (set1[i]==c) // && set2=NULL
	  {
	    del = 1; 
	  }
	i++;
      }
      if (!del) putchar(new_c);
    }
return 0;
}
  
     




