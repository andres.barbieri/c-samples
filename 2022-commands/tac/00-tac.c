#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 2048

static int usage(char *cmd)
{
  fprintf(stderr, " usage %s INPUT-FILE\n",cmd);
  exit(1);
}

static void show_line_reverse(char *line, int len, int newline)
{
  for(int k=len-1;k>=0;k--)
    {
      putchar(line[k]);
    }
  if ((len)&&(newline)) putchar('\n');
}
  
int main(int argc, char *argv[])
{
  FILE *fp;
  char *fname;
  int  c;
  long int i = 0;
  int      j = 0;
  char line[LEN];
  
  if (argc<2) usage(argv[0]);

  fname = argv[1]; 
  if ((fp = fopen(fname,"r")) == NULL)
    {
      perror("at opening file");
      exit(2);
    }
  while ((fseek(fp,-(i+1),SEEK_END)==0)&&((c = fgetc(fp)) !=EOF))
      {
	if (c!='\n')
	  {
	    if (j<LEN) { line[j] = c; j++; }
	  }
	else
	  {
	    show_line_reverse(line, j, 1);
	    j=0;
	  }
	i++;
      }
  int newline = (fseek(fp,-1,SEEK_END)==0)&&((c = fgetc(fp)) !=EOF) && (c=='\n');
  show_line_reverse(line, j , newline);
  fclose(fp);
  return 0;
}
  
     




