#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef TOP
#define TOP 4
#endif

static int usage(char *cmd)
{
  fprintf(stderr, " usage %s INPUT-FILE\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  FILE *fp;
  char *fname;
  int  c;
  int  lines = 0;
  int  i = 0;
  int  r;
  
  if (argc<2) usage(argv[0]);

  fname = argv[1]; 
  if ((fp = fopen(fname,"r")) == NULL)
    {
      perror("at opening file");
      exit(2);
    }
  // Try to skip last CR if there are
  if ( (fseek(fp,-1,SEEK_END)==0)&&((c = fgetc(fp)) !=EOF)&&(c != '\n') )
    {
      i++;
    }
  // Count lines from ending
  while ( ((r=fseek(fp,-(i+2),SEEK_END))==0) &&((c = fgetc(fp)) !=EOF)&&(lines<TOP) )
      {
	if (c=='\n')
	  {
	    lines++;
	  }
	i++;
      }

  // if !BOF discard last char read else set at begining 
  if (!r) fgetc(fp);
  else fseek(fp,0,SEEK_SET);
  while((c = fgetc(fp)) !=EOF) { putchar(c); };
  fclose(fp);
  return 0;
}
  
     




