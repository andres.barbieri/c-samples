// Like grep -nF pattern < input

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 1024

static int usage(char *cmd)
{
  fprintf(stderr, "usage %s CHAR-PATTERN\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  char buf[LEN] = {0};
  int found  = 0;
  int lindex = 0;
  char *pattern;

  if (argc<2) usage(argv[0]);
  pattern = argv[1];

  //while (fgets(buf,LEN,stdin)!=NULL)
  while (fgets(buf,LEN,stdin))
    {
      lindex++;
      //if (strstr(buf,pattern)!=NULL)
      if (strstr(buf,pattern))
	{
	  printf("%d: %s",lindex, buf);
	  found = 1;
	}
    }
  return !found;
}
