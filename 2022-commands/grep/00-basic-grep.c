// Like grep -Fq pattern < input

#include <stdio.h>
#include <stdlib.h>

static int usage(char *cmd)
{
  fprintf(stderr, "usage %s CHAR-PATTERN\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  int index = 0;
  int found = 0;
  int eof   = 0;
  int c;
  char *pattern;
  
  if (argc<2) usage(argv[0]);
  pattern = argv[1];

  while ((!found)&&(!eof)&&(pattern[index]!=0))
    {
      c = getchar();
      if ((c!=EOF)&&(c==pattern[index]))
	{
	  index++;
	}
      else if ((c!=EOF)&&(c!=pattern[index]))
	{
	  index=0;
	}
      else
	{
	  eof = 1;
	}
      if (pattern[index]==0) found =1;
    }
  return !found;
}
  
     




