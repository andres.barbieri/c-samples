// Like grep -F -m1 pattern < input

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 1024

static int usage(char *cmd)
{
  fprintf(stderr, "usage %s CHAR-PATTERN\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  char buf[LEN] = {0};
  char *pattern;
  
  if (argc<2) usage(argv[0]);
  pattern = argv[1];

  while (fgets(buf,LEN,stdin))
    if (strstr(buf,pattern)) return !printf("%s",buf);
  return 1;
}
  
     




