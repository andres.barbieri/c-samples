// Like grep -F

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LEN 1024

static int usage(char *cmd)
{
  fprintf(stderr, "usage %s CHAR-PATTERN\n",cmd);
  exit(1);
}

int main(int argc, char *argv[])
{
  int index = 0;
  int found = 0;
  int eof   = 0;
  int c;
  char *pattern;
  char line[LEN] = {0};
  int  lindex = 0;
  int  l = 1;
  
  if (argc<2) usage(argv[0]);
  pattern = argv[1];

  while ((!found)&&(!eof)&&(pattern[index]!=0))
    {
      c = getchar();
      if ((c!=EOF)&&(c==pattern[index]))
	{
	  index++;
	  line[lindex] = c;	  
	  lindex++;
	}
      else if ((c!=EOF)&&(c!=pattern[index]))
	{
	  index=0;
	  line[lindex] = c;	  
	  lindex++;
	}
      else
	{
	  eof = 1;
	}
      if (pattern[index]==0) found = 1; 
      if ((c!=EOF) && (c=='\n')) {lindex = 0; bzero(line,LEN);l++;}
    }
  if (found) printf("%d %s\n",l,line);
  return !found;
}
  
     




