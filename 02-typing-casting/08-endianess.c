#include <stdio.h>
//#include <stdint.h>
#include <arpa/inet.h>
#include <inttypes.h> // includes stdint.h PRN SCN

//#define _BSD_SOURCE             /* See feature_test_macros(7) */
#include <endian.h>

int is_big_endian(void)
{
  return 1==htonl(1);
}   

int is_little_endian(void)
{
  int num = 1;
  return (*(char *)&num == 1);
}

// Swap endian (big to little) or (little to big)
// StackOverflow sample
uint32_t swap_endianess(uint32_t num)
{
  // (HIGH) WW XX YY ZZ (LOW)
  uint32_t b0,b1,b2,b3;
  uint32_t res;
  b0 = (num & 0x000000ff) << 24u; // ZZ -> WW (L -> H)
  b1 = (num & 0x0000ff00) << 8u;  // YY -> XX 
  b2 = (num & 0x00ff0000) >> 8u;  // XX -> YY
  b3 = (num & 0xff000000) >> 24u; // WW -> ZZ (H -> L)
  res = b0 | b1 | b2 | b3; // compose
  return res;
}

int main()
{
  uint16_t y = 10;
  uint32_t x = 10;  
  uint64_t z = 10;
  printf("L=%d B=%d\n",is_little_endian(),is_big_endian());
  printf("y == %" PRIu16 " == %" PRIu16 "\n",y,htons(y)); // htobe16
  printf("x == %" PRIu32 " == %" PRIu32 "\n",x,htonl(x));  
  printf("x == %" PRIu32 " == %" PRIu32 "\n",x,htobe32(x));  
  printf("x == %" PRIu32 " == %" PRIu32 "\n",x,swap_endianess(x));  
  printf("z == %" PRIu64 " == %" PRIu64 "\n",z,htobe64(z));
  printf("z == %" PRIu64 " == %" PRIu64 "\n",z,be64toh(htobe64(z)));
  return 0;
} 


