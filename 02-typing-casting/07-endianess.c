/** 
 * Little Endian:     
 *
 *  adj.     Describes a computer architecture in which,
 *  within a given 16- or 32-bit word, bytes at lower addresses have lower
 *  significance (the word is stored `little-end-first'). The PDP-11 and
 *  VAX families of computers and Intel microprocessors and a lot of
 *  communications and networking hardware are little-endian. See
 *  big-endian, middle-endian, NUXI problem. The term is sometimes used to
 *  describe the ordering of units other than bytes; most often, bits
 *  within a byte.
 *
 *  Big Endian:
 *
 *  Storing the most significant byte in the lowest memory address, which
 *  is the address of the data. Since TCP defines the byte ordering for
 *  network data, end-nodes must call a processor-specific convert utility
 *  (which would do nothing if the machine's native byte-ordering is the
 *  same as TCP's) that acts on the TCP and IP header information only. In
 *  a TCP/IP packet, the first transmitted data is the most significant
 *  byte.
 *  
 *  Intel is little endian,
 *
 *  Most UNIXes (for example, all System V) and the Internet are Big
 *  Endian. Motorola 680x0 microprocessors (and therefore Macintoshes),
 *  Hewlett-Packard PA-RISC, and Sun SuperSPARC processors are Big Endian.
 *  The Silicon Graphics MIPS and IBM/Motorola PowerPC processors are both.
 *
 *  Para 2017 debido a la hegemon'ia de Intel a pasado casi todo a little 
 *  endian aunque para varios procesadores es configurable, esta condici'on se 
 *  la conoce como Bi-endian.
 *
 *  The value 0x0100 is stored:
 *
 *       Big-Endian                    Little-Endian
 *       ----------                    ------------- 
 *                       LOW    (0x0000)
 *     |           |
 *     +-----------+                   --------------
 *   0 |  0x01     |                      0x00
 *     +-----------+                   --------------
 *   1 |  0x00     |                      0x01
 *     +-----------+                   -------------- 
 *     |           |
 *                       HIGH   (0xFFFE)       
 *
 *  The value 0x0001 is stored:
 *
 *       Big-Endian                    Little-Endian
 *       ----------                    ------------- 
 *                       LOW    (0x0000)
 *     |           |
 *     +-----------+                   --------------
 *   0 |  0x00     |                      0x01
 *     +-----------+                   --------------
 *   1 |  0x01     |                      0x00
 *     +-----------+                   -------------- 
 *     |           |
 *                       HIGH   (0xFFFE)    
 */

/*******************

   uint32_t     i = 1;

   unsigned int i = 1;
   char *c = (char*)&i;

       higher memory
          ----->
    +----+----+----+----+
    |0x01|0x00|0x00|0x00|
    +----+----+----+----+
    ^
    |
   &c

    (char*)(*x) == 1

If it were big endian, it  would be:

    +----+----+----+----+
    |0x00|0x00|0x00|0x01|
    +----+----+----+----+
    ^
    |
   &c

    (char*)(*x) == 0


Another way:

    union {
        uint32_t i;
        char     c[4];
    } e = { 0x01000000 };

    return e.c[0]; is big_endian


*****************/

#include <stdio.h>
#include <stdint.h>

int is_little_endian(void)
{
  uint32_t     i = 1;i = 1; // 00 00 00 01 || 01 00 00 00
  char *c = (char*)&i;
  return (*c); 
}   

int main()
{
  if(is_little_endian())
    {
      printf("Little Endian Architecture\n");
    }
  else
    {
      printf("Big Endian Architecture\n");
    }
  return (0);
}

/*** EOF ***/
