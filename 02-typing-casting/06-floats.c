/*
 * Programa para entender el formato IEEE 754 
 * floats en lenguaje "C".
 */

/**************************************************
 * Compiling:
 * gcc -DBIG_ENDIAN_ARCH -Wall -o floats floats.c
 * gcc -Wall -o floats floats.c 
 **************************************************/ 

/* __STDC_IEC_559__                        */
/* Unsigned raw binary 0..2^(n)-1          */
/* Singed   Ca2 binary -2^(n-1)..2^(n-1)-1 */

/* Float       IEEE 754 single precision   */
/* Double      IEEE 754 double precision   */
/* Long Double IEEE 754 double precision   */

/*
 * 
 * Single
 * ------
 * 1  bit  sign     (S)
 * 8  bits exp      (E) EXC=128-1=127 excess
 * 23 bits mantissa (M) raw bin
 *
 * Double
 * ------
 * 1  bit  sign     (S)
 * 11 bits exp      (E) EXC=1024-1=1023 excess
 * 52 bits mantissa (M) raw bin
 *
 * (S)1,(M)x2^(E-EXC)
 * 
 * MAX  = (0) 1,1111...11x2^(111...10-EXC)
 * ZERO = (0) 1,0000...00x2^(000...00-EXC)
 * MIN  = (1) 1,1111...11x2^(111...10-EXC)
 *
 */

/**********
                   (S)(Exponente)(Mantisa..................)
 -3) C0 40  00  00 (1)(1000000 0)(1000000 00000000 00000000) -2^(128-127)*1.100
 -2) C0 00  00  00 (1)(1000000 0)(0000000 00000000 00000000) -2^(128-127)*1.000
 -1) BF 80  00  00 (1)(0111111 1)(0000000 00000000 00000000) -2^(127-127)*1.000
  0) 00 00  00  00 (0)(0000000 0)(0000000 00000000 00000000) Rep. Especial
  1) 3F 80  00  00 (0)(0111111 1)(0000000 00000000 00000000)  2^(127-127)*1.000
  2) 40 00  00  00 (0)(1000000 0)(0000000 00000000 00000000)  2^(128-127)*1.000
  3) 40 40  00  00 (0)(1000000 0)(1000000 00000000 00000000)  2^(128-127)*1.100

-0.125) BE  0  0  0 (1)(0111110 0)(0000000 00000000 00000000) -2^(124-127)*1.100
 0.125) 3E 00 00 00 (0)(0111110 0)(0000000 00000000 00000000)  2^(124-127)*1.100

 inf) 7F 80 00 00 S=0   E=111...111 M= 000...000 Rep. Especial
-inf) FF 80 00 00 S=1   E=111...111 M= 000...000 Rep. Especial
 NaN) 7F 80 C0 00 S=1|0 E=111...111 M!=000...000 Rep. Especial

Numeros Chicos sin desnormalizar (1.) E=-127 reservado para desnormalizados
-----------------------------------
 Small)  00 80 00 00 S=0   E=000...001 M=000...000 = 2^(-126)*1.000 = 2^(-126)
   
Numeros Chicos desnormailzados   (0.)
-----------------------------------
 Nearest Zero)    S=0   E=000...000 M=000...0001 2^(-126) * 0.000...001 = 2^(-127-22) = 2^(-149)
-Nearest Zero)    S=1   E=000...000 M=000...0001 2^(-126) * 0.000...001
          
Sino el m'inimo ser'ia, debido al bit implícito 1.0,

                  S=0   E=000...000 M=000...0000 2^(-127)*1.000...000

aunque ese valor es una rep.especial y representa el 0.00
***********/

#include <stdio.h>
#include <string.h>
// https://www.gnu.org/software/libc/manual/html_node/IEEE-Floating-Point.html
// EXP: -125..128 (-125 ???)
#include <float.h>

#ifdef BIG_ENDIAN_ARCH
#define PR_ARRAY a[0],a[1],a[2],a[3]
#else
#define PR_ARRAY a[3],a[2],a[1],a[0]
#endif

int main()
{
  int   i;
  float f;
  unsigned char  a[4];

  float ninf = -1.00/0.00;
  float inf  =  1.00/0.00;
  float nan  =  0.00/0.00;

  f = (float) -3.5;
  memcpy(a, &f, 4 );
  printf("%8.4f = %02X %02X %02X %02X\n",f,PR_ARRAY);

  for(i=-3;i<4;++i)
  {
    f = (float) i;
    memcpy(a, &f, 4 );
    printf("%8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);
  }

  f = (float) 3.5;
  memcpy(a, &f, 4 );
  printf("%8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  f = -0.00; 
  memcpy(a, &f, 4 );
  printf("%8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  f = -0.125; /* -1/8 */
  memcpy(a, &f, 4 );
  printf("%8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  f = 1.00/0.00;
  memcpy(a, &f, 4 );
  printf("INF = %8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  f = -1.00/0.00;
  memcpy(a, &f, 4 );
  printf("INF = %8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  //------------------------------------------------
  f = 0.00/0.00;
  memcpy(a, &f, 4 );
  printf("NaN = %8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  a[0] = a[1] = a[2] = a[3] = 0xff;
  memcpy(&f, a, 4 );
  printf("NaN = %8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);
 
  a[0] = a[1] = a[2] = 0xff;
  a[3] = 0x7f;
  memcpy(&f, a, 4 );
  printf("NaN = %8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  a[0] = a[1] = 0xff; 
  a[2] = 0x80;
  a[3] = 0x7f;
  memcpy(&f, a, 4 );
  printf("NaN = %8.4f = %02X %02X %02X %02X\n",f, PR_ARRAY);
  //-------------------------------------------------

  a[0] = a[1] = a[2] = 0xff;
  a[3] = 0x7e;
  memcpy(&f, a, 4 );
  printf("VALID         = %63.8f = %02X %02X %02X %02X\n",f, PR_ARRAY);
  
  a[0] = a[1] = a[2] = 0xff;
  a[3] = 0xfe;
  memcpy(&f, a, 4 );
  printf("VALID         = %63.8f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  //(0) 1,1111...11x2^(111...10-EXC)    
  a[3] = 0xff;
  a[2] = 0x7f;
  a[1] = a[0] = 0xff;
  memcpy(&f, a, 4 );
  printf("NEG. LARGEST  = %63.8f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  //(0) 1,1111...11x2^(111...10-EXC)    
  a[3] = 0x7f;
  a[2] = 0x7f;
  a[1] = a[0] = 0xff;
  memcpy(&f, a, 4 );
  printf("POS. LARGEST  = %63.8f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  f = FLT_MAX;
  memcpy(a, &f, 4 );
  printf("POS. LARGEST  = %63.8f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  //(0) 1,0000...00x2^(000...01-EXC)
  a[0] = a[1] = a[3] = 0x00;
  a[2] = 0x80;
  memcpy(&f, a, 4 );
  printf("SMALL (NORM.) = %63.60f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  f = FLT_MIN;
  memcpy(a, &f, 4 );
  printf("SMALL (NORM.) = %63.60f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  a[0] = 0x01;
  a[1] = a[3] = 0x00;
  a[2] = 0x00;
  memcpy(&f, a, 4 );
  printf("NEAREST ZERO  = %63.60f = %02X %02X %02X %02X\n",f, PR_ARRAY);
 
  a[0] = 0x01;
  a[1] = 0x00;
  a[2] = 0x00;
  a[3] = 0x80;
  memcpy(&f, a, 4 );
  printf("NEAREST ZERO  = %62.60f = %02X %02X %02X %02X\n",f, PR_ARRAY);

  printf("\nValid Number against Inf\n");
  printf("------------------------\n"); 
  printf("1.00 /  inf = %+f\n",1.00/inf); 
  printf("1.00 / -inf = %+f\n",1.00/ninf); 
  printf("-1.00/ -inf = %+f\n",-1.00/ninf); 
  printf("1.00 *  inf = %+f\n",1.00*inf); 
  printf("1.00 * -inf = %+f\n",1.00*ninf);
  printf("1.00 +  inf = %+f\n",1.00+inf); 
  printf("1.00 + -inf = %+f\n",1.00+ninf);
  printf("1.00 -  inf = %+f\n",1.00-inf); 
  printf("1.00 - -inf = %+f\n",1.00-ninf);
  printf("\nInf against Inf\n");
  printf("---------------\n"); 
  printf("inf /  inf = %+f\n",inf/inf); 
  printf("inf / -inf = %+f\n",inf/ninf); 
  printf("inf *  inf = %+f\n",inf*inf); 
  printf("inf * -inf = %+f\n",inf*ninf); 
  printf("inf +  inf = %+f\n",inf+inf); 
  printf("inf + -inf = %+f\n",inf+ninf);
  printf("inf -  inf = %+f\n",inf-inf); 
  printf("inf - -inf = %+f\n",inf-ninf); 
  printf("\nNaN against Any\n");
  printf("---------------\n"); 
  printf("1.00 / nan = %+f\n",1.00/nan);
  printf("1.00 * nan = %+f\n",1.00*nan);
  printf("1.00 + nan = %+f\n",1.00+nan);
  printf("1.00 - nan = %+f\n",1.00-nan);
  printf("nan  / inf = %+f\n",nan/inf);

  printf("\n-ZERO against Any\n");
  printf("---------------\n"); 
  printf("0.00 * -0.00 = %+f\n",0.00*-0.00);
  printf("1.00 * -0.00 = %+f\n",1.00*-0.00);
  
  return (0);  
}


