#include <stdio.h>
#include <stdint.h> 
// #include <inttypes.h>

/*
 * Programa de enteros portables en lenguaje "C"
 *
 * NOTES:
 * Ver tama~nos en include/limits.h 
 *
 * Compilar con gcc -m32|-m64 ...  -m96bit-long-double
 * Otras opciones:
 * -m128bit-long-double -m96bit-long-double -mlong-double-64 -mlong-double-80 
 *
 *
 *
 */

int main()
{
   
              int8_t i1;
             uint8_t i2; 
             int16_t i3;
            uint16_t i4;         
	     int32_t i5;         
            uint32_t i6;
             int64_t i7;
            uint64_t i8;
            intptr_t ptr;
	   uintmax_t i9;

  printf("sizeof(int8_t)           = %6d\n",(int) sizeof(int8_t));
  printf("sizeof(int16_t)          = %6d\n",(int) sizeof(int16_t));
  printf("sizeof(int32_t)          = %6d\n",(int) sizeof(int32_t));
  printf("sizeof(int64_t)          = %6d\n",(int) sizeof(int64_t));
  printf("sizeof(intptr_t)         = %6d\n",(int) sizeof(intptr_t));
  printf("sizeof(uintmax_t)        = %6d\n",(int) sizeof(uintmax_t));
  return(0);
}
