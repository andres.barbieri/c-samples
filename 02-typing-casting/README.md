# Aprendiendo algo de tipos built-in en  "C"

Ejemplos de programas en "C" que muestran los tipos built-in y su variaci�n de acuerdo a la arquitectura (arch). Ejemplos de rangos, casting, floating point y portabilidad de enteros.


## Pruebas
 
Probar ejecutando `make` y luego ejecutar los ejemplos

```console
 make clean
 make
```
## Contenido individual:

* `00-types.c`
* `01-ranges.c`
* `02-portable-int.c`
* `03-autocasting.c`
* `04-div.c`
* `05-numeric-storage.c`
* `06-floats.c`
* `07-endianess.c`
* `08-endianess.c`
* `08-user-def-types.c`
* `09-portable-int-io.c`
* `10-cmp.c`

Cada c�digo esta comentado en si mismo.

Luego como salidas de pruebas:

* `00-types.output.x86_64.txt` Salida en arch 64bits
* `00-types.output.x86_i386.txt` Salida en arch 32bits
* `00-types.output.motez1.txt` Salida en mote z1 (zolertia)      
* `00-types.full.output.motez1.txt` Salida completa mote z1  

