#include <stdio.h>

/*
 * Programa para ilustrar el autocasting que hace de tipos el lenguaje "C"
 * entender su uso, funcionamiento y problemas posibles. 
 *
 * NOTES:
 * 
 * Type Conversion Rules:
 * ----------------------
 *
 * 1) Asignments:
 *
 * a) between integer values (char, long, int)
 * 
 * Right expression converted to the type of the left varaible.
 * The high-order part is dropped or the original part is conserved
 * (May be or not sign extension)
 * dropping of excess hbits or add hbits.
 *
 * b) between integer values (char, long, int) and floating point values
 *    (float, double).
 *
 *   int   = float (the fractional part is truncated)
 *   float = int   (the fractional part is added as .00)
 *
 * c) between floating  point values (float, double).
 *
 *  float  = double (implementation dependent truncation or rounding)
 *  double = float  (the value is extended)
 *
 * 2) Expressions
 *
 * a) Narrower operand to a Wider operand without losing info. 
 *    Automatic conversion in expressions. 
 *
 *    float + int    (int is converted to float)
 *    int   + long   (int converted to long)
 *    char  + int    (char to int) 
 *    short + int    (short to int)
 *    float + double (In the original spec say that it is nos resolved automatically)
 * 
 * 3) Expressions that no make scense are not allowed. 
 *     (array[float])
 *
 * 4) Expressions that lose information are allowed but is recomended to draw 
 *    a warning.
 *     (int = long long)
 *
 * 5) Chars are allowed in arithmetic expressions. Chars are unsigneds.
 *     (char + char)
 * 6) Chars ever are positive values
 *
 * 7) Booleans 1 = true, 0 = false
 */

int main()
{
   
                 int i1;
  unsigned       int i2; 
           short int i3;         
  long     long  int i4;
 
                char c1;
   unsigned     char c2; 

               float  f1;
	       double d1; 
//long         double d3;  unused
 	    
  i2 =0xffffffff;
  i1 = i2;
  printf("Autocast uint int\n");
  printf("int  (%16d)   = uint(%16u) \n",i1, i2);
  i2 = i1;
  i2 = (unsigned int) i1;
  printf("uint (%16u)   = int (%16d) \n",i2, i1);
  printf("uint (%16X)   = int (%16X) \n",i2, i1);

  i1 =0x0fffffff;
  i2 = i1;
  printf("uint (%16u)   = int (%16d) \n",i2, i1);
  printf("uint (%16X)   = int (%16X) \n",i2, i1);

  printf("***\n");
  printf("Autocast sint int, uint\n");
  i1 = 0xffff0000;
  i2 = 0xffff0000;
  i3 = i1;
  printf("sint (%16d)   = int (%16d)\n",i3,i1);
  printf("sint (%16d)   = uint(%16u)\n",i3,i2);
  printf("sint (%16hX)   = int (%16X)\n",i3,i1);
  printf("sint (%16hX)   = uint(%16X)\n",i3,i2);

  i1 = -0x000f;
  i2 =  0x000f;
  i3 = i1;
  printf("sint (%16d)   = int (%16d)\n",i3,i1);
  printf("sint (%16hX)   = int (%16X)\n",i3,i1);
  i3 = i2;
  printf("sint (%16d)   = uint(%16u)\n",i3,i2);
  printf("sint (%16hX)   = uint(%16X)\n",i3,i2);
  printf("***\n");

  i3 = 0xffff;
  i1 = i3;
  i2 = i3;
  printf("int  (%16d)   = sint(%16d)\n",i1,i3);
  printf("int  (%16X)   = sint(%16hX)\n",i1,i3);
  printf("uint (%16u)   = sint(%16d)\n",i2,i3);
  printf("uint (%16X)   = sint(%16hX)\n",i2,i3);

  printf("***\n");
  printf("Autocast int long long int\n");
  i4 = 0xffffffff00000000;
  i1 = i4;
  printf("int  (%16d)   = llint(%16lld)\n",i1,i4);
  printf("int  (%16X)   = llint(%16llX)\n",i1,i4);

  i1 = 0xffffffff;
  i4 = i1;
  printf("llint(%16lld)   = int  (%16d)\n",i4,i1);
  printf("llint(%16llX)   = int  (%16X)\n",i4,i1);

  printf("***\n");
  printf("Autocast char int , int char , char uchar\n");
  i1 = 0xff00;
  c1 = i1;
  
  printf("char (%16d)   = int (%16d)\n",c1,i1);
  printf("char (%16X)   = int (%16X)\n",c1,i1);
  c1 = 'a';
  i1 = c1;
  printf("int  (%16d)   = char(%16d)('%c')\n",i1,c1,c1);

  c1 = '}';
  c1 = c1 + 10;
  c2 = c1 + 10;
  printf("char (%16d)   = ucha(%16d)\n",c1,c2);
  printf("***\n");
  printf("Autocast int float, float int \n");

  f1 = 134.56;
  i1 = f1;
  printf("int  (%16d)   = float(%.12f)\n",i1,f1);
  printf("int  (%16d)   = float(%e)\n",i1,f1);
  f1 = i1;
  printf("float(%.12f)   = int  (%16d)\n",f1,i1);  

  printf("***\n");
  printf("Autocast float double, double float \n");
  d1 = 134.56;
  f1 = d1;
  printf("float (%.16f)   = double (%.16f)\n",f1,d1);
  d1 = f1;  
  printf("double(%.16f)   = float  (%.16f)\n",d1,f1);
  return(0);
}
