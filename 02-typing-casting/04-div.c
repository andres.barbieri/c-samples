#include <stdio.h>

/*
 * Ejemplos de divisiones num'ericas en forma entera o punto flotante.
 * C'omo invocar algunas otras operaciones para ser usadas como divisiones.
 * Se muestra c'omo la asinación con tipos de diferentes tama~nos fijos
 * puede ser usada con el pro'posito de obtener el resultado de divisiones
 * particulares.  
 */

int main()
{
  
  int i1;
  char c1;
  
  // Division Entera - Obtener el resto
  i1 = 0xff01;
  c1 = i1;  
  printf("char (%16d)    = int (%16d)\n",c1,i1);
  printf("char (%16X)    = int (%16X)\n",c1,i1);
  printf("     (%16X) >> %d   = (%16X)\n",i1,8,i1>>8);
  printf("%d   >>   %d   = %d\n",i1,8,i1>>8);
  printf("%d mod(\%%) %d = %d\n",i1,256,i1%256);
  printf("%d div(/) %d = %d\n",i1,256,i1/256);

  printf("***\n");
  // Division en Float - presición
  i1 = 1234;
  // Se pierde precisi'on
  printf("%d /   %d = %f\n",i1,256,(i1/(float)255));
  printf("%d /   %d = %f\n",i1,256,(i1/255.00));
  printf("%d /   %d = %.30f\n",i1,256,(float)i1/255);
  printf("%d /   %d = %.30f\n",i1,256,(double)i1/255);
  printf("%d /   %d = %f\n",i1,256,(float)(i1/255));

  // No se pierde presici'on
  printf("***\n");
  printf("%d /   %d = %.30f\n",i1,256,(float)i1/256);
  printf("%d /   %d = %.30f\n",i1,256,(double)i1/256);
  return(0);
}
