/***
 *
 * Ejemplo de comparación signed vs. unsgined, asm intel
 * Ver el assembler que genera y ver como compara con signo y sin signo
 * combinados.
 *
 * gcc -c -DT1="unsigned int" 10-icmp.c -o x.o | grep j
 * objdump -d x.o | grep j
 * 1c:	76 05                	jbe    23 <main+0x23>
 * 21:	eb 03                	jmp    26 <main+0x26>
 *
 * gcc -c -DT1="int" 10-icmp.c -o x.o
 * objdump -d x.o | grep j
 * 1c:	7e 05                	jle    23 <main+0x23>
 * 21:	eb 03                	jmp    26 <main+0x26>
 *
 * JBE Jump if below or equal C==1  ||  Z==1
 * JLE Jump if less  or equal S!=O  ||  Z==1
 */

  
#ifndef T1
#define T1 int
#endif

#ifndef T2
#define T2 int
#endif

int main()
{
  T1 a = 10;
  T1 b = -20;
  
  if (a>b) return a;
  return b;
}
