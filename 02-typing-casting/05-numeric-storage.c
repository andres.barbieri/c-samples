#include <stdio.h>
#include <string.h>

// Asume sizeof(float) = sizeof(int) = 4
void print_float_fmt(float f)
{
  unsigned char  a[4];

  memcpy(a, &f, 4 );
#ifdef BIG_ENDIAN_ARCH
  printf("%8.4f = %02X %02X %02X %02X",f,a[0],a[1],a[2],a[3]);
#else
  printf("%8.4f = %02X %02X %02X %02X",f,a[3],a[2],a[1],a[0]);
#endif
}

void print_int_fmt(int i)
{
  unsigned char  a[4];

  memcpy(a, &i, 4 );
#ifdef BIG_ENDIAN_ARCH
  printf("%8d = %02X %02X %02X %02X",i,a[0],a[1],a[2],a[3]);
#else
  printf("%8d = %02X %02X %02X %02X",i,a[3],a[2],a[1],a[0]);
#endif
}

// Asume sizeof(short int) = 2
void print_sint_fmt(short int i)
{
  unsigned char  a[2];

  memcpy(a, &i, 2 );
#ifdef BIG_ENDIAN_ARCH
  printf("%8d = %02X %02X",i,a[0],a[1]);
#else
  printf("%8d = %02X %02X",i,a[1],a[0]);
#endif
}


int main()
{
  float      f   = 3.5;   
  float      f2  = 3.0;   
  int        i   =  3;
  short int  s   =  4;
  int        i2  =  70000;

  print_float_fmt(f);
  printf("\n");
  print_float_fmt(-f);
  printf("\n");
  print_float_fmt(f2);
  printf("\n");
  print_float_fmt(-f2);   
  printf("\n");
  print_int_fmt(i);
  printf("\n");
  print_int_fmt(-i);
  printf("\n");
  print_int_fmt(i);
  printf("\n");
  print_int_fmt(-i);
  printf("\n");
  print_sint_fmt(s);
  printf("\n");
  print_sint_fmt(-s);
  printf("\n");
  print_int_fmt(i2);
  printf("\n");
  print_int_fmt(-i2);
  printf("\n");

  return (0);
}

/*** EOF ***/
