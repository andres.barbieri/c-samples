#include <stdio.h>
//#include <stdint.h> 
#include <inttypes.h> // includes stdint.h PRN SCN

/*
 * Programa de enteros portables en lenguaje "C"
 *
 * NOTES:
 * Ver tama~nos en include/limits.h 
 *
 * Compilar con gcc -m32|-m64 ...  -m96bit-long-double
 * Otras opciones:
 * -m128bit-long-double -m96bit-long-double -mlong-double-64 -mlong-double-80 
 * 
 * Ver como se expanden las macro PRN pre-procesando con gcc -E 
 *
 *
 */

int main()
{
              int8_t i1 = -10;
             uint8_t i2 =  10; 
             int16_t i3 = -8000;
            uint16_t i4 =  65000;         
	     int32_t i5 =  -650001;         
            uint32_t i6 =  6500002;
             int64_t i7 =  -11111111111111111;
            uint64_t i8 =   11111111111111111;
            intptr_t ptr;
	   uintmax_t i9;

  printf("sizeof(int8_t)            = %20d\n",(int) sizeof(int8_t));
  printf("int8_t                    = %20" PRId8 "\n", i1);
  printf("sizeof(int16_t)           = %20d\n",(int) sizeof(int16_t));
  printf("int16_t                   = %20" PRId16 "\n", i3);
  printf("uint16_t                  = %20" PRIu16 "\n", i4);
  printf("sizeof(int32_t)           = %20d\n",(int) sizeof(int32_t));
  printf("int32_t                   = %20" PRId32 "\n", i5);
  printf("uint32_t                  = %20" PRIu32 "\n", i6);
  printf("sizeof(int64_t)           = %20d\n",(int) sizeof(int64_t));
  printf("int64_t                   = %20" PRId64 "\n", i7);
  printf("uint64_t                  = %20" PRIu64 "\n", i8);
  printf("sizeof(intptr_t)          = %20d\n",(int) sizeof(intptr_t));
  printf("sizeof(uintmax_t)         = %20d\n",(int) sizeof(uintmax_t));
  return(0);
}
