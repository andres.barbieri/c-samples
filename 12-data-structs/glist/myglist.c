/*
 * Ejemplo de una lista gen'erica "casera"
 */

#include <stdlib.h> /*** malloc ***/ 
#include <string.h> /*** memcpy ***/
#include "myglist.h"

#ifndef NULL
#define NULL (0)
#endif

//#define MYGLIST_CPY_ELEM 1

int add_front(list_t *l, void *e, size_t s, void (*f)(void *, size_t))
{
  list_t aux;
  
  aux = *l;
  *l = malloc(sizeof(node_t));
  if (*l == NULL) 
    {
      return -1;
    }
  /*** Copy Data ***/
#ifdef MYGLIST_CPY_ELEM
  (*l)->elem = malloc(s);
  if ((*l)->elem == NULL)
    {
      free (*l);
      return -2;
    }
  memcpy((*l)->elem, e, s);
#else
  (*l)->elem = e;
#endif
  (*l)->f    = f;
  (*l)->size = s;  
  (*l)->next = aux;
  return 0;
}

int delete_front(list_t *l)
{
  list_t aux;
  if (l != NULL)
    {
      aux = *l;
#ifdef MYGLIST_CPY_ELEM
      free((*l)->elem);
#endif
      (*l) = aux->next;
      free(aux);
      return 0;
    }
  return -1;
}

int delete_all(list_t *l)
{
  list_t aux;
  while (*l != NULL)
    { 
      aux = *l;
#ifdef MYGLIST_CPY_ELEM
      free((*l)->elem);
#endif
      *l = (*l)->next;
      free(aux);
    }
  return 0;
}

int iterate_f(const list_t l)
{
  list_t aux;
  aux = l;
  while (aux !=NULL)
    {
      aux->f((aux->elem),aux->size);
      aux = aux->next;
    }
  return 0;
}

/*** EOF ***/
