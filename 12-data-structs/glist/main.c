/*
 * Ejemplo de una lista gen'erica "casera"
 */

#include <stdio.h>
#include "myglist.h"

void f_int(void *i, size_t s)
{
  printf("%d(%lu) ",*((int*)i), s);
}

void f_char(void *c, size_t s)
{
  printf("%c(%lu) ",*((char*)c), s);
}
  
void f_double(void *d, size_t s)
{
  printf("%lf(%lu) ",*((double*)d), s);
}
 
void f_string(void *s, size_t l)
{
  printf("%s(%lu) ",((char*)(s)), l);  
}


int main()
{
  list_t l = NULL;
  int i;
  double d;
  char j;

  for(i=0;i<10;i++) add_front(&l,(void*) &i, sizeof(i), f_int);
  for(j='a';j<'j';j++) add_front(&l,(void*) &j, sizeof(j), f_char);
  add_front(&l,(void*) "HOLA", sizeof("HOLA"), f_string);
  add_front(&l,(void*) "QUE TAL", sizeof("QUE TAL"), f_string);
  for(d=0.0;d<3.0;d++) add_front(&l,(void*) &d, sizeof(d),f_double);
  iterate_f(l); printf("*\n");
  delete_front(&l);
  delete_front(&l);
  iterate_f(l); printf("*\n");
  delete_front(&l);
  delete_front(&l);
  delete_front(&l);
  iterate_f(l); printf("*\n");
  delete_front(&l);
  delete_all(&l);
  iterate_f(l); printf("*\n");
  return 0;
}

/*** EOF ***/
