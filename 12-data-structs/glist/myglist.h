
/*
 * Ejemplo de una lista gen'erica "casera"
 */

#ifndef MY_GLIST_
#define MY_GLIST_ 1

#include <stdlib.h> /*** size_t ***/

typedef struct node {
  void    *elem;
  size_t  size;
  void    (*f)(void *, size_t);
  struct node *next;
} node_t;

typedef node_t *list_t;

int add_front(list_t *l, void *e, size_t s, void (*f)(void *, size_t));

int delete_front(list_t *l);

int delete_all(list_t *l);

int iterate_f(const list_t l);

#endif /* myglist.h */
