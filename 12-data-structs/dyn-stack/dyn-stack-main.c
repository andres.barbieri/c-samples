#include <stdio.h>         /** printf **/
#include <stdlib.h>        /** malloc, free **/
#include <stdbool.h>       /** bool **/

typedef struct stack_node {
         int                 elem;
         struct stack_node   *next;
} stack_node_t;

typedef stack_node_t *stack_t;


void st_create(stack_t *self)
{
  *self = NULL;
}

/** 
 * PRE: st_create(self),  stack debe estar creada 
 */
bool st_is_empty(const /*@in@*/ stack_t self)
{
   return (self == (stack_t) NULL);
}

/** 
 * PRE: st_create(self), stack debe estar creada 
 */
int st_length(const /*@in@*/ stack_t self)
{
  if (st_is_empty(self)) {
      return 0;
    } else {
    return 1 + st_length(self->next);
  }
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self) 
 */
int st_top(const /*@in@*/ stack_t self)
{
  return self->elem;
}

/** 
 * PRE: st_create(self)  &&  st_length(self)< STACK_SIZE
 */
void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ int elem)
{
   stack_node_t *node;

  node = malloc(sizeof(stack_node_t));
  node->elem = elem;
  node->next = (stack_node_t*) (*self);
  (*self) = (stack_t) node;
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self) 
 */
void st_pop(/*@inout@*/ stack_t *self)
{
  stack_t aux;

  aux = (*self);
  (*self) = (stack_t) (*self)->next;
  free(aux);
}

/** 
 * PRE: st_create(self) 
 */
void st_show(const /*@in@*/ stack_t self)
{
  stack_t aux;

  aux = self;
  printf("l=(%d)",st_length(self));
  printf("[");
  while(!st_is_empty(aux))
    {
      printf("%d ",aux->elem);
      aux = aux->next;
    }
  printf("]");
}


int main()
{
  stack_t s;

  st_create(&s);

  st_show(s);
  printf("\n");
  
  for (int i = 0; i < 10; i++) {
    st_push(&s, i);
  }
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("(%d)\n",st_length(s));
  printf("TOP=%d\n",st_top(s));
  for (int i = 0; i < 10; i++) {
    printf("%d \n",st_top(s));
    st_pop(&s);
  }
  st_show(s);
  printf("\n");
  return 0;
}

/*** EOF ***/
