#include <stdio.h>         /** printf **/
#include <stdlib.h>        /** malloc, free **/
#include "stack.h"

void st_create(stack_t *self)
{
  *self = NULL;
}

/** 
 * PRE: st_create(self),  stack debe estar creada 
 */
bool st_is_empty(const /*@in@*/ stack_t self)
{
   return (self == (stack_t) NULL);
}

/** 
 * PRE: st_create(self), stack debe estar creada 
 */
int st_length(const /*@in@*/ stack_t self)
{
  if (st_is_empty(self)) {
      return 0;
    } else {
    return 1 + st_length(self->next);
  }
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self)) 
 */
int st_top(const /*@in@*/ stack_t self)
{
  return self->elem;
}

/** 
 * PRE: st_create(self)  &&  st_length(self)< STACK_SIZE
 */
void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ int elem)
{
   stack_node_t *node;

  node = malloc(sizeof(stack_node_t));
  node->elem = elem;
  node->next = (stack_node_t*) (*self);
  (*self) = (stack_t) node;
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self) 
 */
void st_pop(/*@inout@*/ stack_t *self)
{
  stack_t aux;

  aux = (*self);
  (*self) = (stack_t) (*self)->next;
  free(aux);
}

/** 
 * PRE: st_create(self) 
 */
void st_show(const /*@in@*/ stack_t self)
{
  stack_t aux;

  aux = self;
  printf("l=(%d)",st_length(self));
  printf("[");
  while(!st_is_empty(aux))
    {
      printf(DATATYPEPRMASK" ",aux->elem);
      aux = aux->next;
    }
  printf("]");
}

/*** EOF ***/
