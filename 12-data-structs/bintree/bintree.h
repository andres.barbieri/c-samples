#ifndef MY_BINTREE_H_
#define MY_BINTREE_H_ 1
#include <stdlib.h>        /** malloc / free **/
#include <stdbool.h>       /** bool **/
#include <stdio.h>         /** printf **/


#ifndef DATATYPE
//# define DATATYPE int
# define DATATYPE char
#endif

typedef struct bintree_node {
         DATATYPE           elem;
         struct bintree_node   *left;
         struct bintree_node   *right;
} bintree_node_t;

typedef bintree_node_t *bintree_t;

typedef enum {in_order, post_order, pre_order} order_t;

bintree_t bt_create();

bool bt_is_empty(const /*@in@*/ bintree_t self);

int bt_length(const /*@in@*/ bintree_t self);

int bt_includes(/*@in@*/ bintree_t self, const /*@in@*/ DATATYPE elem);

DATATYPE bt_head(const /*@in@*/ bintree_t self);

bintree_t bt_left(const /*@in@*/ bintree_t self);

bintree_t bt_right(const /*@in@*/ bintree_t self);

void bt_add(/*@inout@*/ bintree_t *self, const /*@in@*/ DATATYPE elem);

void bt_free(/*@inout@*/ bintree_t *self);

void bt_iterate(const /*@in@*/ bintree_t self, DATATYPE func(DATATYPE), order_t order);

#endif /* bintree.h */

/*** EOF ***/
