#include <stdio.h>
#include "gbintree.h"

void int_print(void *e)
{
  printf("%d ",*((int *) e));
  //return e;
}

void int_print_stream(void *e, FILE *stm)
{
  fprintf(stm, "%d",*((int *) e));
}


int int_cmp(void * x, void *y) 
{
    int a = *((int *) x);
    int b = *((int *) y);
    // a>b  : 1
    // a==b : 0
    // a<b  :-1
    return ( (a > b) ? 1 : ((a == b) ? 0: -1) );
}

int main()
{
  bintree_t t  = NULL;
  int       x  = 18;
  int       y  = 15;
  int       w  = 100;

  for (int i = 0; i < 20; i+=2) {
    //bt_add(&t, &i, 0, int_cmp); // all have the same value, why ?
    bt_add(&t, &i, sizeof(i), int_cmp);
  }
  bt_iterate(t,int_print,in_order);
  printf("\n");
  bt_iterate(t,int_print,pos_order);
  printf("\n");
  bt_iterate(t,int_print,pre_order);
  printf("\n");
  bt_show_all_levels(t,int_print_stream, stdout);
  printf("%d %d %d %d\n",bt_length(t), bt_depth(t),
	 bt_includes(t, &x, int_cmp), bt_includes(t, &y, int_cmp));
  for (int i = 1; i < 20; i+=2) {
    //bt_add(&t, &i, 0, int_cmp); // all have the same value, why ?
    bt_add(&t, &i, sizeof(i), int_cmp);
  }
  bt_show_all_levels(t,int_print_stream, stdout);
  printf("%d %d %d %d\n",bt_length(t), bt_depth(t),
	bt_includes(t, &x, int_cmp), bt_includes(t, &y, int_cmp));
  for (int j = -10; j <0; j++) {
    //bt_add(&t, &j, 0, int_cmp); // all have the same value, why ? hint: where is index j located ?
    bt_add(&t, &j, sizeof(j), int_cmp);
  }
  bt_add(&t, &w, 0, int_cmp); 
  bt_iterate(t,int_print,in_order);
  printf("\n");
  bt_iterate(t,int_print,revr_order);
  printf("\n");
  bt_iterate(t,int_print,pos_order);
  printf("\n");
  bt_iterate(t,int_print,pre_order);
  printf("\n");
  bt_show_all_levels(t,int_print_stream, stdout);
  printf("%d %d %d %d\n",bt_length(t), bt_depth(t),
	   bt_includes(t, &x, int_cmp), bt_includes(t, &y, int_cmp));
  bt_free(&t);
  printf("%d %d %d %d\n",bt_length(t), bt_depth(t), 
	   bt_includes(t, &x, int_cmp), bt_includes(t, &y, int_cmp));
  return 0;
}
