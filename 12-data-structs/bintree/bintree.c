#include "bintree.h"

bintree_t bt_create()
{
  return (bintree_t) NULL;
}

bool bt_is_empty(const /*@in@*/ bintree_t self)
{
  return (self == (bintree_t) NULL);
}

int bt_length(const /*@in@*/ bintree_t self)
{
  if (bt_is_empty(self)) {
      return 0;
    } else {
    return 1 + bt_length(bt_left(self)) + bt_length(bt_right(self));
  }
}

int bt_includes(/*@in@*/ bintree_t self, const /*@in@*/ DATATYPE elem)
{
 if (bt_is_empty(self)) {
      return 0;
   } else {
   return (self->elem == elem) || bt_includes(bt_left(self), elem)
     || bt_includes(bt_right(self), elem);
 }
}

DATATYPE bt_head(const /*@in@*/ bintree_t self)
{
  return self->elem;
}

bintree_t bt_left(const /*@in@*/ bintree_t self)
{
  return self->left;
}
bintree_t bt_right(const /*@in@*/ bintree_t self)
{
  return self->right;
}

void bt_add(/*@inout@*/ bintree_t *self, const /*@in@*/ DATATYPE elem)
{
  if (bt_is_empty(*self))
    {
      bintree_node_t *node = malloc(sizeof(bintree_node_t));
      node->elem = elem;
      node->left = NULL;
      node->right = NULL;
      *self = node;
    }
  else if (((*self)->elem) > elem)
    {
      bt_add(&(*self)->left, elem);
    }
  else
    {
      bt_add(&(*self)->right, elem);
    }
}

void bt_free(/*@inout@*/ bintree_t *self)
{
  bintree_t left_aux,right_aux;

  if (!bt_is_empty(*self))
    {
      left_aux = (*self)->left;
      right_aux = (*self)->right;
      free(*self);
      bt_free(&left_aux);
      bt_free(&right_aux);
    }
  *self = NULL;
}

void bt_iterate(const /*@in@*/ bintree_t self, DATATYPE func(DATATYPE), order_t order)
{
  if (!bt_is_empty(self))
    {
      switch (order)
	{
	case in_order:
	  bt_iterate(self->left,func,order);
	  func(self->elem);
	  bt_iterate(self->right,func,order);
	  break;
	case pre_order:	  
	  func(self->elem);
	  bt_iterate(self->left,func,order);
	  bt_iterate(self->right,func,order);
	  break;
	default: /* post_order */
	  bt_iterate(self->left,func,order);
	  bt_iterate(self->right,func,order);
	  func(self->elem);
	}
    }
}
		
 
/*** EOF ***/

