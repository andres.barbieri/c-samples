#include "gbintree.h" 
#include <string.h> /* memcpy */
#include <math.h>

bintree_t bt_create()
{
  return (bintree_t) NULL;
}

bool bt_is_empty(const /*@in@*/ bintree_t self)
{
  return (self == (bintree_t) NULL);
}

int bt_length(const /*@in@*/ bintree_t self)
{
  if (bt_is_empty(self)) {
      return 0;
    } else {
    return 1 + bt_length(bt_left(self)) + bt_length(bt_right(self));
  }
}

int bt_depth(const /*@in@*/ bintree_t self)
{
  if (bt_is_empty(self)) {
      return 0;
    } else {
    return 1 + fmax(bt_depth(bt_left(self)), bt_depth(bt_right(self)));
  }
}


int bt_includes(/*@in@*/ bintree_t self, /*@in@*/ void *elem,  
                /*@in@*/ int (*cmp)(void*, void*))
{
 if (bt_is_empty(self)) {
      return 0;
   } else {
   return (!cmp(self->elem,elem)) || bt_includes(bt_left(self), elem, cmp)
     || bt_includes(bt_right(self), elem, cmp);
 }
}

void *bt_head(const /*@in@*/ bintree_t self)
{
  return self->elem;
}

bintree_t bt_left(const /*@in@*/ bintree_t self)
{
  return self->left;
}
bintree_t bt_right(const /*@in@*/ bintree_t self)
{
  return self->right;
}

int bt_add(/*@inout@*/ bintree_t *self, /*@in@*/ void *elem, /*@in@*/size_t size,  
            /*@in@*/ int (*cmp)(void*, void*))
{
  bintree_node_t *node = NULL;
  if (bt_is_empty(*self))
    {
      node = malloc(sizeof(bintree_node_t));
      if (!node) // == NULL
	{
	  return -1;
	}
      if (size == 0)
	{
	  node->elem = elem;
	  node->size = 0;
	}
      else
	{
	  node->elem = malloc(size);
	  if (!(node->elem)) // == NULL
	    {
	      free (node);
	      return -2;
	    }
	  memcpy(node->elem, elem, size);
	  node->size = size;
	}
      node->left = NULL;
      node->right = NULL;
      *self = node;
      return 0;
    }
  else if ( cmp(((*self)->elem), elem)>0 )
    {
      return bt_add(&((*self)->left), elem, size, cmp);
    }
  else
    {
      return bt_add(&((*self)->right), elem, size, cmp);
    }
}

void bt_free(/*@inout@*/ bintree_t *self)
{
  if (!bt_is_empty(*self))
    {
      bt_free(& ((*self)->left));
      bt_free(& ((*self)->right));
      if (((*self)->size) != 0) free(((*self)->elem));
      free(*self);
    }
  *self = NULL;
}
void bt_iterate(const /*@in@*/ bintree_t self, void (*func)(void *), order_t order)
{
  if (!bt_is_empty(self))
    {
      switch (order)
	{
	case in_order:
	  bt_iterate(self->left,func,order);
	  func(self->elem);
	  bt_iterate(self->right,func,order);
	  break;
	case revr_order:
	  bt_iterate(self->right,func,order);
	  func(self->elem);
	  bt_iterate(self->left,func,order);
	  break;
	case pre_order:	  
	  func(self->elem);
	  bt_iterate(self->left,func,order);
	  bt_iterate(self->right,func,order);
	  break;
	default: /* pos_order */
	  bt_iterate(self->left,func,order);
	  bt_iterate(self->right,func,order);
	  func(self->elem);
	}
    }
}

static void bt_show_level(const bintree_t self, 
                          void (*print_func)(void *, FILE *), 
                          FILE *stm, 
                          int level)
{
  if (!bt_is_empty(self))
    {
      if (level == 0)
	{
	  print_func(self->elem, stm);
	  fprintf(stm, " ");
	}
      else
	{
	  bt_show_level(self->left, print_func, stm, level-1);
 	  bt_show_level(self->right, print_func, stm, level-1);
	}
    }
}

void bt_show_all_levels(const /*@in@*/ bintree_t self, 
			void (*print_func)(void*, FILE*), 
			FILE *stm)
{
  int i;
  for(i=0;i<bt_depth(self);i++)
    {
      bt_show_level(self, print_func, stm, i);
      fprintf(stm, "\n");
    }
}
/*** EOF ***/

