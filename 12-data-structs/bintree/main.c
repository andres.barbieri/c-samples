#include "bintree.h"

int print(int e)
{
  printf("%d ",e);
  return e;
}

int inc(int e)
{
  return (e+1);
}

int ge8(int e)
{
  return (e>=8);
}

int plus(int e1,int e2)
{
  return (e1+e2);
}

int main()
{
  bintree_t t  = NULL;

  for (int i = 0; i < 10; i++) {
    bt_add(&t, i);
  }
  bt_iterate(t,print,in_order);
  printf("\n");
  bt_iterate(t,print,post_order);
  printf("\n");
  bt_iterate(t,print,pre_order);
  printf("\n");
  printf("%d\n",bt_length(t));
  for (int i = -10; i <0; i++) {
    bt_add(&t, i);
  }
  bt_iterate(t,print,in_order);
  printf("\n");
  bt_iterate(t,print,post_order);
  printf("\n");
  bt_iterate(t,print,pre_order);
  printf("\n");
  printf("%d\n",bt_length(t));
  bt_free(&t);
  printf("%d\n",bt_length(t));
  return 0;
}
