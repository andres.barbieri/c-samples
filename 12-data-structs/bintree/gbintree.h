#ifndef MY_GBINTREE_H_
#define MY_GBINTREE_H_ 1
#include <stdlib.h>          /** malloc / free **/
#include <stdbool.h>         /** bool **/
#include <stdio.h>         /** printf **/


typedef struct bintree_node {
  void                  *elem;
  size_t                size;
  struct bintree_node   *left;
  struct bintree_node   *right;
} bintree_node_t;

typedef bintree_node_t *bintree_t;

typedef enum {in_order, pos_order, pre_order, revr_order} order_t;

bintree_t bt_create();

bool bt_is_empty(const /*@in@*/ bintree_t self);

int bt_length(const /*@in@*/ bintree_t self);

int bt_depth(const /*@in@*/ bintree_t self);

int bt_includes(/*@in@*/ bintree_t self, /*@in@*/ void *elem, /*@in@*/ int (*cmp)(void*, void*));

void *bt_head(const /*@in@*/ bintree_t self);

bintree_t bt_left(const /*@in@*/ bintree_t self);

bintree_t bt_right(const /*@in@*/ bintree_t self);

int bt_add(/*@inout@*/ bintree_t *self, /*@in@*/ void *elem, /*@in@*/size_t size,  
           /*@in@*/ int (*cmp)(void*, void*));

void bt_free(/*@inout@*/ bintree_t *self);

void bt_iterate(const /*@in@*/ bintree_t self, void (*func)(void *), order_t order);

void bt_show_all_levels(const /*@in@*/ bintree_t self, void (*print_func)(void *, FILE *), FILE *stm);

#endif /* gbintree.h */

/*** EOF ***/
