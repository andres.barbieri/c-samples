#include <stdio.h>
#include <assert.h>
#include "sstack_check.h"

int main()
{
  stack_t s;

  st_create(&s);

  st_show(s);
  printf("\n");
  
  for (int i = 0; i < 10; i++) {
    st_push(&s, i);
  }
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("(%d)\n",st_length(s));
  printf("TOP=%d\n",st_top(s));
  // Going out of range
  for (int i = 0; i < 10; i++) {
    int e;
    if (st_top_check(s,&e))
      {
	printf("%d \n",e);
      }
    st_pop_check(&s);
  }
  st_show(s);
  for (int i = 0; i < 1000; i++) {
    st_push_check(&s, i);
  }
  st_show(s);
  fflush(stdout);
  for (int i = 0; i < 1000; i++) {
    assert(!st_is_empty(s));
    st_pop(&s);
  }  
  printf("\n");
  return 0;
}

/*** EOF ***/
