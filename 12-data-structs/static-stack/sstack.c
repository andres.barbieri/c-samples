#include <stdio.h> 
#include "sstack.h"

void st_create(stack_t *self)
{
  self->dim = 0;
}

/** 
 * PRE: st_create(self), stack debe estar creada 
 */
bool st_is_empty(const /*@in@*/ stack_t self)
{
  return self.dim == 0;
}

/** 
 * PRE: st_create(self), stack debe estar creada 
 */
int st_length(const /*@in@*/ stack_t self)
{
  return self.dim;
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self)) 
 */
DATATYPE st_top(const /*@in@*/ stack_t self)
{
  return self.elements[self.dim-1];
}

/** 
 * PRE: st_create(self)  &&  (st_length(self)<STACK_SIZE)
 */
void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ DATATYPE elem)
{
  self->elements[self->dim] = elem;
  self->dim++;
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self)) 
 */
void st_pop(/*@inout@*/ stack_t *self)
{
  (self->dim)--;
}

/** 
 * PRE: st_create(self) 
 */
void st_show(const /*@in@*/ stack_t self)
{
  int i;
  printf("l=(%d)",st_length(self));
  printf("[");
  for (i=0; i<st_length(self) ; i++)
  {
    printf(DATATYPEPRMASK" ",self.elements[i]);
  }
  printf("]");
}

/*** EOF ***/
