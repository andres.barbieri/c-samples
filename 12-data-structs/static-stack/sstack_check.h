#ifndef SSTACK_CHECK_H_
#define SSTACK_CHECK_H_ 1

#include "sstack.h"

bool st_top_check(const /*@in@*/ stack_t self, DATATYPE *elem);

bool st_push_check(/*@inout@*/ stack_t *self, const /*@in@*/ DATATYPE elem);

bool st_pop_check(/*@inout@*/ stack_t *self);
  
#endif

/*** EOF ***/
