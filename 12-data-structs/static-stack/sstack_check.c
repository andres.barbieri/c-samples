#include <stdio.h> 
#include "sstack_check.h"

/** 
 * POST: true on success
 *       false if fails
 */
bool st_top_check(const /*@in@*/ stack_t self, DATATYPE *elem)
{
  if (!(st_is_empty(self)))
    {
      *elem = self.elements[self.dim-1];
      return true; 
    }
    return false;  
}

/** 
 * POST: true on success
 *       false if fails
 */
bool st_push_check(/*@inout@*/ stack_t *self, const /*@in@*/ DATATYPE elem)
{
  if (st_length(*self)<STACK_SIZE)
    {
      self->elements[self->dim] = elem;
      self->dim++;
      return true;
    }
  return false;
}

bool st_pop_check(/*@inout@*/ stack_t *self)
{
  if (!(st_is_empty(*self)))
    {
      (self->dim)--;
      return true; 
    }
    return false;  
}

/*** EOF ***/
