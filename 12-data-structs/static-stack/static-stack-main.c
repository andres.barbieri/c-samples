#include <stdio.h>         /** printf **/
#include <stdbool.h>       /** bool **/

#ifndef STACK_SIZE
#define STACK_SIZE   256
#endif

typedef struct st {
          int elements[STACK_SIZE];
          int dim; 
} stack_t;



stack_t st_create(stack_t *self)
{
  self->dim = 0;
}

/** 
 * PRE: st_create(self),  stack debe estar creada 
 */
bool st_is_empty(const /*@in@*/ stack_t self)
{
  return self.dim == 0;
}

/** 
 * PRE: st_create(self), stack debe estar creada 
 */
int st_length(const /*@in@*/ stack_t self)
{
  return self.dim;
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self) 
 */
int st_top(const /*@in@*/ stack_t self)
{
  return self.elements[self.dim-1];
}

/** 
 * PRE: st_create(self)  &&  st_length(self)< STACK_SIZE
 */
void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ int elem)
{
  self->elements[self->dim] = elem;
  self->dim++;
}

/** 
 * PRE: st_create(self) && !(st_is_empty(self) 
 */
void st_pop(/*@inout@*/ stack_t *self)
{
  (self->dim)--;
}

/** 
 * PRE: st_create(self) 
 */
void st_show(const /*@in@*/ stack_t self)
{
  int i;
  printf("l=(%d)",st_length(self));
  printf("[");
  for (i=0; i<st_length(self) ; i++)
  {
    printf("%d ",self.elements[i]);
  }
  printf("]");
}


int main()
{
  stack_t s;

  st_create(&s);

  st_show(s);
  printf("\n");
  
  for (int i = 0; i < 10; i++) {
    st_push(&s, i);
  }
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("(%d)\n",st_length(s));
  printf("TOP=%d\n",st_top(s));
  for (int i = 0; i < 10; i++) {
    printf("%d \n",st_top(s));
    st_pop(&s);
  }
  st_show(s);
  printf("\n");
  return 0;
}

/*** EOF ***/
