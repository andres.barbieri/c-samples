#ifndef SSTACK_H_
#define SSTACK_H_ 1

#include <stdbool.h>       /** bool **/

#ifndef DATATYPE
#define DATATYPE int
#endif

#ifndef DATATYPEPRMASK
# define DATATYPEPRMASK "%d"
//# define DATATYPEPRMASK "%c"
#endif

#ifndef STACK_SIZE
#define STACK_SIZE   256
#endif

typedef struct st {
          DATATYPE elements[STACK_SIZE];
          int dim; 
} stack_t;


void st_create(stack_t *self);

bool st_is_empty(const /*@in@*/ stack_t self);

int st_length(const /*@in@*/ stack_t self);

DATATYPE st_top(const /*@in@*/ stack_t self);

void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ DATATYPE elem);

void st_pop(/*@inout@*/ stack_t *self);

void st_show(const /*@in@*/ stack_t self);

#endif

/*** EOF ***/
