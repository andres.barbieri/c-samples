#include <stdio.h>
#include "sstack.h"

int main()
{
  stack_t s;

  st_create(&s);

  st_show(s);
  printf("\n");
  
  for (int i = 0; i < 10; i++) {
    st_push(&s, i);
  }
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("TOP=%d\n",st_top(s));
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("(%d)\n",st_length(s));
  printf("TOP=%d\n",st_top(s));
  // Going out of range
  for (int i = 0; i < 10; i++) {
    printf("%d \n",st_top(s));
    st_pop(&s);
  }
  st_show(s);
  printf("\n");
  return 0;
}

/*** EOF ***/
