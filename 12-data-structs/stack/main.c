#include "stack.h"
#include <list.h>

int main()
{
  stack_t s = st_create();
  list_t  l = ls_create();
  
  for (int i = 0; i < 10; i++) {
    st_push(&s, i);
  }
  st_show(s);
  printf("\n");
  st_pop(&s);
  st_show(s);
  printf("\n");
  st_pop(&s);
  st_show(s);
  printf("\n");
  printf("(%d)\n",st_length(s));
  return 0;
}
