#include "stack.h"

stack_t st_create()
{
  return (stack_t) ls_create();
}

bool st_is_empty(const /*@in@*/ stack_t self)
{
  return ls_is_empty((list_t) self);
}

int st_length(const /*@in@*/ stack_t self)
{
  return ls_length((list_t) self);
}

DATATYPE st_top(const /*@in@*/ stack_t self)
{
  return ls_head((list_t)self);
}

void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ DATATYPE elem)
{
  ls_add_front((list_t*)self,elem);
}

void st_pop(/*@inout@*/ list_t *self)
{
  ls_remove((list_t*)self);
}

void st_show(const /*@in@*/ stack_t self)
{
  ls_show((list_t)self);
}

/*** EOF ***/
