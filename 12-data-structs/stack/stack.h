#ifndef _STACK_H_
#define _STACK_H_ 1

#include "list.h"          
#include <stdbool.h>       /** bool **/

typedef list_t stack_t;

stack_t st_create();

bool st_is_empty(const /*@in@*/ stack_t self);

int st_length(const /*@in@*/ stack_t self);

DATATYPE st_top(const /*@in@*/ stack_t self);

void st_push(/*@inout@*/ stack_t *self, const /*@in@*/ DATATYPE elem);

void st_pop(/*@inout@*/ stack_t *self);

void st_show(const /*@in@*/ stack_t self);

#endif

/*** EOF ***/
