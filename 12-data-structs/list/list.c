#include "list.h"

list_t ls_create()
{
  return (list_t) NULL;
}

bool ls_is_empty(const /*@in@*/ list_t self)
{
  return (self == (list_t) NULL);
}

int ls_length(const /*@in@*/ list_t self)
{
  if (ls_is_empty(self)) {
      return 0;
    } else {
    return 1 + ls_length(ls_tail(self));
  }
} 

int ls_includes(/*@in@*/ list_t self, const /*@in@*/ DATATYPE elem)
{
 if (ls_is_empty(self)) {
      return 0;
    } else {
   return (self->elem == elem) || ls_includes(ls_tail(self), elem);
 }
}

DATATYPE ls_head(const /*@in@*/ list_t self)
{
  return self->elem;
}

list_t ls_tail(const /*@in@*/ list_t self)
{
  return self->next;
}

void ls_add(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem)
{
  ls_add_front(self,elem);
}

void ls_add_front(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem)
{
  list_node_t *node;

  node = malloc(sizeof(list_node_t));
  node->elem = elem;
  node->next = (list_node_t*) (*self);
  (*self) = (list_t) node;
}

void ls_add_back(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem)
{
  list_node_t *node;

  if (ls_is_empty(*self)) {
    node = malloc(sizeof(list_node_t));
    node->elem = elem;
    node->next = (list_node_t*) NULL;
    (*self) = (list_t) node;
  } else {
    ls_add_back(&((*self)->next),elem);
  }
}

/* Precond: !ls_empty(self) */
DATATYPE ls_remove(/*@inout@*/ list_t *self)
{
  list_t aux;
  DATATYPE elem;

  elem = (*self)->elem;
  aux = (*self);
  (*self) = (list_t) (*self)->next;
  free(aux);
  return elem;
}

/*
 * status code:
 * 0: OK operaci'on con 'exito
 * -1: error, lista vac'ia
 * En el caso de status=-1 el valor del par'ametro 
 * elem debe ser ignroado
 */
void ls_remove_status(/*@inout@*/ list_t *self, 
                      /*@out@*/   DATATYPE *elem, 
                      /*@out@*/   int *status)
{
  if (!ls_is_empty(*self))
    {
      *status = 0;
      *elem = ls_remove(self);
    }
  else
    {
      *status = -1;
    }
}

char *ls_to_string(const /*@in@*/ list_t self)
{
  list_t aux = self;
  char *str; 
  int str_len;

  if (ls_is_empty(aux))
    {
      str_len = (sizeof(char) * 3);
      str = malloc((size_t)str_len);
      sprintf(str,"[]");
    }
  else
    {
      str_len = (sizeof(char) * MAXINTTOSTR * ls_length(aux));
      str = malloc((size_t)str_len); 
      str[0] = '\0';
      while (!ls_is_empty(aux)) {
        sprintf(str,PRNMASK(DATATYPEPRMASK), str, ls_head(aux));
	aux = ls_tail(aux);
      }
    }
  return str;
}

void ls_show(const /*@in@*/ list_t self)
{
  char *s = NULL;

  printf("l=(%d)",ls_length(self));
  s = ls_to_string(self);
  printf("%s",s);
  if (s!=NULL) 
    free(s);  
}

void ls_free(/*@inout@*/ list_t *self)
{
  int i;
  int len = ls_length(*self);
  list_t aux = NULL;

  if (len !=0)
    {
      for(i=0;i<len;i++)
	{
	  aux = (*self)->next;
	  free(*self);
	  (*self) = aux;
	}
    }
}  

void ls_iterate(const /*@in@*/ list_t self, DATATYPE func(DATATYPE))
{
  list_t aux = self;
  //while (aux)
  //while (aux !=NULL)
  while (!ls_is_empty(aux)) {
      aux->elem = (func(aux->elem));
      //aux = aux->next;
      aux = ls_tail(aux);
  }
}

list_t ls_select(const /*@in@*/ list_t self, int func(DATATYPE))
{
  list_t aux  = self;
  list_t newl = ls_create();
  //while (aux !=NULL)
  while (!ls_is_empty(aux)) {
    if (func(aux->elem)) ls_add_back(&newl, aux->elem);
      //aux = aux->next;
      aux = ls_tail(aux);
  }
  return newl;
}

DATATYPE ls_fold(const /*@in@*/ list_t self, DATATYPE func(DATATYPE,DATATYPE), DATATYPE zero)
{
  DATATYPE base = zero;
  list_t aux = self;
  //while (aux)
  //while (aux !=NULL)
  while (!ls_is_empty(aux)) {
    base = func(base, aux->elem);
      //aux = aux->next;
      aux = ls_tail(aux);
  }
  return base;
}

/*** EOF ***/

