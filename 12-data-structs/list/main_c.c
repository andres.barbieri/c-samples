#include "list.h"

int main()
{
  list_t l  = NULL;
  int    code;
  char   tmp;

  ls_show(l);   
  printf("\n");
  for (int i = 'a'; i < 'f'; i++) {
    ls_add_front(&l, i);
  }
  ls_show(l);
  printf("\n");
  ls_remove(&l);
  ls_show(l);
  printf("\n");
  ls_remove(&l);
  ls_show(l);
  printf("\n");
  for (int i = 'Q'; i <= 'W'; i++) {
    ls_add_back(&l, i);
  }
  ls_show(l);
  printf("\n");
  for(int i = 0;i<20;i++) {
    ls_remove_status(&l,&tmp,&code);
    printf("%d ",code);
  }
  printf("\n");
  ls_show(l);
  printf("\n");
  ls_free(&l);
  return 0;
}
