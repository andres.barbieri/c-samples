#include "dlist.h"

int main()
{
  list_t l  = NULL;

   ls_show(l);printf("\n");
   ls_add_front(&l, 200);
   ls_show(l);printf("\n");

  for (int i = 0; i < 10; i++) {
    ls_add_front(&l, i);
  }
  ls_show(l);
  printf("\n");
  ls_remove(&l);
  ls_show(l);
  printf("\n");
  ls_remove(&l);
  ls_show(l);
  printf("\n");
  for (int i = 100; i <= 102; i++) {
    ls_add_back(&l, i);
  }
  ls_show(l);
  printf("\n");
  ls_free(&l);
  return 0;
}
