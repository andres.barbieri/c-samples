#ifndef MY_LIST_H_
#define MY_LIST_H_ 1
#include <stdlib.h>        /** malloc / free **/
#include <stdbool.h>       /** bool **/
#include <stdio.h>         /** printf **/

#ifndef DATATYPEPRMASK
//# define DATATYPEPRMASK "%d"
# define DATATYPEPRMASK "%c"
#endif

#ifndef DATATYPE
//# define DATATYPE int
# define DATATYPE char
#endif

#ifndef MAXINTTOSTR
# define MAXINTTOSTR 8
#endif

#define PRNMASK(string) "%s "string
// PRNMASK(DATATYPEPRMASK)

typedef struct list_node {
         DATATYPE           elem;
         struct list_node   *next;
} list_node_t;

typedef list_node_t *list_t;

list_t ls_create();

bool ls_is_empty(const /*@in@*/ list_t self);

int ls_length(const /*@in@*/ list_t self);

int ls_includes(/*@in@*/ list_t self, const /*@in@*/ DATATYPE elem);

DATATYPE ls_head(const /*@in@*/ list_t self);

list_t ls_tail(const /*@in@*/ list_t self);

void ls_add(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem);

void ls_add_front(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem);

void ls_add_back(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem);

/* Precond: !ls_empty(self) */
DATATYPE ls_remove(/*@inout@*/ list_t *self);

/*
 * status code:
 * 0: OK operaci'on con 'exito
 * -1: error, lista vac'ia
 * En el caso de status=-1 el valor del par'ametro 
 * elem debe ser ignroado
 */
void ls_remove_status(/*@inout@*/ list_t *self, 
                      /*@out@*/   DATATYPE *elem, 
                      /*@out@*/   int *status);

char *ls_to_string(const /*@in@*/ list_t self);

void ls_show(const /*@in@*/ list_t self);

void ls_free(/*@inout@*/ list_t *self);

void ls_iterate(const /*@in@*/ list_t self, DATATYPE func(DATATYPE));

list_t ls_select(const /*@in@*/ list_t self, int func(DATATYPE));

DATATYPE ls_fold(const /*@in@*/ list_t self, DATATYPE func(DATATYPE,DATATYPE), DATATYPE zero);

#endif /* list.h */

/*** EOF ***/
