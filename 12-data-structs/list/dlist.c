#include "dlist.h"

list_t ls_create()
{
  return (list_t) NULL;
}

bool ls_is_empty(const /*@in@*/ list_t self)
{
  return (self == (list_t) NULL);
}

int ls_length(const /*@in@*/ list_t self)
{
  if (ls_is_empty(self)) {
      return 0;
    } else {
    return 1 + ls_length(ls_tail(self));
  }
} 

DATATYPE ls_head(const /*@in@*/ list_t self)
{
  return self->elem;
}

list_t ls_tail(const /*@in@*/ list_t self)
{
  return self->next;
}

list_t ls_prev(const /*@in@*/ list_t self)
{
  return self->prev;
}


void ls_add(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem)
{
  ls_add_front(self,elem);
}

void ls_add_front(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem)
{
  list_node_t *node;

  node = malloc(sizeof(list_node_t));
  node->elem = elem;
  node->next = (list_node_t*) (*self);
  node->prev = (list_node_t*) NULL;
  if (!ls_is_empty(*self)) 
    {
      (*self)->prev = node;
    }
  (*self) = (list_t) node;
}

void ls_add_back(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem)
{
  list_node_t *node;
  list_t      prev;
   
  while (!ls_is_empty(*self)) 
  {
    prev = *self;
    (*self) = (list_t) (*self)->next;
  }
  node = malloc(sizeof(list_node_t));
  node->elem = elem;
  node->prev = (list_node_t*) prev;
  node->next = (list_node_t*) NULL;
}

DATATYPE ls_remove(/*@inout@*/ list_t *self)
{
  list_t aux;
  DATATYPE elem;

  elem = (*self)->elem;
  aux = (*self);
  (*self) = (list_t) (*self)->next;
  if (!ls_is_empty(*self))
    {
      (*self)->prev = NULL;
    }
  free(aux);
  return elem;
}


char *ls_to_string(const /*@in@*/ list_t self)
{
  list_t aux = self;
  char *str; 
  int str_len;

  if (ls_is_empty(aux))
    {
      str_len = (sizeof(char) * 3);
      str = malloc((size_t)str_len);
      sprintf(str,"[]");
    }
  else
    {
      str_len = (sizeof(char) * MAXINTTOSTR * ls_length(aux));
      str = malloc((size_t)str_len); 
      str[0] = '\0';
      while (!ls_is_empty(aux)) {
        sprintf(str,PRNMASK(DATATYPEPRMASK), str, ls_head(aux));
	aux = ls_tail(aux);
      }
    }
  return str;
}

void ls_show(const /*@in@*/ list_t self)
{
  char *s = NULL;

  printf("l=(%d)",ls_length(self));
  s = ls_to_string(self);
  printf("%s",s);
  if (s!=NULL) 
    free(s);  
}

void ls_free(/*@inout@*/ list_t *self)
{
  int i;
  int len = ls_length(*self);
  list_t aux = NULL;

  if (len !=0)
    {
      for(i=0;i<len;i++)
	{
	  aux = (*self)->next;
	  free(*self);
	  (*self) = aux;
	}
    }
}  

/*** EOF ***/

