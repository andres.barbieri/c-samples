#ifndef _DLIST_H_
#define _DLIST_H_ 1
#include <stdlib.h>        /** malloc / free **/
#include <stdbool.h>       /** bool **/
#include <stdio.h>         /** printf **/

#ifndef DATATYPEPRMASK
# define DATATYPEPRMASK "%d"
#endif

#ifndef DATATYPE
# define DATATYPE int
#endif

#ifndef MAXINTTOSTR
# define MAXINTTOSTR 8
#endif

#define PRNMASK(string) "%s "string
// PRNMASK(DATATYPEPRMASK)

typedef struct list_node {
         DATATYPE           elem;
         struct list_node   *next;
         struct list_node   *prev;
} list_node_t;

typedef list_node_t *list_t;

list_t ls_create();

bool ls_is_empty(const /*@in@*/ list_t self);

int ls_length(const /*@in@*/ list_t self);

DATATYPE ls_head(const /*@in@*/ list_t self);

list_t ls_tail(const /*@in@*/ list_t self);

list_t ls_prev(const /*@in@*/ list_t self);

void ls_add(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem);

void ls_add_front(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem);

void ls_add_back(/*@inout@*/ list_t *self, const /*@in@*/ DATATYPE elem);

DATATYPE ls_remove(/*@inout@*/ list_t *self);

char *ls_to_string(const /*@in@*/ list_t self);

void ls_show(const /*@in@*/ list_t self);

void ls_free(/*@inout@*/ list_t *self);

#endif

/*** EOF ***/
