# Programa m�nimo en el lenguaje "C"

Ejemplo de programa m�nimo en el lenguaje "C"

## Primeros pasos

Encontrar que no esta bien o fuera de acuerdo con el est�ndar en los archivos .c.
Encontrar que acepta el compilador y que salida da.
Probar con algunos flags y ver los warnings se obtienen.
Ver que hace finalmente el programa.
Cu�l es el "std=" default que utiliza ?

### Pre-requisitos

Compilador GCC

## Pruebas

Compilar: main1.c, main2.c ... main8.c de las siguientes formas:

```console
 gcc main[X].c
 gcc -Wall main[X].c
 gcc -Wall main[X].c -o main
 gcc -std=c89 -Wall main[X].c -o main.exe
 gcc -std=gnu89 -Wall main[X].c -o main.exe
 gcc -std=c99 -Wall main[X].c
 gcc -std=c18 -Wall main[X].c 
```

Por ejemplo:

```console
 gcc -Wall main3.c -o main
```

Probar ejecutando:

```console
./main; echo $?
```

Otros ejemplos:

```console
 gcc -Wall main4.c -o main
 ./main && echo "OK"
 ./main || echo "FAIL"
 gcc -Wall main5.c -o main
 ./main && echo "OK"
```

