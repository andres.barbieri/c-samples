# Código base para ayudar en el TPI 2020 "C"

Este código intenta ayudar a los alumnos a poder completar el TPI 2020. NO es obligatorio su uso. Se puede usar parcial o totalmente, completando sus correspondientes espacios, copiando y pegando partes, tomando ideas o directamente descartarlo.

## Organización

Los archivos `.m` y `.m2` son matrices de testing. Se pueden crear nuevas.

* [`list`](https://gitlab.com/andres.barbieri/c-samples/-/tree/master/2020-tpi/c-matrix-share-to-complete/list): interfaz y código de lista a completar.

* `mat.c` `mat.h` `error-mat.c` `error-mat.h`:  código de matrices a completar.

* ` main-test.c` `test-props.c`: programas de testing.

* `Makefile` para compilar el proyecto.

## Modo de uso

Se puede analizar el código fuente incluido en cada directorio y probar corriendo

```console
$ git clone https://gitlab.com/andres.barbieri/c-samples.git c-samples/2020-tpi/c-matrix-share-to-complete
$ cd c-samples/2020-tpi/c-matrix-share-to-complete
$ cat README.md
$ make clean
$ make test
$ ...
```

Tener en cuenta que el archivo para el testing del código proveído es la entrada `test7.m`. Se requiere matrices cuadradas para los test que se aportan. Pueden usar también los archivos main para testear su código y la matriz `test7.m`. También tener en mente que el código de los programas principales no tienen los controles de los valores de retorno para todos los casos.

