#include <stdio.h>

#define MAX_LEN        30
#define A_LEN          10

// Compilar con diferentes definiciones y ver resultados
#ifndef B_NEQ_A
#define B_LEN          10
#else
#define B_LEN           8
#endif

int main()
{
  int    int_vector2[MAX_LEN];                      /* No init */                    
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  int    b[B_LEN] = {0, 1, 2};                      /* Incomplete Init will be filled with 0 */
  int    i;

  /* No checking for array boundary */
  i = int_vector2[8];
  printf("i=int_vector2[8]=%d\n",i);
  i = a[A_LEN];
  printf("i=a[%d]=%d\n",A_LEN,i);
  i = a[A_LEN+1];
  printf("i=a[%d]=%d\n",A_LEN+1,i);
  i = b[-1];
  printf("i=b[%d]=%d\n",-1,i);
  i = b[A_LEN];
  printf("i=b[%d]=%d\n",A_LEN,i);
  i = b[B_LEN+1]; 
  printf("i=b[%d]=%d\n",B_LEN+1,i);
  return 0;
}
