#include <stdio.h>

#define MAX_LEN        30
#define A_LEN          10
#define B_LEN          10

int global_vector[MAX_LEN]; /* Init with 0 */
// int global_vector[MAX_LEN] = {10};  /* { 10, 0, 0, 0, .... } */

int main()
{
  int    int_vector[30];
  float  flt_vector[30];
  int    int_vector2[MAX_LEN]; 
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  int    b[B_LEN] = {0, 1, 2}; /* Incomplete init will be filled with 0 */
  int    i;

  // Array dimmensions
  // typeof(sizeof) == long unsigned int
  printf("sizeof(int_vect)==%u\n",(unsigned int)sizeof(int_vector));
  printf("sizeof(int_vect2)==%u==%u\n",(unsigned int)sizeof(int_vector2),(unsigned int)(MAX_LEN*sizeof(int)));
  printf("sizeof(a)=%u\n",(unsigned int)sizeof(a));

   // Array accessing
   printf("global_vector[%d]=%d..global_vector[%d]=%d\n",0, global_vector[0], MAX_LEN-1, global_vector[MAX_LEN-1]);    
   printf("int_vector2[%d]=%d..int_vector2[%d] =%d\n"   ,0, int_vector[0]   , MAX_LEN-1, int_vector[MAX_LEN-1]);
   printf("flt_vector[%d]=%f\n"                         ,0, flt_vector[0]);
   // A's elements    
   printf("##\n");  for(i=0;i<A_LEN;i++)   printf("a[%d]=%d\n",i,a[i]);
   // A's elements in reverse order
   printf("##\n"); for(i=A_LEN-1;i>=0;i--) printf("a[%d]=%d\n",i,a[i]);
   // B's elements
   printf("##\n");for(i=0;i<B_LEN;i++)     printf("b[%d]=%d\n",i,b[i]);

  return 0;
}
