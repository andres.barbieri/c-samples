#include <stdio.h>

#define MAX_LEN        30
#define A_LEN          10
#define B_LEN          10


int main()
{
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  int    b[B_LEN] = {0, 1, 2};                      /* Incomplete Init will be filled with 0 */

  a[-0x40000000] = MAX_LEN;
  return 0;
}
