// Are all the same

int func(int x[]);
int func(int x[10]); 
int func(int *x);


// Arrays in C are passed by #reference" actually they are
// pointers , not by value/copy

#include <stdio.h>
int func3 (int x[] , int y[10] , int *z )
{
  printf("sizeof(x)=%3ld sizeof(y)=%3ld sizeof(z)=%3ld\n",
          sizeof(x), sizeof(y), sizeof(z));
  return 0;
}

int main () 
{
  int x [35];
  int y [20];
  int z [10];
  printf("sizeof(x)=%3ld sizeof(y)=%3ld sizeof(z)=%3ld\n",
	 sizeof(x), sizeof(y), sizeof(z));
  func3(x,y,z);
return 0;
}
