#include <stdio.h>

#define MAX_LEN        30
#define A_LEN          10
#define B_LEN          10


/* Compilar y probar con i definida en diferentes contextos */
int i;

int main()
{
  //int    i;
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  int    b[B_LEN] = {0, 1, 2};                      /* Incomplete Init will be filled with 0 */
  //int    i;

  // Inconditional iterator 
  //for(int i=0;;i--) { 
  for(i=0;;i--) { 
    printf("a[%d]=%d ",i,a[i]);
    // Overwrites index i (??)
        a[i] = MAX_LEN;
  }
  return 0;
}
