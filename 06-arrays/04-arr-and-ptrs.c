#include <stdio.h>

#define MAX_LEN        30
#define A_LEN          10
#define B_LEN          10

int global_vector[MAX_LEN]; /* Init with 0 */

int main()
{
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  int    b[B_LEN] = {0, 1, 2}; /* Incomplete init will be filled with 0 */

  printf("a) a %p = &a[0] %p = &a %p\n",a, &a[0], &a);
  printf("b) b %p = &b[0] %p = &b %p\n",b, &b[0], &b);

  printf("a[0] %d  = *a %d\n",a[0], *a);
  printf("b[2] %d  = *(b+2) %d\n",b[2], *(b+2));

  a[0] = 10;
  printf("a[0] %d = *a %d\n",a[0], *a);

  *(a+4) = 11;
  printf("a[4] %d = *(a+4) %d\n",a[4], *(a+4));

  return 0;
}
