#include <stdio.h>

#define A_LEN          10
#define B_LEN          10


int main()
{
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  char   b[B_LEN] = {0, 1, 2}; /* Incomplete init will be filled with 0 */
  int    i;
 
  
   // A's elements    
   printf("##\n");  for(i=0;i<A_LEN;i++)   printf("a[%d]=%d\n",i,a[i]);
   printf("##\n");  for(int j=0;j<A_LEN;j++)   printf("a[%d]=%d\n",j,a[j]);
   printf("##\n");  for(int *aux=a;(aux!=(a+A_LEN));aux++) printf("[%p]=%d\n",aux,*aux);
   {
      int *aux; 
      printf("##\n");  aux=a , printf("a wrong inc: %ld\n",(long int)((aux+1)-aux) );
      printf("##\n");  aux=a , printf("a inc:       %ld\n",((long int)(aux+1))-((long int)aux) );
   }
   // A's elements in reverse order
   printf("##\n"); for(i=A_LEN-1;i>=0;i--) printf("a[%d]=%d\n",i,a[i]);
   printf("##\n");  for(int *aux=(a+A_LEN-1);(aux!=a);aux--) printf("[%p]=%d\n",aux,*aux);
   // B's elements
   printf("##\n");for(i=0;i<B_LEN;i++)     printf("b[%d]=%d\n",i,b[i]);
   printf("##\n");  for(char *aux=b;(aux!=(b+A_LEN));aux++) printf("[%p]=%d\n",aux,*aux);
   {
     char *aux;
     printf("##\n");  aux=b , printf("b inc: %ld\n",((long int)(aux+1))-((long int)aux) );
   }
  return 0;
}
