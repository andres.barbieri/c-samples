#include <stdio.h>

#define MAX_LEN        30
#define A_LEN          10

// Compilar con diferentes definiciones y ver resultados
#ifndef B_NEQ_A
#define B_LEN          10
#else
#define B_LEN           8
#endif

int main()
{
  int    int_vector2[MAX_LEN];                      /* No init */                    
  int    a[A_LEN] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}; /* Full init */
  int    b[B_LEN] = {0, 1, 2};                      /* Incomplete Init will be filled with 0 */
  int    i;

  /* No checking for array boundary */
  int_vector2[8] = 0xff;
  a[A_LEN]       = 0xfe;
  a[A_LEN+1]     = 0xfd;
  a[A_LEN+5]     = 0xfd;
  a[A_LEN+6]     = 0xfc;
  b[-1]          = 0xfb;
  b[-10]         = 0xfb;
  b[A_LEN]       = 0xfa;

  printf("int_vector2[%d]=%X",8,int_vector2[8]);
  printf("\n##\n");  
  for(i=0;i<A_LEN;i++) printf(" a[%d]=%X",i,a[i]);
  printf("\n##\n");  
  for(i=0;i<B_LEN;i++) printf(" b[%d]=%X",i,b[i]);
  printf("\n");  
  return 0;
}
