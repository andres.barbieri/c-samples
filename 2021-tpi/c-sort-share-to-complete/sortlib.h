// Complete me

#include <stdio.h>
#include "error-sort.h"

int sort_file  (/*@ in @*/  FILE *fpin, 
                /*@ in @*/  FILE *fpout,   
                /*@ in @*/ int   flag_r,
                /*@ in @*/ int   flag_s,
                /*@ in @*/ int   flag_c);

