/***
 *   
 * FILE NAME:   sort-stub.c
 *
 * PURPOSE:     
 *
 * Programa de referencia para ordenador de archivos           
 *
 * FILE REFERENCES:        
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @file      <fname>            <i|o|io>  <fdesc>
 * 
 * EXTERNAL REFERENCES:
 *
 *            Name               I/O       Description             
 *            ----               ---       -----------
 * @external  <eref>             <i|o|io>  <edesc> 
 *
 * MAIN: 
 *
 * $Author:   user$ email@domain
 *
 * $Revision: 1.1$
 *
 * $Date:     DD/MM/YYYY$
 *
 * REQUIREMENTS/FUNCTIONAL SPECIFICATIONS:
 *
 * <freqs>
 *
 * NOTES:
 *
 * <notes>
 *
 * DEVELOPMENT HISTORY:
 *
 * Date        Author     Revision    Release   Description  
 * ----        ------     --------    -------   -----------
 * DD/MM/YYYY  user          0.1       -      Start
 *
 *	                                   
 */

#include <stdio.h>                      /* printf */
#include <stdlib.h>                     /* exit */
#include <getopt.h>                     /* getopt */
#include <stdbool.h>                    /* Booleans */
#include "error-sort.h"
#include "sortlib.h"

extern char *optarg;
extern int  opterr;
//extern int optind, opterr, optopt;

// Please, complete me
#define OP_ARG_I          'i'
#define OP_ARG_I_LONG     "in"

#define OP_ARG_O          'o'
#define OP_ARG_O_LONG     "out"

#define OP_ARG_R          'r'
#define OP_ARG_R_LONG     "reverse"

#define OP_ARG_S          's'
#define OP_ARG_S_LONG     "shuffle"

#define OP_ARG_C          'c'
#define OP_ARG_C_LONG     "count"

#define OP_HLP            'h'
#define OP_HLP_LONG       "help"

// Please, complete me
#define OPTION_STRING "i:o:rsch"

// Please, complete me
static struct option long_options[] = 
              {
              {OP_ARG_I_LONG  , required_argument, 0, (int) OP_ARG_I },
              {OP_ARG_O_LONG  , required_argument, 0, (int) OP_ARG_O },
	      {OP_ARG_R_LONG,   no_argument,       0, (int) OP_ARG_R },
	      {OP_ARG_S_LONG,   no_argument,       0, (int) OP_ARG_S },
	      {OP_ARG_C_LONG,   no_argument,       0, (int) OP_ARG_C },
	      {OP_HLP_LONG,     no_argument,       0, (int) OP_HLP },
              {0, 0, 0, 0}
              };

static void usage(char *argv[], int code)
{
  // PROGRAMA --in file1.txt --out file2.txt [flags]
  // Please, complete me.
  printf("usage: %s [[--in|-i]  <input-file>]  \n"
         "          [[--out|-o] <output-file>] \n"
	 "          [-r|--reverse] [-s|--shuffle] [-c|--count]\n",
	 argv[0]);
 exit(code);
}

/***
 * Parse Common arguments
 */
static int parse_args(/*@ in @*/  int  argc, 
	 	      /*@ in @*/  char *argv[],
		      /*@ out @*/ char **arg_i,
		      /*@ out @*/ char **arg_o,
		      /*@ out @*/ int  *flag_r,
		      /*@ out @*/ int  *flag_s,
		      /*@ out @*/ int  *flag_c)
{
     int  res;
     int arg_i_count  = 0;
     int arg_o_count  = 0;
     
     opterr = 0; // Supress getopt errors
     *arg_i      = NULL;
     *arg_o      = NULL;     
     *flag_r = *flag_s = *flag_c = 0;
          
     while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
     //while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
     {
      if  (res == (int) OP_ARG_I)
	{
          // Take care, RO
	  *arg_i = optarg;
          arg_i_count++; 
	}
      else if  (res == (int) OP_ARG_O)
	{
          // Take care, RO
	  *arg_o = optarg;
          arg_o_count++; 
	}
      else if  (res == (int) OP_ARG_R)
	{
	  *flag_r = 1;
	}
      else if  (res == (int) OP_ARG_S)
	{
	  *flag_s = 1;
	}
      else if  (res == (int) OP_ARG_C)
	{
	  *flag_c = 1;
	}
      else if  (res == (int) OP_HLP)
	{
	  return (int) false;
	}
      else 
	{
          // Invalid Option, res ==  '?' 
	  return (int) false;
	}
     }
     /****     
     fprintf(stderr," %s %s %d %d %d %d %d\n",*arg_i, *arg_o, arg_i_count, arg_o_count,
	     (arg_i_count > 1) || (arg_o_count > 1) , ( (*flag_c) && ((*flag_s) || (*flag_r)) ), ( (*flag_s) && (*flag_r) ) );          
     fprintf(stderr,"%d %d\n",argc,optind);
     *****/
     res =          ( (arg_i_count > 1) || (arg_o_count > 1)    || 
		    ( (*flag_c) && ((*flag_s) || (*flag_r)) ) ||
		      ((*flag_s) && (*flag_r)) ||
		      (argc != optind) );
     return !res;
}


int main(int argc, char *argv[])
{
  FILE *fpin, *fpout;
  char *arg_i; 
  char *arg_o;
  int  flag_r,flag_s,flag_c;

 if (!parse_args(argc, argv, &arg_i, &arg_o, &flag_r, &flag_s, &flag_c))
  {
    usage(argv, EXIT_FAILURE);
  }

 // Open file
 if ( (arg_i !=NULL) && (! (fpin = fopen(arg_i,"r")) ) )
   {
     output_error(stderr, -E_FILE_ERROR);
     perror(" ");
     exit(-E_FILE_ERROR);  
   }

 // Open file
 if ( (arg_o !=NULL) && (! (fpout = fopen(arg_o,"w")) ) )
   {
     output_error(stderr, -E_FILE_ERROR);
     perror(" ");
     exit(-E_FILE_ERROR);  
   }
 if  (arg_i==NULL)
   {
     fpin = stdin;
     arg_i = "STDIN";
   }

 if  (arg_o==NULL)
   {
     fpout = stdout;
     arg_o = "STDOUT";
   }
#ifdef DEBUG 
 fprintf(stderr,"in: %s out: %s\n",arg_i,arg_o);
#endif
 sort_file(fpin,fpout,flag_r,flag_s,flag_c);
 fclose(fpin);fclose(fpout);
 return (0);
}

