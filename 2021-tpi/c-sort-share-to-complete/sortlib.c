// Complete me
#include "sortlib.h"

/***
 * Stub sort file
 */
int sort_file  (/*@ in @*/  FILE *fpin, 
		/*@ in @*/  FILE *fpout,   
		/*@ in @*/ int   flag_r,
		/*@ in @*/ int   flag_s,
		/*@ in @*/ int   flag_c)
{
#ifdef DEBUG   
  fprintf(stderr,"flags: %d %d %d\n",flag_r,flag_s,flag_c);
#endif
  return 0;
}


