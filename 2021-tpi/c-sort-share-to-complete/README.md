# Código base para ayudar en el TPI 2021 "C"

Este código intenta ayudar a los alumnos a poder completar el TPI 2021. No es obligatorio su uso. Se puede usar parcial o totalmente, completando sus correspondientes espacios, copiando y pegando partes, tomando ideas o directamente descartarlo. Esto es solo un esqueleto de referencia y ayuda, como se indico NO necesariamente debe usarse ni implementarse de esta manera.

## Organización

* `sortlib.c` `sortlib.h` `error-sort.c` `error-sort.h` `strvector.c` `strvector.h`:  código a competar.

* `sort-stub.c`: programas de testing para procesamiento de argumentos.

* `Makefile` para compilar el proyecto.

## Modo de uso

Se puede analizar el código fuente incluido en cada directorio y probar corriendo

```console
$ git clone https://gitlab.com/andres.barbieri/c-samples.git 
$ cd c-samples/2021-tpi/c-sort-share-to-complete
$ cat README.md
$ make clean
$ make -i test
$ ...
```



