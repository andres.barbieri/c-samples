#include <stdio.h>

// External and static variables are initialized to zero by default, this is guaranteed. 
// Automatic and register variables that do not have en explicit initializer will have an 
// indeterminate value (either an unspecified value or a trap representation).

// Buena pr�ctica, iniciar punteros a NULL


#ifdef ptr02_GLOBAL
int *pp;
#endif

int main()
{
  // Declaraci�n - Declaration
#ifndef ptr02_GLOBAL
  int *pp; 
#endif
  int *p;

  printf("sizeof(p)=%lu sizeof(pp)=%lu\n",sizeof(p),sizeof(pp));
  printf("value of p=%p value of pp=%p\n",p,pp);

  return 0;
} 
