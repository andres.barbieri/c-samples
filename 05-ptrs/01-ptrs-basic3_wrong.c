/*
 * Programa para introducir punteros en "C"
 * y entender lo que se puede hacer con los mismos
 * y posibles errores que se pueden cometer
 *
 */

#include <stdio.h>

int main()
{
  // Declaración - Declaration

  int           *p;              // puntero a un entero
  char          *q    = NULL, c; // puntero a char y un char
  int            i    = 10;
  int            *r   = NULL;
  int            j;

  printf("sizeof(NULL) = %18lu valueof(NULL) = %16p|%lu\n",sizeof(NULL),NULL,(long unsigned int)NULL);
  printf("sizeof(p)    = %18lu sizeof(q)     = %18lu\n",sizeof(p),sizeof(q));
  printf("valueof(p)   = %18p valueof(q)    = %18p\n",p,q);
  printf("sizeof(&i)   = %18lu valueof(&i)   = %18p\n",sizeof(&i),&i);

  printf("valueof(*p)  = %18d\n",*p);  //  Problema  
  printf("valueof(*q)  = %18c\n",*q);  //  Problema

  // Asignación - Assignment (Dirección)
  p = r;  
  q = &c;
  p = (int *)0x50;                       // Problema

  // De-referenciación - Dereference (Contenido)
  //*p = 50;                               // Problema
  printf("*p          = %18d\n",*p);     // Problema
  j  = *p;                               // Problema
  j++;
  printf("i          = %18d\n",i);
  //printf("*p         = %18d\n",*p);
  printf("j          = %18d\n",j);
  i  = c;
  printf("i          = %18d\n",i);
  //printf("*p         = %18d\n",*p);
  printf("j          = %18d\n",j);
  return 0;
} 
