/*
 * Programa para introducir punteros en "C"
 *
 * Encontrar posibles errores. 
 * A que valores se inicializan las variables?
 *
 * Complilar con diferntes arch i386, x64
 *
 */

// Try with: gcc -o 01-ptrs-basic11 01-ptrs-basic1.c -Wall -m32
//           gcc -o 01-ptrs-basic11 01-ptrs-basic1.c -Wall [-m64]
// Before:   install libc6-dev-i386

#include <stdio.h>

int main()
{
  // Declaraci'on - Declaration

  int           *p;     // puntero a un entero - int ptr
  char          *q , c; // puntero a char y un char 
  int            i;
  int            *r;
  int            j;

  // Inspeccionar punteros
  // 64bits: long unsigned int::sizeof
  // 32bits:  unsigned int::sizeof
  printf("sizeof(p)  = %18lu sizeof(q)   = %18lu\n",sizeof(p),sizeof(q));
  printf("valueof(p) = %18p valueof(q)  = %18p\n",p,q);
  printf("sizeof(i)  = %18lu valueof(i)  = %18d\n",sizeof(i),i);
  printf("sizeof(&i) = %18lu valueof(&i) = %18p\n",sizeof(&i),&i);

  // Asignaci'on - Assignment (Direcci'on)
  i = p; // assignment makes pointer from integer without a cast (enabled by default)
  p = i; // assignment makes integer from pointer without a cast (enabled by default)
  q = p; // assignment from incompatible pointer type (enabled by default) 
  q = (char*) p;
  p = r; 
  q = &c;
  p = (int *)0x7ffde5bcb594;
  i = 10;
  p = &i; 
  
  // De-referenciaci'on - Dereference (Contenido)
  printf("i          = %18d\n",i);
  *p = 50;
  printf("i          = %18d\n",i);
  j  = *p;
  j++;
  printf("i          = %18d\n",i);
  printf("*p         = %18d\n",*p);
  printf("j          = %18d\n",j);
  i  = c;
  printf("i          = %18d\n",i);
  printf("*p         = %18d\n",*p);
  printf("j          = %18d\n",j);

  // Aritmetica de Punteros 
  printf("*p         = %18d %18p\n",*p, p);
  p++;
  printf("*p         = %18d %18p\n",*p, p); // problemas ?
  
  return 0;
}
