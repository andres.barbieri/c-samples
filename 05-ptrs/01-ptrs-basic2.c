/*
 * Programa para introducir punteros en "C"
 * y entender NULL y la inicializaci'on de los punteros
 *
 */

// Where is NULL defined?
//
// # less /usr/include/stdlib.h
//   ...
// /* Get size_t, wchar_t and NULL from <stddef.h>.  */
// #define         __need_size_t
// #ifndef __need_malloc_and_calloc
// # define        __need_wchar_t
// # define        __need_NULL
// #endif
// #include <stddef.h>
// 
// # cat /usr/include/linux/stddef.h
// #ifndef _LINUX_STDDEF_H
// #define _LINUX_STDDEF_H
// 
// 
// 
// #undef NULL
// #if defined(__cplusplus)
// #define NULL 0
// #else
// #define NULL ((void *)0)
// #endif
// 
// 
// #endif
// 

// Buena pr'actica, iniciar punteros a NULL

#include <stdio.h>

int main()
{
  // Declaración - Declaration

  int           *p    = NULL; // puntero a un entero
  char          *q    = NULL , c; // puntero a char y un char
  int            i    = 10;
  int            *r   = NULL;
  int            j;

  // Inspeccionar punteros
  printf("sizeof(NULL) = %18lu valueof(NULL) = %16p|%lu\n",sizeof(NULL),NULL,(long unsigned int)NULL);
  printf("sizeof(p)    = %18lu sizeof(q)     = %18lu\n",sizeof(p),sizeof(q));
  printf("valueof(p)   = %18p valueof(q)    = %18p\n",p,q);
  printf("sizeof(&i)   = %18lu valueof(&i)   = %18p\n",sizeof(&i),&i);

  // Asignación - Assignment (Dirección)

  p = r;  
  q = &c;
  p = (int *)0x7ffde5bcb594;
  p = &i; 
 
  // De-referenciación - Dereference (Contenido)
  printf("i          = %18d\n",i);
  *p = 50;
  printf("i          = %18d\n",i);
  j  = *p;
  j++;
  printf("i          = %18d\n",i);
  printf("*p         = %18d\n",*p);
  printf("j          = %18d\n",j);
  i  = c;
  printf("i          = %18d\n",i);
  printf("*p         = %18d\n",*p);
  printf("j          = %18d\n",j);
  
  return 0;
} 
