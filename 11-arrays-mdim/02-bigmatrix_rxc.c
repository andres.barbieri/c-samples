/*
 * Programa para ilustrar acceso a matrices en "C"
 * 
 * Declara matriz grande que sobre-pasa stack size
 * Para correr probablemente debe habilitar el uso m'aximo del strack
 * con:
 *
 * $ ulimit -a | grep stack
 * stack size              (kbytes, -s) 8192
 *
 * $ ulimit -s unlimited
 * ulimit -a | grep stack
 * stack size              (kbytes, -s) unlimited
 *
 * Sino se obtiene:
 * 
 * $ ./02-bigmatrix_rxc 
 * Segmentation fault
 *
 * Ver la diferencia de tiempos de ejecuci'on accediendo RXC o CXR
 *
 * $ time ./02-bigmatrix_rxc 
 * real	0m0.920s
 * user	0m0.815s
 * sys	0m0.104s
 *
 * $ time ./02-bigmatrix_rxc 
 * 
 * real	0m2.660s
 * user	0m2.535s
 * sys	0m0.124s
 * 
 */

#include <stdio.h>

#ifdef SMATRIX
#define LEN1  150
#define LEN2  150
#else
#define LEN1  15000
#define LEN2  15000
#endif

void fillMatrix_rxc(int m[LEN1][LEN2], unsigned int topr,unsigned int topc, int value)
{
  int i,j;

  for(i=0;i<topr;i++) 
    {
      for(j=0;j<topc;j++) 
	{
	  m[i][j] = value;
	}
    }
}

void fillMatrix_cxr(int m[LEN1][LEN2], unsigned int topr,unsigned int topc, int value)
{
  int i,j;

  for(i=0;i<topc;i++) 
    {
      for(j=0;j<topr;j++) 
	{
	  m[j][i] = value;
	}
    }
}

int main()
{
  int matrix[LEN1][LEN2];

#ifdef RXC // Acceso correcto 
  fillMatrix_rxc(matrix,LEN1,LEN2,10);
#else // Acceso incorrecto, muchos cache misses
  fillMatrix_cxr(matrix,LEN1,LEN2,10);
#endif
  return 0;
}
