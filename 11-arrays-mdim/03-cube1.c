#include <stdio.h>

#define LEN1  4
#define LEN2  2
#define LEN3  2

/***
Arrange in memory:

[0 ,0, 0 ] [0 ,1, 0 ] [0 ,2, 0 ] ... [0   ,LEN2-1, 0]
[1 ,0, 0 ] [1 ,1, 0 ] [1  2, 0 ] ... [1   ,LEN2-1, 0]
...
[LEN1,0,0] [LEN1,1,0] [LEN1,2,0] ... [LEN1,LEN2-1, 0]

1st part
1st row
2nd row
3rd row
4th row
...
n-th row

2nd part
1st row
2nd row
3rd row
4th row
...
n-th row

***/

int getElem(int m[LEN1][LEN2][LEN3], unsigned int x,unsigned int y, unsigned z)
{
 
  return m[x][y][z];
}

int getElem2(int m[LEN1][LEN2][LEN3], unsigned int x,unsigned int y, unsigned z)
{
  return *(((int*)m) + ( (LEN2 *LEN3 * x) + (LEN2 * y) + z) );
}

// NO: No puede tener incompleta la geometr'ia 
// error: declaration of ‘m’ as multidimensional array must have bounds for all dimensions except the first
//void setElem(int m[][][LEN3], unsigned int x,unsigned int y, unsigned int z, int value)

// SI: 
void setElem(int m[][LEN2][LEN3], unsigned int x,unsigned int y, unsigned int z, int value)
//void setElem(int m[LEN1][LEN2][LEN3], unsigned int x,unsigned int y, unsigned int z, int value)
{
  m[x][y][z] = value;
}

int main()
{
  //         4 x   2  x  2 
  int cube  [LEN1][LEN2][LEN3]     = { {{0, 1}, {2, 3}} , {{4, 5}, {6, 7}} , {{8, 9}, {10, 11}} , {{12, 13}, {14, 15}} };
  int cube2 [LEN1 * LEN2 * LEN3 ]  = {   0, 1 ,  2, 3   ,   4, 5 ,  6, 7   ,   8, 9 ,  20, 11   ,   12, 13 ,  14, 15   };
  int i,j,k;
  
  printf("sizeof(cube)=%ld sizeof(cube2)=%ld\n",sizeof(cube),sizeof(cube2));
  for(i=0;i<LEN1;i++) 
    {
      for(j=0;j<LEN2;j++) 
	{
	  for(k=0;k<LEN3;k++)
	    {
	      printf("(%d)",getElem(cube,i,j,k));
	      printf("(%d)",getElem2(cube,i,j,k));
	    }
	}
      printf("\n");
    }
  printf("\n");
  for(i=0;i<LEN1;i++) 
    {
      for(j=0;j<LEN2;j++) 
	{
	  for(k=0;k<LEN3;k++)
	    {
	      printf("(%d)",cube2[(LEN3*LEN2*i)+((LEN2*j)+k)]);
	    }
	}
      printf("\n");
    }
  return 0;
}

