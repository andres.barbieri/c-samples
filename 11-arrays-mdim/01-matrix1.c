#include <stdio.h>

#define LEN1  4
#define LEN2  2

/***
Arrange in memory:

[0   ,0] [0   ,1] [0   ,2] ... [0   ,LEN2-1]
[1   ,0] [1   ,1] [1   ,2] ... [1   ,LEN2-1]
...
[LEN1,0] [LEN1,1] [LEN1,2] ... [LEN1,LEN2-1]

1st row
2nd row
3rd row
4th row
...
n-th row
***/

int getElem(int m[LEN1][LEN2], unsigned int x,unsigned int y)
{
 
  return m[x][y];
}

int getElem2(int m[LEN1][LEN2], unsigned int x,unsigned int y)
{
  //warning: return makes integer from pointer without a cast
  // m: int (*)[LEN2] y deber'ia ser m: int*
  //return *(m + ((LEN2 * x) + y));
  return *(((int*)m) + ((LEN2 * x) + y));
}

// NO: No puede tener incompleta la geometr'ia 
//error: array type has incomplete element type
//void setElem(int m[][], unsigned int x,unsigned int y, int value)

// SI: Puede ser la interfaz Solo las columnas se necesitan
//void setElem(int m[][LEN2], unsigned int x,unsigned int y, int value)
void setElem(int m[LEN1][LEN2], unsigned int x,unsigned int y, int value)
{
  m[x][y] = value;
}

void setElem2(int m[][LEN2], unsigned int x,unsigned int y, int value)
{
  int *p;
  // warning: initialization from incompatible pointer type
  // p = ((m) + ((LEN2 * x) + y)); // Error, increment; smash the stack
  p = (((int*)m) + ((LEN2 * x) + y));
  *p = value;
}

int main()
{
  //int matrix[LEN1][LEN2]  = { 2 , 5 }
  //           4 x  2    
  int matrix[LEN1][LEN2] = { {2, 5}, {2,5}, {}, {3,4} };
  int i,j;

  for(i=0;i<LEN1;i++) 
    {
      for(j=0;j<LEN2;j++) 
	{
	  printf("(%d)",getElem(matrix,i,j));
	  printf("(%d)",getElem2(matrix,i,j));
	}
      printf("\n");
    }

  for(i=0;i<LEN1;i++) 
    {
      for(j=0;j<LEN2;j++) 
	{
	  //setElem(matrix,i,j,j);
	  setElem2(matrix,i,j,j);
	}
    }
  printf("\n");
  for(i=0;i<LEN1;i++) 
    {
      for(j=0;j<LEN2;j++) 
	{
	  printf("(%d)",getElem(matrix,i,j));
	  printf("(%d)",getElem2(matrix,i,j));
	}
      printf("\n");
    }

  return 0;
}
