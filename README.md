# Ejemplos de programas en "C"

Ejemplo de programas mínimo en lenguaje "C" con diferentes propósitos

## Organización

Los ejemplos están organizados en directorios individuales, y en un orden de acuerdo a como se pueden abordar los temas:

* [`00-main-format`](https://gitlab.com/andres.barbieri/c-samples/tree/master/00-main-format): programa mínimo en "C".
* [`00-main-hello`](https://gitlab.com/andres.barbieri/c-samples/tree/master/00-main-hola): programa Hello World en "C".
* ...

## Modo de uso

Se puede analizar el código fuente incluido en cada directorio siguiendo el README que se acompaña.

```console
$ git clone https://gitlab.com/andres.barbieri/c-samples.git c-samples
$ cd c-samples/<dir>
$ cat README.md
$ ...
```

Se puede clonar usando ssh con también:

```console
$ git clone git@gitlab.com:andres.barbieri/c-samples.git
...
```

