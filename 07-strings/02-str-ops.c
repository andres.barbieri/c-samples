#include<stdio.h>  // printf
#include<string.h> // strlen, strcat, strcmp, strncmp

#define STR_LEN  9

#define STR_MACRO "hola"

int main()
{
  char str[] = "hola";
  char str2[5] = { 'a', 'o', 'l', 'a', '\0' };
  char dst[9]; // Not init'd
  //char dst[9] = "";
  
  dst[0]='\0';
  printf("dst=\"%s\"\n",dst);
  strncat(dst, str,  strlen(str) );
  printf("dst=\"%s\"\n",dst);
  strncat(dst, str2, strlen(str2) );
  printf("dst=\"%s\"\n",dst);
  printf("%d\n",strncmp(str2,str,STR_LEN));
  printf("%d\n",strncmp(str,str2,STR_LEN));
  str2[0] = 'h';
  printf("%d\n",strncmp(str,str2,STR_LEN));
  printf("%d\n",strncmp(str,dst,STR_LEN));
  printf("%d\n",strncmp(dst,str,STR_LEN));
  printf("%d\n",strncmp(str,STR_MACRO,STR_LEN));
  printf("%d\n",strncmp(str,str,STR_LEN));
  dst[0] = 0;
  printf("%d\n",strncmp(dst,"",STR_LEN));
  return 0;
}
