#include <stdio.h>

#include "string_lib.h"

int main()
{
  char str[] = "HOLA";
  char empty[] = "";
  char str2[] = "HOLA HOLA HOLA";
  char str3[] = "HOLA3";  
  char str4[] = "hola4";  
  char str5[32] = {'a','b','c',0 };

  printf("%d\n",str_len(str));
  printf("%d\n",str_len(NULL));
  printf("%d\n",str_len(empty));
  printf("%d\n",str_len(str2));
  printf("##\n");
  printf("%d\n",str_cmp(str,str));
  printf("%d\n",str_cmp(NULL,NULL));
  printf("%d\n",str_cmp(str,NULL));
  printf("%d\n",str_cmp(NULL,str));
  printf("%d\n",str_cmp(empty,str));
  printf("%d\n",str_cmp(str2,str));
  printf("%d\n",str_cmp(str3,str));
  printf("%d\n",str_cmp(str,str3));
  printf("##\n");
  printf("%s\n",str_cpy(str,NULL));
  /*printf("%s\n",str_cpy(NULL,str));*/ printf("NO resuelta\n");
  printf("%s\n",str_cpy(str3,str4));
  printf("%s\n",str_cpy(str4,str));
  printf("##\n");
  printf("%s",str_cat(str5,str)); printf("(%d)\n",str_len(str5));
  printf("%s",str_cat(str5,str2)); printf("(%d)\n",str_len(str5));
  printf("%s",str_cat(str5,empty)); printf("(%d)\n",str_len(str5));
  printf("%s",str_cat(str5,str4)); printf("(%d)\n",str_len(str5));
  return 0;
}
