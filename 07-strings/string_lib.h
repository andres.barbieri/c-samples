#ifndef MY_STRING_LIB
#define MY_STRING_LIB

#ifndef NULL
#define NULL (0)
#endif

unsigned int str_len(const char *s);

int str_cmp(const char *s1, const char *s2);

char *str_cpy(char *dest, const char *src);

char *str_cat(char *dest, const char *src);

char *str_str(const char *haystack, const char *needle);

#endif /* string_lib.h -- MY_STRING_LIB */


