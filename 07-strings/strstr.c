#include "string_lib.h"

char *strstr1(const char *haystack, const char *needle) {
  const char *s, *n;
  
  while (*haystack) {
      s = haystack;
      n = needle;
      while ((*n) && (*haystack)) {
	  if (*haystack == *n) {
	      haystack++;
	      n++;
	    }
	  else {
	      haystack++;
	      break;
	    }
	}
      if (!(*n)) return (char*)s;
    }
  return 0; // NULL
}

char *strstr2(const char *haystack, const char *needle)
{
  char *curhay;
  char *curneedle;
  
  while (*haystack)
    {
      curhay    = (char*) haystack;
      curneedle = (char*) needle;
      while ((*curneedle) && (*haystack))
	{
	  if (*haystack == *curneedle)
	    {
	      haystack++;
	      curneedle++;
	    }
	  else
	    {
	      haystack++;
	      break;
	    }
	}
      if (!(*curneedle))
	{
	  return curhay;
	}
    }
  return NULL;
}


char *strstr3(const char *haystack, const char *needle)
{
  char *curhay;
  char *curneedle;
  
    while ((haystack != NULL) && (*haystack != 0))
    {
      curhay    = (char*) haystack;
      curneedle = (char*) needle;
      while ((*curneedle !=0 ) && (*haystack !=0 ))
	{
	  if (*haystack == *curneedle)
	    {
	      haystack++;
	      curneedle++;
	    }
	  else
	    {
	      haystack++;
	      break;
	    }
	}
      if (*curneedle == 0)
	{
	  return curhay;
	}
    }
  return NULL;
}

char *strstr4(const char *haystack, const char *needle)
{
  int i;
  int j;
  int ii;
  
  i = 0;
  while (haystack[i] != 0)
    {
      ii = i;
      j  = 0;
      while ((needle[j] != 0 ) && (haystack[i] != 0 ))
	{
	  if (haystack[i] == needle[j])
	    {
	      i++;
	      j++;
	    }
	  else
	    {
	      i++;
	      break;
	    }
	}
      if (needle[j] == 0)
	{
	  return (char*) &(haystack[ii]);
	}
    }
  return NULL;
}


char *strstr5(const char *haystack, const char *needle)
{
  int i;
  int j;
  int ii;
  int match;
  int found = 0;
  
  i = 0;
  while ( (haystack[i] != '\0') && (!found) )
    {
      ii    = i;
      j     = 0;
      match = 1;
      while ( (needle[j] != '\0') && (haystack[i] != '\0') && (match) )
	{
	  if (haystack[i] == needle[j])
	    {
	      i++;
	      j++;
	      match = 1;
	    }
	  else
	    {
	      i++;
	      match = 0;
	    }
	}
      if (needle[j] == '\0')
	{
	  found = 1;
	}
    }
  if (found)
    {
      // warning: return discards ‘const’ qualifier from pointer target type [-Wdiscarded-qualifiers]
      //return &(haystack[ii]); // compile with flag: -Wno-discarded-qualifiers
      return (char*) &(haystack[ii]);
    }
  else
    {
      return NULL;
    }
}

// From: https://codereview.stackexchange.com/questions/35396/strstr-implementation
char * strstr6(const char *haystack, const char *needle) {  
    const char *s = haystack, *n = needle;  
    for (;;)  
        if      (!*n)          return (char *)s;  
        else if (!*s)          return 0;  // NULL
        else if (*s++ != *n++) { s = ++haystack; n = needle;}  
}


char *rstrstr(const char *haystack, const char *needle)
{

  if (!*needle)
    {
      return (char*) haystack;
    }
  if (!*haystack)
    {
      return 0; // NULL
    }
  if ( (*needle == *haystack) && (rstrstr(haystack+1,needle+1) == haystack+1) )
    {
      return (char*) haystack;
    }
  else
    {
      return rstrstr(haystack+1,needle);
    }
}
			      
/** EOF strstr.c **/
