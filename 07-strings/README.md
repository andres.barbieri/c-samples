# Aprendiendo strings en "C" Preprocessor

Ejemplos de programa que muestran ejemplos de uso strings en "C". 

## Introducci�n

Los strings en "C" son manejados directamente como arreglos de bytes, chars, y los mismos deben ser terminados en 0 ('\0').

### Pre-requisitos

Sistema UNIX compatible (??).
Compilador GCC, paste

## Pruebas

Probar y analizar los ejemplos que se proveen.
 
### Primeros Pasos

Generar los binarios con:
 
```console
$ make
```
Esto genera:

* `01-str-decl`
* `02-str-ops`
* `03-str-wrong`
* `04-str-loops`
* `05-main-str`
* `05-main-str_lib`
* `05-main-str_libalt`
* `06-main-strstr`
* `07-str_reverse`

El primer ejemplo, `01-str-decl` muestra declaraciones b�sicas y uso de strings, el segundo, `02-str-ops`, muestra el uso de las funciones est�ndares de strings prove�das por la libC y los prototipos en '<string.h>'. El tercero muestra errores com�nes que se cometen con el uso de strings, `03-str-wrong`. El cuarto muestra diferentes formas de iterar sobre un string, `04-str-loops`. El quinto ejemplo muestra posibles formas de implementar la funcionalidad porve�da por las funciones definidas en '<string.h>' y compara contra estas. Para comparar se puede ejecutar:

```console
$ ./05-main-str > output.txt 
$ ./05-main-str_lib > outputlib.txt

$ diff -d output.txt outputlib.txt 
2c2
< NO resuelta
---
> 0
7,9c7,9
< NO resuelta
< NO resuelta
< NO resuelta
---
> 0
> -1
> 1
15c15
< NO resuelta
---
> HOLA

$ paste output.txt outputlib.txt 
4	4
NO resuelta	0
0	0
14	14
##	##
0	0
NO resuelta	0
NO resuelta	-1
NO resuelta	1
-72	-72
32	32
51	51
-51	-51
##	##
NO resuelta	HOLA
NO resuelta	NO resuelta
hola4	hola4
HOLA	HOLA
##	##
abcHOLA(7)	abcHOLA(7)
abcHOLAHOLA HOLA HOLA(21)	abcHOLAHOLA HOLA HOLA(21)
abcHOLAHOLA HOLA HOLA(21)	abcHOLAHOLA HOLA HOLA(21)
abcHOLAHOLA HOLA HOLAHOLA(25)	abcHOLAHOLA HOLA HOLAHOLA(25)
```

Se presenta una alternativa a `string_lib.c` las funciones de strings en el archivo `string_lib_alt.c`. Tambi�n se puede testear usando el mismo "main" pero generando el ejecutable con la implementaci�n alternativa. Se puede comparar de forma similar:

```console
$ ./05-main-str_lib > outputlib.txt
$ ./05-main-str_libalt > outputlibalt.txt

$ diff -d outputlib.txt outputlibalt.txt
```

En `07-str_reverse` se muestran diferentes formas de implementar un reverse de strings en C, usados como arreglos y como punteros.


