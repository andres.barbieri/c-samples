#ifndef NULL
#define NULL (0)
#endif


unsigned int str_len(const char *s)
{
  unsigned int count = 0;
  if (s != NULL)
    {
      while(s[count])
	{
	  count++;
	}
      return count;
    }
  else
    {
      return 0;
    }
}

int str_cmp(const char *s1, const char *s2)
{
  int i = 0;
  if ((s1==NULL)&&(s2==NULL))
    return 0;
  if ((s1==NULL)&&(s2!=NULL))
    return 1;
  if ((s1!=NULL)&&(s2==NULL))
    return -1;
  while ((s1[i])&&(s2[i]))
    {
      if (s1[i] < s2[i])
	return -1;
      else if (s1[i] > s2[i])
	return 1;
      else
	i++;
    }
  return (s1[i]-s2[i]);
}

char *str_cpy(char *dest, const char *src)
{
  int i = 0;
  if ((dest==NULL)||(src==NULL))
    return dest;
  while (src[i])
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = 0;
  return dest;
}

char *str_cat(char *dest, const char *src)
{
  int i,j;
  
  i = j = 0;
  if ((dest == NULL)||(src==NULL)) return dest;
  while(dest[i]) i++;
  while(src[j])
    {
      dest[i] = src[j];
      i++;
      j++; 
    }
  return dest;
}

char *strstr5(const char *haystack, const char *needle);

char *str_str(const char *haystack, const char *needle)
{
  return strstr5(haystack, needle);
}

/** EOF ** string_lib.c **/



