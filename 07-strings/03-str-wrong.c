#include<stdio.h>
#include<string.h> //strcat strncat strlen

#define STR_LEN  9

#define STR_MACRO       "hola"

int main()
{
  char str[]   = "hola";
  char str2[5] = { 'h', 'o', 'l', 'a', '\0' };
  char *str3   = "hola";
  char src[9]; // Not init
  //char dst2[];
  char buf[STR_LEN];

  //str   = "otro"; //error: incompatible types when assigning to type 'char[5]' from type 'char *'
  //dst2  = "0tr0"; //error: incompatible types when assigning to type 'char[1]' from type 'char *
 
  str2[0] = 'j';   // trabaja sobre una copia en el stack 
  // str3[0] = 'j';   // intenta trabajar sobre un puntero a un literal error

  //gets(buf); //warning: 'gets' is deprecated (declared at /usr/include/stdio.h
  fgets(buf, 60, stdin);

  // Comparaciones de punteros y no de contenido
  printf("%s == %s\n",str,str2);
  if (str == str2) { printf("equals\n"); } else { printf("not equals\n"); };

  // Trabajo sin espacio - Segmentation fault
  //strcat(str3,STR_MACRO);

  // Trabajo sobre copias sin inicializar
  printf("src=\"%s\" len==%u\n",src,(unsigned int)strlen(src));
  strncat(str, src,  strlen(str));
  strncat(str, str,  strlen(str));
  strncat(str, str,  strlen(str));
  printf("str=\"%s\"\n",str);

  return 0;
}
