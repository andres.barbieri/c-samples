#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char *reverse(char s[])
{
	char aux;
	int i;

	for(i=0;i<(strlen(s)/2);i++)
	{
	aux = s[i];
        s[i] = s[strlen(s)-i-1];
	s[strlen(s)-i-1] = aux;
	}
	return s;
}

char *reverse2(char s[])
{
    int c, i, j;

    for (i = 0, j = strlen(s)-1; i < j; i++, j--)
      {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
      }
    return s;
}

char *stradd(const char *src1, char c)
{
  char *aux=malloc(strlen(src1)+2);
  strcpy(aux,src1);
  aux[strlen(src1)]   = c;
  aux[strlen(src1)+1] = 0;
  return aux;
}

char *reverse3_memLOST(char *s)
{
  if (strlen(s) <= 1)
    return s;
  else
    {
      char *aux = reverse3_memLOST(s+1);
      return stradd(aux,s[0]);
    }
}

char *reverse3(char *s)
{
  if (strlen(s) <= 1)
    return s;
  else
    {
      char *aux = reverse3(s+1);
      aux = stradd(aux,s[0]);
      strcpy(s,aux);
      free(aux);
      return s;
    }
}

char *reverse4(char *s)
{
  char c;
  char *begin = s;
  
  if (s)
  {
    char *end = s + strlen(s) - 1;

    while (begin < end)
    {
      c = *begin;
      *begin = *end;
      *end = c;
      begin++;
      end--;
    }
  }
  return s;
}


void swap(char *a,char *b)
{
  char c;
  
  c  = *a;
  *a = *b;
  *b = c;
}

char *reverse5(char *s)
{
  char *aux = s;
  
  if (s)
  {
    char *end = s + strlen(s) - 1;

    while (s < end)
    {
      swap(s,end);
      s++;
      end--;
    }
  }
  return aux;
}

char *reverse6(char *s)
{
  if (s)
  {
    for(char *end = s+strlen(s)-1,*begin = s; begin<end; begin++,end--)
    {
      swap(begin,end);
    }
  }
  return s;
}

char *reverse7(char *s)
{
  if (s)
  {
    for(char *end = s+strlen(s)-1,*begin = s; begin<end; swap(begin,end),begin++,end--);
  }
  return s;
}


int main()
{
  
	char cero[] = "";  
	char uno[] = "a";
	char dos[] = "ab";
	char tres[] = "abc";
	char cuatro[] = "abcd";		
	char cinco[] = "esto es una prueba de un reverse en C";		
	
	printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse(cero),reverse(uno),reverse(dos),reverse(tres),reverse(cuatro));
	printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse2(cero),reverse2(uno),reverse2(dos),reverse(tres),reverse2(cuatro));	
	printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse3(cero),reverse3(uno),reverse3(dos),reverse3(tres),reverse3(cuatro));
	printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse4(cero),reverse4(uno),reverse4(dos),reverse4(tres),reverse4(cuatro));
		printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse5(cero),reverse5(uno),reverse5(dos),reverse5(tres),reverse5(cuatro));
		printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse6(cero),reverse6(uno),reverse6(dos),reverse6(tres),reverse6(cuatro));
		printf("\"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",reverse7(cero),reverse7(uno),reverse7(dos),reverse7(tres),reverse7(cuatro));		
	 printf("\"%s\"\n",reverse6(cinco));	
	//printf("\"%s\"\n",reverse3_memLOST(cuatro));	
	return 0;
}
