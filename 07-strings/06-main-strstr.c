#include <stdio.h>

#include "string_lib.h"

int main()
{
  char *a ="la Ciencia de la Computacion no tiene que ver con las computadoras "
           "mas que la Astronomia con los telescopios";
  
  char *b[] = { "Ciencia", "ciencia", "Ciencia de la Computacion", "Computadoras",
		"computadoras", "las computadoras", "los computadoras", "teles", "telescopios" ,
		"opios", "mas", "no tiene que ver", a, "que", "quien", "", NULL };
  char *r; 

  int i = 0;
  while (b[i] != NULL)
    {
      r = str_str(a,b[i]);
      if (r !=NULL)
	printf("FOUND IN: %s\n",r);
      else
	printf("NOT FOUND: %s\n",b[i]);
      i++;
    }
  return 0;
}
