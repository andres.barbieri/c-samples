#include <stdio.h>
#include <string.h>

int main()
{
  char str[] = "HOLA";
  char empty[] = "";
  char str2[] = "HOLA HOLA HOLA";
  char str3[] = "HOLA3";  
  char str4[] = "hola4";  
  char str5[32] = {'a','b','c',0 };

  printf("%d\n",(int)strlen(str));
  /*printf("%d\n",strlen(NULL));*/ printf("NO resuelta\n");
  printf("%d\n",(int)strlen(empty));
  printf("%d\n",(int)strlen(str2));
  printf("##\n");
  printf("%d\n",strcmp(str,str));
  /*printf("%d\n",strcmp(NULL,NULL));*/printf("NO resuelta\n");
  /*printf("%d\n",strcmp(str,NULL));*/printf("NO resuelta\n");
  /*printf("%d\n",strcmp(NULL,str));*/printf("NO resuelta\n");
  printf("%d\n",strcmp(empty,str));
  printf("%d\n",strcmp(str2,str));
  printf("%d\n",strcmp(str3,str));
  printf("%d\n",strcmp(str,str3));
  printf("##\n");
  /*printf("%s\n",strcpy(str,NULL));*/printf("NO resuelta\n");
  /*printf("%s\n",strcpy(NULL,str));*/printf("NO resuelta\n");
  printf("%s\n",strcpy(str3,str4));
  printf("%s\n",strcpy(str4,str));
  printf("##\n");
  printf("%s",strcat(str5,str)); printf("(%d)\n",(int)strlen(str5));
  printf("%s",strcat(str5,str2)); printf("(%d)\n",(int)strlen(str5));
  printf("%s",strcat(str5,empty)); printf("(%d)\n",(int)strlen(str5));
  printf("%s",strcat(str5,str4)); printf("(%d)\n",(int)strlen(str5));
  return 0;
}
