#include <stdio.h>

int main()
{
  extern int a; // extern, no masking, uses global value provided by A
  
  printf("%d\n",a);
  
  return 0;
}
