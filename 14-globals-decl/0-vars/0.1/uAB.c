#include <stdio.h>

// implicit extern, no defined, uses global value provided by A
int a;

int main()
{
  printf("%d\n",a);
  
  return 0;
}
