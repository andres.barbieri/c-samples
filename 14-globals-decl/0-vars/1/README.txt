gcc -o uAB uAB.c A.c -Wall -DAUTO
uAB.c: In function 'main':
uAB.c:12:9: warning: 'a' is used uninitialized in this function [-Wuninitialized]
   printf("%d\n",a);
         ^

A "a" si la defino como automática en el main enmascara la global. 

------------------------------

gcc -o uAB uAB.c A.c

Si la defino como global (esta asignando un valor inicial, asume el compilador que es una definición) hay conflicto de doble definición.

gcc -o uAB uAB.c A.c -Wall       
/tmp/ccv9i5sm.o:(.data+0x0): multiple definition of `a'
/tmp/ccZI5jSU.o:(.data+0x0): first defined here
collect2: error: ld returned 1 exit status

gcc -c uAB.c 
 gcc  A.o uAB.o
uAB.o:(.data+0x0): multiple definition of `a'
A.o:(.data+0x0): first defined here
collect2: error: ld returned 1 exit status

--------------------------------

gcc -o uAB uABdecl.c A.c -Wall 

Si se quita la inicialización global el compilador puede asumir que es una declaración y no habrá inconveniente.

