#include <stdio.h>

void func()
{
 extern int a;

 printf("%d\n",a);
}

int main()
{
  func();
  return 0;
}
