gcc -o uAB uAB.c A.c B.c

Definición de "a" en A.c y su declaración en main(uAB.c). La variable declarada en B.c queda privada, oculta y no se provee visibilidad externa a la misma.

----------------------------

gcc -o uAB uABfunc.c A.c B.c 

Declaración en func() en lugar de main().

