# Alcance, Definici�n, Declaraci�n, `extern` keyword

## Alcance/Scope: el alcance de una variable, definici�n, es en donde/hasta donde se puede referenciar/utilizar. Los alcances en "C" pueden ser globales, globales restringidos a un archivo o locales a la funci�n.

## Diferencia entre definici�n y declaraci�n de una variable:

* Definici�n de variable, el compilador reserva memoria y posiblemente inicializa. La definici�n solo debe hacerse una vez en alg�n m�dulo. La definici�n implica tambi�n declaraci�n. Una definici�n posiblemente lleve un valor inicial. Este fuerza a que sea una definici�n.

* Declaraci�n de variable, el compilador requiere que la variable este definida en alg�n lado y sea accesible con el tipo de la declaraci�n. Informa al compilador de la existencia de la variable, pero el compilador no reservara un espacio nuevo para la misma, buscar� donde esta definido. La palabra clave `extern`, justamente declara sin definir. La declaraci�n puede existir m�s de una vez. La declaraci�n puede realizarse de forma global o local a una funci�n. Para que sea local y no implique definici�n debe usarse la palabra clave `extern`.

* Al tiempo de compilaci�n pueden existir identificadores declarados pero no definidos. Al tiempo de link-edici�n se deben encontrar/linkear con su definici�n, sino no se podr� generar el binario final.

* En el caso que se indique una variable sin indicar la palabra clave `extern` ni un valor inicial, puede ser interpretado como declaraci�n o definici�n. 

* Una variable puede ser declarada `extern` dentro de una funci�n, sino ser� una definici�n local.

## Definici�n y declaraci�n de una funci�n:

* La palabra clave `extern` aplicada a la declaraci�n de funci�n no aporta nada, es irrelevante por definici�n todas las funciones declaradas son `extern`. Los proto-types siempre son declaraciones externas. 

# Static Keyword

* La definici�n de una funci�n como `static` indica que tiene un alcance local al archivo donde se define, es privada al m�dulo.

* La definici�n de una variable como `static` indica que tiene un alcance local al m�dulo/funci�n donde se define (por definici�n es local) pero no es autom�tica, no se reservar� el espacio en el stack sino queda definida en un espacio que persiste la ejecuci�n del m�dulo. Mantendr� su espacio durante la ejecuci�n de todo el programa.

# Gu�a de buenas pr�cticas?

1. Un header file solo debe contener declaraciones externas de variables. Nunca `static` o sin calificar.

2. Para una variable solo un header la debe declararla.

3. Un archivo fuente no deber�a contener declaraciones `extern`, deber�a incluir el header que la declara.

4. Una variable solo debe ser definida en un archivo fuente y preferentemente ser inicializada aunque no es necesario hacerlo de forma expl�cita.

5. Un archivo fuente que define una variable debe incluir el header donde se declara para ser consistente declaraci�n y definici�n.

6. Una funci�n no deber�a necesitar declarar variables externas.

7. Evitar variables globales.

