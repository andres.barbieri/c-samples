/*
 *
 * Hello world Program en "C"
 *
 * NOTE:
 *
 * compile: 
 *          gcc hola.c                        -- generate a.out
 *          gcc -o hola hola.c                -- generate hola
 *          gcc -Wall -std=c99 -o hola hola.c -- generate hola
 *          make                              -- generate hola
 *
 * Exit codes:
 *
 * 0    OK
 *
 * 1	Catchall for general errors
 *      Miscellaneous errors.
 *
 * /usr/include/sysexits.h
 *
 */

#include <stdio.h>

int main() 
{
  return (printf("Hola mundo\n") != 11);
}

/*** EOF ***/
