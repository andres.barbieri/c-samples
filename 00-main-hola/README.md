# Aprendiendo estructura de un programa básico en "C"

Ejemplos de programa que muestran algo, i.e. Hello World, en el lenguaje "C"

## Primeros pasos

Encontrar que no esta bien o fuera de acuerdo con el estándar en los archivos .c
Encontrar que acepta el compilador y que salida da para cada fuente.

### Pre-requisitos

Compilador GCC, make, sleep, file, objdump, cat, ldd ...
Básicamente un entorno de desarrollo 

## Pruebas
 
Probar ejecutando paso a paso y ver los resultados de la compilación en etapas:

Fuente -> Pre-procesado -> Assembly -> Objeto (Bin) -> Ejecutable (bin) 

```console
 make clean
 make steps
 cat hola.i
 cat hola.s
 file hola
 ldd hola
 ...
 ./hola; echo $?
```

Probar ejecutando la compilación y linkedición toda junta:

```console
 make clean
 make one
```

Probar los pasos típicos de la compilación: 

Fuente -> Objeto -> Ejecutable 

```console
 make clean
 make
```

Probar la linkedición de forma estática:

```console
 make clean
 make static
```

Inspeccionar y comparar los formatos de los binarios y sus contenidos. Esta salida va a variar de acuerdo a la plataforma, herramientas y compilador.

```console
$ file hola        
hola: ELF 64-bit LSB  executable, x86-64, version 1 (SYSV), dynamically linked (uses shared libs), for ...

$ file hola.static
hola.static: ELF 64-bit LSB  executable, x86-64, version 1 (GNU/Linux), statically linked, for ...

$ ldd hola        
	linux-vdso.so.1 =>  (0x00007fff4975c000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007fa194a5e000)
	/lib64/ld-linux-x86-64.so.2 (0x00007fa194e23000)

$ file hola.o      
hola.o: ELF 64-bit LSB  relocatable, x86-64, version 1 (SYSV), not stripped

 $ objdump -d hola.o 

hola.o:     file format elf64-x86-64

Disassembly of section .text:

0000000000000000 <main>:
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	bf 00 00 00 00       	mov    $0x0,%edi
   9:	b8 00 00 00 00       	mov    $0x0,%eax
   e:	e8 00 00 00 00       	callq  13 <main+0x13>
  13:	83 f8 0b             	cmp    $0xb,%eax
  16:	0f 95 c0             	setne  %al
  19:	0f b6 c0             	movzbl %al,%eax
  1c:	5d                   	pop    %rbp
  1d:	c3                   	retq   
```
