/*
 * Ejemplo de uso de memoria din'amica con 
 * punteros gen'erico a void
 */

#include <stdlib.h> // malloc exit free
#include <stdio.h>  // printf

int main()
{
  // Declaración - Declaration
  void           *p    = NULL;     // int pointer
  int           count = 10;

  p = malloc(count * sizeof(int));

  /* make sure if chunk is allocated or not */
  if (p == NULL) {
    exit(1);
  }

  //*p = 10; // error: invalid use of void expression - warning: dereferencing 'void *'           

  *((char*) p) = -10; // dereferencing 'void *' as 'char *'
  printf("0x%08X\n", *((int*) p));
  *((int*) p)  = -10; // dereferencing 'void *' as 'int *'
  printf("0x%08X\n", *((int*) p));

  //count = *p; // error: void value not ignored as it ought to be
  count = *((int*) p);
  
  // Incremento - Increment
  printf("%p %p\n",p,p+1);
  printf("%p %p\n",p,((int*)p)+1);
                                                             //(((char*)p)[count])); 
  for(count=0;count<sizeof(int);count++) { printf("[%02X]", (*(((char*)p)+count)) & 0xFF ); }
  printf("\n");
  /* after your are done, free up memory */
  /* finalmente liberar la memoria */
  free(p); p = NULL;

  return 0;
} 
