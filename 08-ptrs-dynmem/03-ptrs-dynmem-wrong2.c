/*
 * Programa para ilustrar errores que se comenten programando
 * punteros en "C".
 * Analizar, encontrar los errores e indicar como se corrigen
 * 
 * Correr cambiando la variable de entorno MALLOC_CHECK_
 *
 * MALLOC_CHECK_=0 ./03-ptrs-dynmem-wrong2  00
 * MALLOC_CHECK_=1 ./03-ptrs-dynmem-wrong2  01 
 * MALLOC_CHECK_=2 ./03-ptrs-dynmem-wrong2  10
 * MALLOC_CHECK_=3 ./03-ptrs-dynmem-wrong2  11
 * 
 */

/*
 * MALLOC_CHECK_=1 ./03-ptrs-dynmem-wrong2
 * *** Error in `./03-ptrs-dynmem-wrong2': free(): 
 * invalid pointer: 0x0000000001 c9b054 ***
 * Aborted
 */

#include <stdlib.h> // malloc , free


int main()
{
  // Declaración - Declaration
  int           *p;            // puntero a un entero
  int           count = 10;

   p = malloc(count * sizeof(int));

   *p = 10; // not checking p != NULL before use it
 
   p = malloc(count*2*sizeof(int)); // re-alloc without freeing before

   p+=5;

   free(p);  
     
   return 0;
} 
