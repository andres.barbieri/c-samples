#include <stdio.h>
#include <stdlib.h> // malloc , calloc, free
#include <string.h> // memset

#define MAX_LEN 16

void f(int a[])
{
  printf("%p sizeof(a)=%lu\n",a,sizeof(a));
}

void g(int a[MAX_LEN])
{
  printf("%p sizeof(a)=%lu\n",a,sizeof(a));
}

void h(int *a)
{
  printf("%p sizeof(a)=%lu\n",a,sizeof(a));
}

void w(const int *a)
{
  printf("%p sizeof(a)=%lu\n",a,sizeof(a));
}

void modif_val(int *a)
{
  a[0] = 0xffff;
}

void modif_ref(int *a)
{
  a = NULL;
}

void modif_ref2(int **a)
{
  (*a) = NULL;
}
int main()
{
  int i;
  // Declaración - Declaration
  int           *p = NULL;     // puntero a un entero
  int           *pbak = NULL;  // puntero a un entero
  char          *q = NULL;     // puntero a char 
  int            a_i[MAX_LEN]; // array de enteros
  char           a_c[MAX_LEN]; // array de chars
  int           *r = NULL;     // puntero a un entero

 // Pointer's content in Heap
  p = (int*) malloc(sizeof(int)*MAX_LEN);  // no init - not init'd 
  q = malloc(sizeof(char)*MAX_LEN);        // no init - not init'd  
  r = calloc(MAX_LEN, sizeof(int));        // init a cero - init'd to zero

  if ((p != NULL)&&(q!=NULL))
    {
      // Asignación - Assignment
      for(i=0;i<MAX_LEN;i++) 
	{
	  a_i[i] =  0;
	  a_c[i] = '\0';
	  p[i]   =  0; 
	  q[i]   = '\0';
	}
      printf("sizeof(a_i)=%lu sizeof(a_c)=%lu sizeof(p)=%lu sizeof(q)=%lu\n",
              sizeof(a_i),   sizeof(a_c),   sizeof(p),   sizeof(q));
    }
  //memset((void *)p, 0, MAX_LEN);  // warn: solo inicializa una parte
  memset((void *)p, 0, MAX_LEN*sizeof(int));  // como bzero - like bzero, bzero is deprecated  
  memset((void *)p, 1, MAX_LEN*sizeof(int));  
   
  // Parametro - Parameters
  f(a_i);g(a_i);h(a_i);w(a_i);
  f(p);g(p);h(p);w(p);

  // Parametro - Parameters output
  printf("pre modif ref  : p=%10p p[%d]=%d\n",p,0,p[0]);
  modif_ref(p);
  printf("pos modif ref  : p=%10p p[%d]=%d\n",p,0,p[0]);
  modif_val(p);
  printf("pos modif val  : p=%10p p[%d]=%d\n",p,0,p[0]);
  pbak = p;
  modif_ref2(&p);
  printf("pos modif ref2 : p=%10p p[%d]=SGFLT\n",p,0);
  p = pbak;
  printf("pos restore ref: p=%10p p[%d]=%d\n",p,0,p[0]);
  // Liberar memoria - Freeing Memory 
  free((void*) p);
  free((void*) q);
  free((void*) r);
  return 0;
} 
