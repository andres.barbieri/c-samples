#include <stdio.h>
#include <stdlib.h>

int main()
{
  // Declaración - Declaration

  int           *p = NULL;     // puntero a un entero
  char          *q = NULL, c;  // puntero a char y una var char
  int            j;

  // Asignación - Assignment (Dirección)

  // Pointers contents in Stack
  p = &j;
  q = &c;
  printf("Stack: value of  p=%p  value of q=%p\n",p,q);
  printf("Stack: value of *p=%d  value of *q=%c\n",*p,*q);

  // Pointer's content in Heap
  p = (int*) malloc(sizeof(int));  // not init'd
  q = malloc(sizeof(char)); 

  // Check them before using
  if ((p != NULL)&&(q!=NULL))
    {
      printf("Heap: value of  p=%p value of  q=%p\n",p,q);
      printf("Heap: value of *p=%d value of *q=%c\n",*p,*q);
    }
  else
    {
      return 1;
    }
  *q = 'a', *p = 0;
  printf("value of  p=%p value of  q=%p\n",p,q);   
  printf("value of *p=%d value of *q=%c\n",*p,*q);

  // Freeing Memory
  // If ptr is NULL, no operation is performed.
  free(p);
  free((void*) q);

  // if free(ptr) has already been called  before,
  // undefined behavior occurs.
  // free(p);
  // free((void*) q);
  
  // free stack pointer !!! Error
  // *** glibc detected *** ./ptrs3: free(): invalid pointer:
  // p = &j;
  // free(p);

  if ((p != NULL)&&(q != NULL))
    {
      printf("Heap Released: value of  p=%p value of  q=%p\n",p,q);
      printf("Heap Released: value of *p=%d value of *q=%c\n",*p,*q);
    }
    
  p = (int*) malloc(sizeof(int));
  q = malloc(sizeof(char)); 
  if ((p != NULL)&&(q != NULL))
    {
      printf("Heap Realloc'd: value of p=%p value of q=%p\n",p,q);
      printf("Heap Realloc'd: value of *p=%d value of *q=%c\n",*p,*q);
    }

  // If we do not relase
  // Often all memory for a given process is released back to the system 
  // when process terminates. 
  // However, it's important to always free() your memory.
  // Some embedded systems may not always do so, however.
  // free(p); free(q);

  return 0;
} 
