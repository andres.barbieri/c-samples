/*
 * Ejemplo de uso de memoria din'amica b'asico
 * para tener como referencia.
 */

#include <stdlib.h> // malloc exit free
#include <stdio.h>

int main()
{
  // Declaración - Declaration
  int           *p    = NULL;     // int pointer
  int           count = 10;       // cantidad de ints

  p = (int *)malloc(count * sizeof(int));
  //p = malloc(count * sizeof(int));

  /* make sure if chunk is allocated or not */
  /* asegurarse que la memoria fue otorgada */
  if (p == NULL) {
    exit(1);
  }
  /* if (p != NULL) {       */  
  /* else use the mem chunk */
  /* si obtuvimos la memoria podemos usarla */
  *p = 0X123451;
  printf("%p %p %X %X\n",p,(((char*)p)+1), *p, *(((char*)p)+1)); 
  /* after your are done, free up memory */
  /* finalmente liberar la memoria */
  free(p); p = NULL;
  /* si no liberamos habitualmente al finalizar el proceso
   * se devuelven los recursos al sistemas, pero hay ocasiones
   * que esto no podr'ia ser cierto, por ejemplo el caso
   * de los sistemas embebidos.
   * Es importante liberar cuando no se utiliza
   */
  /* If we do not release ...
   * Often all memory for a given process is released back to the system 
   * when process terminates/exits. 
   * However, it's important to always free() your memory.
   * Some embedded systems may not always do so, however.
   */
  return 0;
} 
