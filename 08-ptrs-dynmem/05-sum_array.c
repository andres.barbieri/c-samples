#include <stdio.h>
#include <stdlib.h>


#define MIN(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x>_y)?(_y):(_x)); })


void show_a(int a[], int len_a)
{
  printf("[");
  for(int i=0;(i<len_a)&&(a!=NULL);i++)
    {
      printf("%6d ",a[i]);
    }   
  printf("]\n");
}

//int sum_a(int a[],int len_a,int b[], int len_b, int c[], int len_c)
	
int sum_a(int a[],int len_a,int b[], int len_b, int **c, int *len_c)
{
  *len_c=MIN(len_a,len_b);
 // int aux[len_c];
 //*c = aux;

  *c = (int*) malloc((*len_c)*sizeof(int));
  if ((*c) == NULL) return -1;
  for(int i=0;i<*len_c;i++)
   {
     (*c)[i]=a[i]+b[i];
     //*c[i]=a[i]+b[i]; Segmentation fault (core dumped)
   }
  return 0;
}

int main()
{
  int a[] = {1,2,3,4,5,6};
  int b[] = {3,4,4,5,5,5,5,5,5,55,};
  int *c  = NULL;
  int lc;
  printf("c=");show_a(c, (sizeof(c)/sizeof(int)));
  sum_a(a, (sizeof(a)/sizeof(int)), 
        b, (sizeof(b)/sizeof(int)),
        &c, &lc);
  printf("a=");show_a(a, (sizeof(a)/sizeof(int)));
  printf("+\n");
  printf("b=");show_a(b, (sizeof(b)/sizeof(int)));
  printf("---------------------------------------------------------------------------\n");
  printf("c=");show_a(c,lc);
  free(c);
  return 0;
}
