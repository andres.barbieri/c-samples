#include <stdio.h>
#include <stdlib.h> // malloc exit free
#include <malloc.h> // GNU extension malloc_usable_size 

int main()
{
  // Declaración - Declaration
  int           *p    = NULL;     // int pointer
  int           count;

  // Probar con diferentes taman~os / try with different sizes 
  //count = 1;
  //count = 5;
  count = 6;
  //count = 10;

  p = (int *)malloc(count * sizeof(int));
 
  /* make sure if chunk is allocated or not */
  if (p == NULL) {
    exit(1);
  }

  *p = 0;
  printf("%ld\n",malloc_usable_size (p));

  /* after your are done, free up memory */
  free(p); 
  return 0;
} 
