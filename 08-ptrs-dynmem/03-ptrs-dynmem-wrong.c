/*
 * Programa para ilustrar errores que se comenten programando
 * punteros en "C".
 * Analizar, encontrar los errores e indicar como se corrigen
 *
 * Utilizar el valgrind
 * valgrind --leak-check=full  ./03-ptrs-dynmem-wrong
 * ...
 *  ==4603== ERROR SUMMARY: 5 errors from 5 contexts (suppressed: 0 from 0)
 *
 * Utilizar Debugger y ejecutar paso a paso
 * gdb ./03-ptrs-dynmem-wrong 
 * list , break 51, run , next ... print p, print *p, ... next
 * 
 * Utilizar el debugger y habilitar la generaci'on del core dump
 * ulimit -c unlimited
 * ulimit -a
 * gdb ./03-ptrs-dynmem-wrong core
 *
 *
 */

#include <stdio.h>  // printf
#include <stdlib.h> // malloc , free

#define VAL    2
#define COUNT  5

void e(int **a) /* wrong */
{
  printf("%d\n",*a[VAL]);
}

void f(int **a) /* OK */
{
  printf("%d\n",(*a)[VAL]);
}

void g(int **a) /* OK */
{
  *a = malloc(sizeof(int)*COUNT);
}

void h(int *a) /* wrong */
{
  a = malloc(sizeof(int)*COUNT);
}

int main()
{
  // Declaración - Declaration
  int           *p;            // puntero a un entero
  int           *q;            // puntero a un entero
  int           count = 10;

  *p = 10; // Not allocated

   p = malloc(count * sizeof(int));

   *p = 10; // not checking p != NULL before use it
 
   p = malloc(count*2*sizeof(int)); // re-alloc without freeing before

   p = NULL;  
   printf("p = %p\n",p); 
   h(p);
   //g(&p);
   printf("p = %p\n",p);
   
   p[VAL] = VAL; 
   e(&p);
   //p[VAL] = VAL; f(&p);

   /* try freeing a part of it */
   free(p + 5);  
     
   /* free up more than once */
   free(p);
   free(p);

   /* free unallocated memory */
   free(q);

   return 0;
} 
