### Init

```console
cd source/c-samples
git init
git remote add origin git@gitlab.com:andres.barbieri/c-samples.git
```

### Add

```console
git add 00-main-format/
git commit -m "00 main format"
git push -u origin master
```

### Update

```console
git add -u 00-main-format/README.md
git commit -m "Readme update console format" 
git push -u origin master
```

### Remove

```console
git rm 00-main-format/*~
git commit --amend
git commit -m "Re commit 00 main format"
git push -u origin master
```
