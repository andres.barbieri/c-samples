# Proyecto de ejemplo para mostrar bibliotecas

## Objetivo

La idea es mostrar las formas de generar librerias/bibliotecas en "C". En esta etapa a�n no se inclyue el uso de Makefile, el cual simplificar�a sin necesidad de usar tantos scripts.

## Modo de uso (inspeccionar los scripts para ver que hacen)

```console
# Construir biblioteca estatica
sh mk-staticlib.sh

# Construir biblioteca dinamica
sh mk-sharedlib.sh

# Generar ejecutables dinamicos, parcialmente din�micos y estaticos

sh mk-exe
sh mk-exe2
sh mk-static-exe
```

Pare ejecutar uarea(ejecutble din�mico) ver execute.sh, para uarea2 y uarea-static, pueden ejecutarse directamente. Luego completar la biblioteca agregando c�lculo de vol�menes y el uso de las funciones.