#include <stdio.h>
#include <area.h>

int main()
{
  printf("%2.5f\n",a_rect(8.0,2.0));
  printf("%2.5f\n",a_circle(1.0));
  printf("%2.5f\n",a_square(2.0));
  printf("%2.5f\n",a_triangle(2.0,3.0));  
  return 0;
}
