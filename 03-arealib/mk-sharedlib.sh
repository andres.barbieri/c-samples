#!/bin/bash

gcc -fPIC -c area/area.c -Iarea          # compila, genera cod. objeto -fPIC (position-independent code)
gcc -fPIC -c area/vols.c -Iarea          # compila, genera cod. objeto -fPIC (position-independent code)
gcc -shared -o libarea.so area.o  vols.o # genera lib dinamica
mkdir -p lib                             # crea dir
mv libarea.so lib                        # coloca la lib en lugar para la linkedicion
ls lib

