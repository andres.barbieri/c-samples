#ifndef VOLS_H
#define VOLS_H 1

double v_cylinder(double r, double h);

double v_rect_prism(double l, double w, double h);

double a_trian_prism(double b, double h, double l);

double a_sphere(double r);

double a_pyramid(double l, double w, double h);

#endif // VOLS_H
