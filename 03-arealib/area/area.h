#ifndef AREA_H
#define AREA_H 1

#define PI ((double)3.14159)

double a_rect(double b, double h);

double a_square(double l);

double a_triangle(double b, double h);

double a_circle(double r);

double a_trapez(double b, double h, double B);

#endif // AREA_H
