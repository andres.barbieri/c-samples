#include <math.h>
#include "area.h"

double a_rect(double b, double h)
{
  return b * h;
}

double a_square(double l)
{
  return a_rect(l,l);
}

double a_triangle(double b, double h)
{
  return (a_rect(b,h)/2.0);
}

double a_circle(double r)
{
  return PI * pow(r,2.0);
}

double a_trapez(double b, double h, double B)
{
  return ((B + b)*h)/2;
}



