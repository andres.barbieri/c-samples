#!/bin/bash

gcc -Wall -c area/area.c -Iarea  # compila, genera cod. objeto
gcc -Wall -c area/vols.c -Iarea  # compila, genera cod. objeto
ar -cvq libarea.a area.o vols.o  # archiva los cod. objetos (genera lib estatica)
mkdir -p lib                     # crea dir
mv libarea.a lib                 # coloca la lib en lugar para la linkedicion
ls lib


