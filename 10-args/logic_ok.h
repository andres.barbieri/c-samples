#ifndef uLOGIC_H
#define uLOGIC_H
/* 
 * Macros para funciones l'ogicas
 */

#define NOT(a) \
   ({ __typeof__ (a) _x = (a); \
     (!_x); })

#define XOR(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       (((!_x)&&(_y))||((_x)&&(!_y))); })

#define AND(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x)&&(_y)); })

#define OR(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x)||(_y)); })

#define NOR(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       (!((_x)||(_y))); })

#define NAND(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       (!((_x)&&(_y))); })

#endif /* logic_ok.h */
