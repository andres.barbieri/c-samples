/*
 * Compilar con:
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "logic_ok.h"

#define OP_AND "--and"
#define OP_OR  "--or"
#define OP_XOR "--xor"

#define MAX_ARGS 256
#define OP_LEN   6

static void usage(char *argv[], int code)
{
 printf("usage: %s {--and|--or|--xor} n1 n2 [n3 n4 ... nn]\n",argv[0]);
 exit(code);
}

int main(int argc, char *argv[])
{
  int i;
  int op = 0;
  int result;
  int numbers[MAX_ARGS];
  
  if (argc < 4) 
    {
      usage(argv,1);
    }
  result = atoi(argv[2]);
  if (strncmp(argv[1],OP_AND,OP_LEN) == 0)
	{
	  op = 1;
	}
  else if (strncmp(argv[1],OP_OR,OP_LEN) == 0)
	{
	  op = 2;
	}
  else if (strncmp(argv[1],OP_XOR,OP_LEN) == 0)
	{
	  op = 3;
	}
  else
    {
      usage(argv,1);
    }
  // Collect arguments 
  for(i=3;(i<argc)&&(i<MAX_ARGS);i++)
    {
      numbers[i-3] = atoi(argv[i]);
    }
  // Process arguments
  for(i=0;i<argc-3;i++)
    {
      switch (op)
	{
	case 1: /* AND */
	  result = AND(result,numbers[i]); 	  
	  break;
	case 2: /* OR */
	  result = OR(result,numbers[i]); 	  
	  break;
	case 3: /* XOR */
	  result = XOR(result,numbers[i]); 	 
	  break;
	default:
	  printf("Shouldn't happen\n");
	  exit(2);	  
	}
    }
  printf("[%d]\n",result);
 exit(EXIT_SUCCESS);
}
