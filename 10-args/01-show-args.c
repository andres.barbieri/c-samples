/*
 * Programa para mostrar procesamiento de par'ametros/argumentos
 * v'ia linea de comando (command-line)
 *
 * El tama~no m'aximo de la l�nea de de comando (argumentos)
 * depende del SO/kernel.
 *
 * En sistemas Unix/POSIX.1 se puede consultar con la llamada sysconf(3)
 * y depende el execv.
 *
 * En GNU/Linux se puede:
 *
 * $ getconf -a | grep ARG_MAX     
 * ARG_MAX                            2097152
 * _POSIX_ARG_MAX                     2097152
 *
 * Ejemplo de corridas:
 *
 * $ ./01-show-args    argv[0]="./01-show-args"
 *
 * $ ./01-show-args 1 2
 * argv[0]="./01-show-args"
 * argv[1]="1"
 * argv[2]="2"
 *
 * $ ./01-show-args 1 2 3 hola uno otro
 * argv[0]="./01-show-args"
 * argv[1]="1"
 * argv[2]="2"
 * argv[3]="3"
 * argv[4]="hola"
 * argv[5]="uno"
 * argv[6]="otro"
 */

#include<stdio.h>

int main(int argc, char **argv)
//int main(int argc, char *argv[])
{
  int i;
  for(i=0;i<argc;i++)
    {
      printf("argv[%d]=\"%s\"\n",i, argv[i]);
    }
  return (0);
}

