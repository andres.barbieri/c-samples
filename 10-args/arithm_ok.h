#ifndef uARITHM_H
#define uARITHM_H
/* 
 * Macros para funciones aritm'eticas
 */

#define ADD(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x)+(_y)); })

#define SUB(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x)-(_y)); })


#define MUL(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x)*(_y)); })

#define DIV(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x)/(_y)); })

#endif /* arithm_ok.h */
