#ifndef MY_LOGIC_ARITHM
#define MY_LOGIC_ARITHM 1

int and(int x, int y);

int or(int x, int y);

int xor(int x, int y);

int add(int x, int y);

int not(int x);

#endif /* logic_arithm.h */
