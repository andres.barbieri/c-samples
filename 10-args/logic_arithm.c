#include "logic_arithm.h"
#include "logic_ok.h"
#include "arithm_ok.h"

int and(int x, int y)
{
  return AND(x,y);
}

int or(int x, int y)
{
  return OR(x,y);
}

int xor(int x, int y)
{
  return XOR(x,y);
}

int add(int x, int y)
{
  return ADD(x,y);
}

int not(int x)
{
  return NOT(x);
}

/** EOF **/

