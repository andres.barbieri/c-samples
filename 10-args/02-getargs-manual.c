#include <stdio.h>                      /* printf */
#include <stdlib.h>                     /* exit */
#include <stdbool.h>                    /* Booleans */
#include <string.h>                     /* strncmp */

#define OP_ARG_A        "-a"
#define OP_ARG_A_LONG   "--arga"

#define OP_ARG_B        "-b"
#define OP_ARG_B_LONG   "--argb"

#define OP_ARG_C        "-c"
#define OP_ARG_C_LONG   "--flagc"

#define OP_HLP          "-h"
#define OP_HLP_LONG     "--help"

#define OP_LEN          8

#define DEFAULT_OPT_ARG_A  "UNO"
#define DEFAULT_OPT_ARG_B  (65536)
#define DEFAULT_OPT_ARG_C  (false)
#define MAX_B              (65535)
#define MIN_B              (0)

static void usage(char *argv[], int code)
{
 printf("usage: %s {--argb|-b} <0..65535> [{--arga|-a} <string>] [--flagc|-c]\n",argv[0]);
 exit(code);
}

/*
 * Parse Common arguments
 */
static int parse_args(/*@ in @*/  int  argc, 
	 	      /*@ in @*/  char *argv[],
		      /*@ out @*/ char **arg_a,
		      /*@ out @*/ int  *arg_b,
		      /*@ out @*/ bool *flag_c)
{
     int  i;
     int arg_a_count  = 0;
     int arg_b_count  = 0;
     int flag_c_count = 0;

     *arg_a      = DEFAULT_OPT_ARG_A; 
     *arg_b      = DEFAULT_OPT_ARG_B;
     *flag_c     = DEFAULT_OPT_ARG_C;

     for(i=1;i<argc;i++)
       {
	 if ( (strncmp(argv[i],OP_ARG_A,OP_LEN) == 0)||(strncmp(argv[i],OP_ARG_A_LONG,OP_LEN) == 0) )
	   {
	     if ((i+1)<argc)
	       {
		 arg_a_count++;
		 i++;                            
		 *arg_a = argv[i];
	       }
	     else
	       {
		 return (int) false;
	       }
	   }
	 else if ( (strncmp(argv[i],OP_ARG_B,OP_LEN) == 0)||(strncmp(argv[i],OP_ARG_B_LONG,OP_LEN) == 0) )
	   {
	     if ((i+1)<argc)
	       {
		 arg_b_count++;
		 i++;
		 *arg_b = atoi(argv[i]);
	       }
	     else
	       {
		 return (int) false;
	       }
	   }
	 else if ( (strncmp(argv[i],OP_ARG_C,OP_LEN) == 0)||(strncmp(argv[i],OP_ARG_C_LONG,OP_LEN) == 0) )
	   {
	     flag_c_count++;
	     *flag_c = true;
	   }
	 else
	   {
	     return (int) false;	  	     
	   }
       }
     if ((arg_a_count>1) || (arg_b_count>1) || (flag_c_count>1))
       {
	 return (int) false;	  
       }
     return (int) ( (*arg_a != NULL) && (*arg_b > MIN_B) && (*arg_b < MAX_B) );
}

int main(int argc, char *argv[])
{
 char *arg_a;
 int  arg_b;
 bool flag_c;

 if (!parse_args(argc, argv, &arg_a, &arg_b, &flag_c))
  {
    usage(argv, EXIT_FAILURE);
  }
 printf("%s %d (%d)\n",arg_a, arg_b, flag_c);
 return (0);
}

