#include <stdio.h>                      /* printf */
#include <stdlib.h>                     /* exit */
#include <getopt.h>                     /* getopt */
#include <stdbool.h>                    /* Booleans */

#include "logic_arithm.h"

extern char *optarg;
extern int  optind, opterr;
//extern int optind, opterr, optopt;

#define MAX_V_COUNT     256

#define OP_NULL         0
#define OP_AND          1
#define OP_OR           2
#define OP_XOR          3
#define OP_ADD          4

#define OP_ARG_AND_LONG "and"
#define OP_ARG_AND      'a'

#define OP_ARG_OR_LONG "or"
#define OP_ARG_OR      'o'

#define OP_ARG_XOR_LONG "xor"
#define OP_ARG_XOR      'x'

#define OP_ARG_ADD_LONG "add"
#define OP_ARG_ADD      '+'  

#define OP_FLAG_NOT_LONG "not"
#define OP_FLAG_NOT      'n'

#define OP_HLP          'h'
#define OP_HLP_LONG     "help"

#define OPTION_STRING "aox+nh"

static struct option long_options[] = 
              {
              {OP_ARG_AND_LONG,  no_argument, 0, (int) OP_ARG_AND },
              {OP_ARG_OR_LONG,   no_argument, 0, (int) OP_ARG_OR },
              {OP_ARG_XOR_LONG,  no_argument, 0, (int) OP_ARG_XOR },
              {OP_ARG_ADD_LONG,  no_argument, 0, (int) OP_ARG_ADD },
              {OP_FLAG_NOT_LONG, no_argument, 0, (int) OP_FLAG_NOT },
              {OP_HLP_LONG,      no_argument, 0, (int) OP_HLP },
              {0, 0, 0, 0}
              };

static void usage(char *argv[], int code)
{
 printf("usage: %s [-n|--not]* {--and|-a|--or|-o|--xor|-x|--add|-+} n1 [n2 n3 n4 ... nn]\n",argv[0]);
 exit(code);
}

/*
 * Parse Common arguments
 */
static int parse_args(/*@ in @*/  int  argc, 
	 	      /*@ in @*/  char *argv[],
		      /*@ out @*/ int  (**arg_op)(int,int),
		      /*@ out @*/ int  *flag_not,
                      /*@ out @*/ int  *init_val,
                      /*@ out @*/ int  values[],
		      /*@ out @*/ int  *values_count)                     

{
     int  res;
     opterr = 0; // Supress getopt errors
 
     *values_count  = 0;    
     *arg_op        = NULL;
     *flag_not      = 0;
   
     while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
     //while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
     {
#ifdef DEBUG
       printf("%c %d %d [%s]\n",res,res,optind,argv[optind-1]);
#endif
       if  ((res == (int) OP_ARG_AND)&&(*arg_op == OP_NULL))	{
	  *arg_op = and; 
          *init_val = 1;
	}
       else if  ((res == (int) OP_ARG_OR)&&(*arg_op == OP_NULL))
	{
	  *arg_op   = or;
          *init_val = 0;
	}
       else if  ((res == (int) OP_ARG_XOR)&&(*arg_op == OP_NULL))
	{ 
	  *arg_op   = xor;
          *init_val = 0;
	}
       else if  ((res == (int) OP_ARG_ADD)&&(*arg_op == OP_NULL))
	{
	  *arg_op = add;
          *init_val = 0;
	}
       else if  (res == (int) OP_FLAG_NOT)
	{
	  (*flag_not)++;
	}
      else 
	{ 
          // Invalid Option, res ==  '?' 
	  return (int) false;
	}
     }
     if (optind==argc)
       {
         // There are no values
	 return (int) false;
       }
     if (optind<argc) 
       {
         // Collect parameters
         int i;
	 for(i=0;(i<(argc-optind))&&(i<MAX_V_COUNT);i++)
	   {
	     values[i] = atoi(argv[optind+i]);
	     (*values_count)++;
	   }
       }
     return ( *arg_op != NULL); // Are there enough values?
}

int main(int argc, char *argv[])
{
  int           (*arg_op)(int,int);
  int  values[MAX_V_COUNT];
  int  values_count;
  int  flag_not;
  int  init_val;
  int i;
  int result;
  
  if (!parse_args(argc, argv, &arg_op, &flag_not, &init_val, values, &values_count))
  {
    usage(argv, EXIT_FAILURE);
  }
#ifdef DEBUG
printf("OP=(%d) NOT TIMES=(%d) over %d values",arg_op, flag_not, values_count);
#endif

 result = init_val;
 for(i=0;(i<values_count)&&(i<MAX_V_COUNT);i++)
   {
     result = arg_op(result,values[i]);
   }
 for(i=0;i<(flag_not%2);i++)
   {
     result=not(result);
   }
  printf("[%d]\n",result);
 return (0);
}
