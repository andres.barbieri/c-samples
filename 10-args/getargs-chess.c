#include <stdio.h>                      /* printf */
#include <stdlib.h>                     /* exit */
#include <getopt.h>                     /* getopt */
#include <stdbool.h>                    /* Booleans */
#include <string.h>                     /* strncmp */

extern int opterr;

#define OP_ARG_A        'w'
#define OP_ARG_A_LONG   "white"

#define OP_ARG_B        'b'
#define OP_ARG_B_LONG   "black"

#define OP_ARG_C        'o'
#define OP_ARG_C_LONG   "output"

#define OP_ARG_D        'p'
#define OP_ARG_D_LONG   "play"

#define OP_HLP          'h'
#define OP_HLP_LONG     "help"

#define DEFAULT_OPT_ARG_A  NULL
#define DEFAULT_OPT_ARG_B  NULL
#define DEFAULT_OPT_ARG_C  NULL
#define DEFAULT_OPT_ARG_D  NULL

#define OPTION_STRING "w:b:p:o:h"

static struct option long_options[] = 
              {
              {OP_ARG_A_LONG, required_argument, 0, (int) OP_ARG_A },
              {OP_ARG_B_LONG, required_argument, 0, (int) OP_ARG_B },
              {OP_ARG_C_LONG, required_argument, 0, (int) OP_ARG_C },
              {OP_ARG_D_LONG, required_argument, 0, (int) OP_ARG_D },
              {0, 0, 0, 0}
              };

static void usage(char *argv[], int code)
{
 printf("usage: %s [{--arga|-a} <string>] {--argb|-b} <0..65535> [--flagc|-c]\n",argv[0]);
 exit(code);
}

/**
 * Parse Common arguments
 */
static int parse_args(/*@ in @*/  int  argc, 
	 	      /*@ in @*/  char *argv[],
		      /*@ out @*/ char **arg_a,
		      /*@ out @*/ char **arg_b,
		      /*@ out @*/ char **arg_c,
		      /*@ out @*/ char **arg_d,
)
{
     int  res;
     int arg_a_count  = 0;
     int arg_b_count  = 0;
     int arg_c_count  = 0;
     int arg_c_count  = 0;

     opterr = 0; // Supress getopt errors
     *arg_a      = DEFAULT_OPT_ARG_A; 
     *arg_b      = DEFAULT_OPT_ARG_B;
     *arg_c      = DEFAULT_OPT_ARG_C;
     *arg_c      = DEFAULT_OPT_ARG_D;
          
     while ( (res = getopt_long(argc, argv, OPTION_STRING, long_options, NULL)) != -1 )
     // while ( (res = getopt(argc, argv, OPTION_STRING)) != -1 )
     {
       if  (res == (int) OP_ARG_A) 
	{
          // Take care, RO
	  *arg_a = optarg;
          arg_a_count++; 
	}
      else if  (res == (int) OP_ARG_B)
	{
	  *arg_b = optarg;
	  arg_b_count++;
	}
      else if  (res == (int) OP_ARG_C)
	{
	  *arg_c = optarg;
	  arg_c_count++;
	}
     else if  (res == (int) OP_ARG_D)
	{
	  *arg_d = optarg;
	  arg_d_count++;
	}
      else if  (res == (int) OP_HLP)
	{
	  return (int) false;
	}
      else 
	{ 
          // Invalid Option, res ==  '?' 
	  return (int) false;
	}
     }
     if ( (arg_a_count>1) || (arg_b_count>1) || (arg_c_count>1) || (arg_d_count>1) )
       {
	 return (int) false;	  
       }
     //play
     return ( ((arg_d != NULL)&&(arg_a == NULL)&&(arg_b == NULL)&&(arg_c == NULL)) ||
              ((arg_d == NULL)&&(arg_a != NULL)&&(arg_b != NULL)&&(arg_c != NULL)) ||
              ((arg_d == NULL)&&(arg_a != NULL)&&(arg_b != NULL)&&(arg_c == NULL)) );
}

int main(int argc, char *argv[])
{
 char *arg_a;
 int  arg_b;
 bool flag_c;

 if (!parse_args(argc, argv, &arg_a, &arg_b, &flag_c))
  {
    usage(argv, EXIT_FAILURE);
  }
 printf("%s %d (%d)\n",arg_a, arg_b, flag_c);
 return (0);
}

