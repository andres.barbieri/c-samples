/*** Strictly Copy & Paste that file here ***/
#include "code.c"
/********************************************/

/*
 * --------------------------------------------------------------- 
 * USAGE:
 *
 * <command> <p1> <p2> .... <pn>
 * 
 * argv[1]  <values>   :  <description> 
 *                                                 
 * argv[2]  <values>   :  <description> 
 *
 * argv[3]  <values>   :  <description> 
 *
 * ...
 *
 * --------------------------------------------------------------- 
 *
 */

/*** Global Headers ***/
#include <stdio.h>   // printf
#include <stdlib.h>  // exit, EXIT_SUCCESS, EXIT_FAILURE
#include <stdbool.h> // bool, true, false ISO99 C

/*** Local Headers ***/
/*** Replace these lines with yours specific header files ***/
//#include "locals1.h"
//#include "locals2.h"

/*** Replace "N" with yours specific argument count value ***/
#define ARGCOUNT N

/**
 * arg:  command line first argument
 * code: EXIT_SUCCESS|EXIT_FAILURE
 */
static void usage(char *arg, int code)
{
    printf("usage: %s <params>\n", arg);
    if ( code != 0 )
    {
         exit(code);
    }
}

int main(int argc, char *argv[])
{
  if ( argc < ARGCOUNT )
  {
    usage(argv[0], EXIT_FAILURE);
  }
  return (0);
}

/*** EOF ***/
