// gcc scanf.c -O0 -fno-stack-protector
// ./a.out 
//

// a=0 s="."
// AA
// a=0 s="AA"
  
// a=0
// AAAAAAAAA
// a=65 s=AAAAAAAAA

// a=0 s=".dsfd."
// AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
// a=1094795585 s="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
// Segmentation fault (core dumped)


#include <stdio.h>

int main()
{
  int  a = 0;
  char s[8];
  //char s[8] = {0};
 
  printf("a=%d s=\"%s\"\n",a,s);
  scanf("%s",s);
  printf("a=%d s=\"%s\"\n",a,s);
  return 0;
}
	  
