/*
 * Programa de ejemplo de uso de fgets, sscanf con diferentes tipos.
 * Intenta ofrecer versiones seguras para hacer I/O con formato.
 *
 * NOTES:
 * ver notas en 03-scandata.c
 */
#include <stdio.h>
#include <string.h> // strchr

#define STR_LEN 32
#define BUF_LEN 256

void skip_line()
{    
  int c = 0;
  if (!feof(stdin))
    {
      while ((c != EOF)&&(c!='\n'))
	{
	  c = getchar();
	}
    }
}

void simple_skip_line()
{
  int c = 0;

  while ((c != EOF)&&(c!='\n')) {
    c = getchar();
  }
}

int get_int(int /*@out@*/ *v)
{
char line[BUF_LEN];
int  i;
if (fgets(line, sizeof(line), stdin)) {
  if (!strchr(line,'\n')) skip_line();
    if (1 == sscanf(line, "%d", &i)) {
      *v=i;
      return 1;
    }
 }
 return 0;
}

int get_double(double /*@out@*/ *v)
{
char line[BUF_LEN];
double f;
if (fgets(line, sizeof(line), stdin)) {
    if (!strchr(line,'\n')) skip_line();
    if (1 == sscanf(line, "%lf", &f)) {
      *v=f;
      return 1;
    }
 }
 return 0;
}

int main()
{
    int    cant;
    char   str[STR_LEN]; 
    double v;
    int res1  = 0;
    int res2  = 0;
    int res3  = 0; 

    while ((res1+res2+res3) != 3)
    {
        // En str tengo 0 bytes .. 31bytes+\0 
        printf("Ingrese los datos %%s:");
        res1 = (fgets(str, STR_LEN, stdin) != NULL);
	{
          // En str tengo 1 byte .. 31bytes+0 (puede o no \n) 1 antes 
	  char *p;
          // Quita newline si existe
	  if ((p=strchr(str, '\n')) != NULL) *p = 0;
          // Quedaron datos en el buffer ?
          if ((p == NULL)&&(!feof(stdin)))
	      simple_skip_line();
	}
	printf("Ahora ingrese %%d:");
        res2 = get_int(&cant);
	printf("Ahora ingrese %%lf:");
        res3 = get_double(&v);
	printf("Scan:(%d) str=%s cant=%d v=%lf\n",res1+res2+res3,str,cant,v);
    }
    return 0;
}
