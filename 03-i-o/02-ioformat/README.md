# Aprendiendo I/O formateada b�sica en "C"

Ejemplos de programa que muestran como funciona la "formated I/O" b�sica en el lenguaje "C". Estos ejemplos est�n orientados a I/O interactiva por terminal. En general para programas "seguros" no se recomienda utilizar estas funciones `printf()` y/o `scanf()` debido a que su complejidad y forma de trabajar, son propensas a buffer overflows. Adem�s si los formatos no concuerdan pueden generar que el programa termine de forma anormal.

## Introducci�n

### Pre-requisitos

Sistema UNIX compatible (??).
Compilador GCC, make, man

## Pruebas
 
Ver el las p�ginas del manual como es el uso de las funciones `printf()` y `scanf()`. Para esto se requiere tener instalados las man page de desarrollo.

### Primeros Pasos

Ejecutar

```console
$ make
```

Para el c�digo `00-prnums.c` analizar el programa y la salida al ejecutarlo.

Para el c�digo `./01-prstr.c` analizar el programa y la salida al ejecutarlo.

Para el c�digo `./02-scanint.c` analizar los siguientes juegos de datos como entrada. Qu� sucede con los datos que no se estaban en el formato adecuado y fueron consumidos ? Las l�neas que comienzan con "Scan" son la salida del programa.

```console
$ ./02-scanint 
(123)
Scan:(1) 123
100 200 300
Scan:(3) 100 200 300

$ ./02-scanint 
(123)
Scan:(1) 123
100
200
300
Scan:(3) 100 200 300

$ ./02-scanint 
(123)
Scan:(1) 123
100	200	300
Scan:(3) 100 200 300

$ ./02-scanint 
(123)
Scan:(1) 123
100 200 300 400 500 600 700
Scan:(3) 100 200 300

$ ./02-scanint 
(123)
Scan:(1) 123
100 aaaa bbbb
Scan:(1) 100 32767 0

$ ./02-scanint 
(123)
Scan:(1) 123
100 200 bbbbbb
Scan:(2) 100 200 0

$ ./02-scanint 
(123)
Scan:(1) 123
aaa
Scan:(0) 123 32764 0

$ ./02-scanint 
aaaaa
Scan:(0) -472859424
Scan:(0) -472859424 32764 0

$ ./02-scanint 
100
Scan:(0) -1080502544
200 300 400 500 
Scan:(3) 100 200 300

$ ./02-scanint 
Scan:(-1) 570576160
CTRL+D
100 200 300
Scan:(3) 100 200 300

$ ./02-scanint 
qqqqq
Scan:(0) 719572320
Scan:(0) 719572320 32765 0

$ ./02-scanint 
(-345)
Scan:(1) -345
-12 -34 -54
Scan:(3) -12 -34 -54

$ ./02-scanint 
(--345)
Scan:(0) -1665513360
Scan:(1) -345 32766 0
```

Para el c�digo `./03-scandata.c` analizar el c�digo y probar con los juegos de datos sugeridos en el c�digo.


```console
$ ./03-scandata
Ingrese los datos %s %d %lf:12345678901234567890123456789 23 23.45
Scan:(3) str=12345678901234567890123456789 cant=23 v=23.450000

$ ./03-scandata
Ingrese los datos %s %d %lf:12345678901234567890123456789 aaaa 23.45
Scan:(1) str=12345678901234567890123456789 cant=1079149000 v=0.000000
Ingrese los datos %s %d %lf:Scan:(3) str=aaaa cant=23 v=0.450000

$ ./03-scandata
Ingrese los datos %s %d %lf:12345678901234567890123456789 23 aaaaaaaaa
Scan:(2) str=12345678901234567890123456789 cant=23 v=0.000000
Ingrese los datos %s %d %lf:12345678901234567890123456789Scan:(2) str=aaaaaaaaa cant=-1 v=0.000000
Ingrese los datos %s %d %lf:1234 1234 ^D
Scan:(2) str=1234 cant=1234 v=0.000000
Ingrese los datos %s %d %lf:1234 ^D
Scan:(2) str=^D cant=1234 v=0.000000
Ingrese los datos %s %d %lf:Scan:(1) str=^D cant=1234 v=0.000000
Ingrese los datos %s %d %lf:Scan:(-1) str=^D cant=1234 v=0.000000
Ingrese los datos %s %d %lf:Scan:(-1) str=^D cant=1234 v=0.000000
Ingrese los datos %s %d %lf:Scan:(-1) str=^D cant=1234 v=0.000000
Ingrese los datos %s %d %lf:01235678901234567890123456789012345678901234567890 2 2
Scan:(3) str=01235678901234567890123456789012345678901234567890 cant=2 v=2.000000
*** stack smashing detected ***: ./03-scandata terminated
Aborted

$ ./03-scandata
Ingrese los datos %s %d %lf:1111111111111111111111111111111111111111111111111111111111111 2 3
Scan:(3) str=1111111111111111111111111111111111111111111111111111111111111 cant=2 v=3.000000
*** stack smashing detected ***: ./03-scandata terminated
Aborted
```

El smashing detected es claramente un buffer overrun. En este caso producido porque se pretende escribe fuera del espacio reservado de la variable y un poco m�s. Se detecta debido a un mecanismo de protecci�n agregado por el compilador gcc. Ver `man gcc`. `-fstack-protector` y `-fno-stack-protector` y `-Wstack-protector`. El gcc genera c�digo extra para este chequeo en tiempo de ejecuci�n. Se agregan variables de guarda/protecci�n en el stack al invocar determinadas funciones o tener buffers de determinado tama�o. Las variables se inicializan en un valor conocido al entrar y se chequean al salir, si dan valores diferentes el programa recibe la se�al de SIGABRT y este debe terminar. Si se compila de esta forma y se ejecuta los resultados ser�n distintos:

``` console
$ gcc -Wall -std=c99    03-scandata.c   -o 03-scandata -fno-stack-protector

$ ./03-scandata 
Ingrese los datos %s %d %lf:11111111111111111111111111111111111111111111111111111111111111111111111111111111
1
1
...
Segmentation fault
```

Analizar los c�digos comparar y ver los resultados de los programas:

* `03-scandata.c` (visto anteriormente) 
* `04-scandata.c`  
* `05-scandata.c`  
* `06-scandata.c`  
* `07-scandata.c`
* `08-scandata.c`

Para la versi�n 07 ver la reserva de memoria din�mica de forma autom�tica. Y tener en cuenta que se debe liberar de forma manual si la lecutra tuvo �xito. Ver el ejemplo de lectura con formato `[a-z|A-Z]`. Para 06 y 07 se muestra el l�mite de la variable a leer, pero no se realiza la limpieza del buffer de entrada.

Para la versi�n 08 se muestran otros mecanismos m�s seguros de hacer entrada.


