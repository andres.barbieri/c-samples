/*
 * Programa de ejemplo de uso de scanf con comb. de tipos
 *
 * NOTES:
 * Probar con las siguientes entradas:
 * \n es <ENTER> ^D es la sec CTRL+D (Unix like)
 *
 * 12345678901234567890123456789 23 23.45
 * 12345678901234567890123456789\n 23\n 23.45\n
 * 12345678901234567890123456789 aaaa 23.45
 * 12345678901234567890123456789 23 23
 * 1 1 1 
 * 12345678901234567890123456789 23 aaaaaaaaa
 * 12345678901234567890123456789 ^D ^D
 * 1234 1234 ^D
 * 1234 ^D
 * ^D ^D ^D
 * 01235678901234567890123456789012345678901234567890 2 2
 * 1111111111111111111111111111111111111111111111111111111111111 2 3
 */
#include <stdio.h>

#define STR_LEN 32

int main()
{
    char   str[STR_LEN]; // Es suficiente ???
    int    cant;
    double v;
    int res;
    while (res != 3)
    {
	printf("Ingrese los datos %%s %%d %%lf:");
	res = scanf("%s %d %lf", str, &cant, &v);
        // Qu'e pasa con los datos en el buffer que no ten'ian el formato esperado ?
        printf("Scan:(%d) str=%s cant=%d v=%lf\n",res,str,cant,v);
    }
    return 0;
}
