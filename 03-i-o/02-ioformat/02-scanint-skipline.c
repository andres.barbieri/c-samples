/*
 * Programa de ejemplo de uso de scanf en enteros
 */

#include <stdio.h>

// Skipline. vacia buffer visto en: 01-get-eof
void skip_line();

int main()
{
  int r,x,y,z;
  
  // Espera los "(" ")"
  printf("Ingrese: ( %%d ):");
  r=scanf("( %d )",&x); 
  printf("Scan:(%d) %d\n",r,x);
  // skipline, vacia buffer
  skip_line();
  printf("Ingrese 3 ints: %%d %%d %%d:");
  // Espacios y Enters en la entrada se eliminan
  // Todas estas exp. parecen dar el mismo resultado
  //r=scanf("%d %d %d",&x,&y,&z);           
  //r=scanf("%d%d%d",&x,&y,&z);              
  //r=scanf("%d      %d      %d",&x,&y,&z);  
  //r=scanf("%d\n%d\n%d",&x,&y,&z);          
  r=scanf("%d\t%d\t%d",&x,&y,&z);            
  printf("Scan:(%d) %d %d %d\n",r,x,y,z);
  return 0;
}


