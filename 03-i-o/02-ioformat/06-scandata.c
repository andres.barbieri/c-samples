/*
 * Programa de ejemplo de uso de scanf con comb. de tipos
 *
 * NOTES:
 * ver notas en 03-scandata.c
 */
#include <stdio.h>

#define STR_LEN 32
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

void simple_skip_line()
{
  int c = 0;

  while ((c != EOF)&&(c!='\n')) {
    c = getchar();
  }
}

int main()
{

    int    cant;
    char   str[STR_LEN]; // Es suficiente ???
    double v;
    int res;

    while (res != 3)
    {
	printf("Ingrese los datos %%s %%d %%lf:");
	res = scanf("%"TOSTRING(STR_LEN)"s %d %lf", str, &cant, &v);
        simple_skip_line();
        printf("Scan:(%d) str=%s cant=%d v=%lf\n",res,str,cant,v);
    }
    return 0;
}
