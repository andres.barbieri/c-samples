/*
 * Programa de ejemplo de uso de printf con n'umeros
 */

#include <stdio.h>

int main()
{
  float            f  = 2/23.0;
  int              i  = 34;
  long long int    ii = 3333333333333334;
  double           d  = -2/23.0;

  printf("sizeof(int)=%d sizeof(long int)=%d sizeof(long long int)=%d\n",sizeof(int),sizeof(long int), sizeof(long long int));
  printf("%16d = 0x%016X\n",i,i);
  printf("%16d = 0x%016X\n",ii,ii); // Format warning expects int found llint
  printf("%16lld = 0x%016llX\n",ii,ii);
  printf("%3.5f = %e\n",f,f);
  printf("%3.50lf\n",d);
  printf("%+3.50lf\n",-d);
  printf("%016X\n",f); // Format Warning
  printf("%016X\n",(int)f);
  return 0;
}
