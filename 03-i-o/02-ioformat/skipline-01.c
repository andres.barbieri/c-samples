#include <stdio.h>

void skip_line()
{    
  int c = 0;
  if (!feof(stdin))
    {
      while ((c = getchar()) != '\n' && c != EOF);
    }
}

