/*
 * Programa de ejemplo de uso de scanf con comb. de tipos
 *
 * NOTES:
 * ver notas en 03-scandata.c
 */
#include <stdio.h>
#include <stdlib.h>

void simple_skip_line()
{
  int c = 0;

  while ((c != EOF)&&(c!='\n')) {
    c = getchar();
  }
}

int main()
{
    int    cant;
    char   *str; 
    double v;
    int res  = 0;
    int res2 = 0;

    while ((res+res2) != 3)
    {
         printf("Ingrese los datos %%s:");
        // Dynamic allocation
	//res = scanf("%ms",&str);
	res = scanf("%m[a-z|A-Z|0-9]",&str);
	simple_skip_line();
        if (res == 1)
	  {
            printf("Ahora ingrese %%d %%f:");
	    res2 = scanf("%d %lf", &cant, &v);
	    simple_skip_line();
	  }
	  printf("Scan:(%d) str=%s cant=%d v=%lf\n",res+res2,str,cant,v);
	  if (res == 1)
	    {
	      free(str);
	    }
    }
    return 0;
}
