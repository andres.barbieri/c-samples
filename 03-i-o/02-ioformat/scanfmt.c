/*
 * Programa para comparar el comportamiento de scanf
 * con formato con Cannonical I/O.
 *
 * NOTES:
 *
 * Compilar:
 *
 * gcc -o scanfmt scanfmt.c -DINT
 * gcc -o scanfmt scanfmt.c -DCHAR
 * gcc -o scanfmt scanfmt.c -DFLT
 * gcc -o scanfmt scanfmt.c -DHEX
 * gcc -o scanfmt scanfmt.c -DSTR
 *
 *
 * Sin datos:
 *
 * Si la entrada es vac'ia todas se desbloquean
 * inmediatamente, es decir procesan el EOF
 * y salen.
 *
 * -----------------------------------
 * Con datos FLT INT STR
 *
 * Probar que con float, int y string: 
 * cualquier dato fuera de formato es
 * devuelto al buffer, incluso el \n
 * Si se sale con EOF y hay datos fuera
 * del formato quedan en el buffer y se
 * desbloquea.
 *
 * En el caso de leer el dato y consumir
 * toda la entrada, es decir se entra dato v�lido y
 * seguido un EOF en lugar de \n, requieren otro EOF 
 * u otro \n para terminar de desbloquear. No queda
 * nada en el buffer.
 *
 * En el caso que el dato sea \n solamente
 * no se consume y permanece bloqueadas.
 * requieren un dato un EOF para desbloquea.
 *
 * Con datos CHAR
 *
 * Para el caso de char, consume cualquier entrada
 * includo el \n, pero solo un byte. Tener en cuenta que 
 * el EOF no es una entrada.
 *
 * En el caso de leer el dato y consumir toda la entrada,
 * es decir se cargo un byte y luego EOF, se desbloquea  
 * y nada queda en el buffer.
 * 
 */

#include <stdio.h>

int main()
{
#if  (defined FLT)  
#define MSG "ingrese un float:"
#define FMT "%f"
  float var;
#elif (defined INT)
#define MSG "ingrese un int:"
#define FMT "%d"
  int var;
#elif (defined CHAR) 
#define MSG "ingrese un char:"
#define FMT "%c"
  char var;
#elif (defined HEX) 
#define MSG "ingrese un int en formato hex:"
#define FMT "%X"
  int  var;
#else // STR
#define FMT "%s"
#define MSG "ingrese un string:"
  char buf[256];
  char var = buf[0];
#endif 
  int r;

  printf(MSG);
  r = scanf(FMT,&var);
  printf("\nr=%d\n",r);
  fflush(stdout);
  if (r == EOF)
    {
      printf("El buffer qued� vac�o\n");
    }
  else
    {
      printf("En buffer qued�:");   
      while ((r=getchar()) != EOF)
	{
                    printf("[%d]",r);
                    fflush(stdout);
	}
    }
  return 0;
}


