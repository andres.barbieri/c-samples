/*
 * Programa de ejemplo de uso de scanf con comb. de tipos
 *
 * NOTES:
 * Probar con las siguientes entradas:
 *
 * 12345678901234567890123456789 23 23.45
 * 12345678901234567890123456789 aaaa 23.45
 * 12345678901234567890123456789 23 23
 * 1 1 1 
 * 12345678901234567890123456789 23 aaaaaaaaa
 * 12345678901234567890123456789 ^D
 * 1234 1234 ^D
 * 1234 ^D
 * ^D ^D ^D
 */
#include <stdio.h>

#define STR_LEN 32

void simple_skip_line()
{
  int c = 0;

  while ((c != EOF)&&(c!='\n')) {
    c = getchar();
  }
}

int main()
{

    int    cant;
    char   str[STR_LEN]; // Es suficiente ???
    double v;
    int res;

    while (res != 3)
    {
	printf("Ingrese los datos %%s %%d %%lf:");
	res = scanf("%s %d %lf", str, &cant, &v);
        simple_skip_line();
        printf("Scan:(%d) str=%s cant=%d v=%lf\n",res,str,cant,v);
    }
    return 0;
}
