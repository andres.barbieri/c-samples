/*
 * Programa de ejemplo de uso de puts, fputs y printf
 * (salida de strings)
 */

#include <stdio.h>

#define STR  "1234567890"
#define STRn "1234567890\n"

int main()
{
  puts(STR); // add new line auto'lly
  fputs(STR, stdout);
  fputs("\n",stdout);  
  printf("%s",STRn); 
  printf("%s\n",STR); 
  printf("%40s\n",STR); 
  printf("%-40s\n",STR); 
  printf("%s\n",STR STR STR STR); // cpp and compiler concat
  printf("%s%s%s%s\n",STR,STR,STR,STR); 
  printf("%.5s\n",STR);
  printf("%.*s %.*s\n",5,STR,8,STR);
  printf("%d=printf\n",printf("%s\n",STR));
  printf("%d=printf\n",fprintf(stdin,"%s\n",STR));
  printf("%d=printf\n",fprintf(stdout,"%.5s",STR));
  return 0;
}
