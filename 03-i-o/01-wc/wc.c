#include <stdio.h>

#define TAB   '\t'
#define SPACE ' '
#define CR    '\n'
#define IN    0

int main(int argc, char *argv[]) 
{
  int  n_lines    = 0;
  int  n_words    = 0;
  int  n_chars    = 0;
  int  c          = IN;
  int  found_word = 0;
  
  while (c != EOF)
    {
      c = getchar();
      // word
      while ( (c != EOF) && (c != SPACE ) && (c != TAB ) && (c != CR) )
	{
	  found_word = 1;
	  n_chars++;
	  c = getchar();
	}
      if (found_word) n_words++;
      found_word = 0;
      // spaces
      while ( ((c != EOF) && (c != CR)) && ((c == SPACE ) || (c == TAB )) )
	{
	  n_chars++;
	  c = getchar();
	}
      if (c!=EOF) n_chars++;
      // lines
      if (c==CR) n_lines++; 
    }
  printf("%2d %2c %2d %2c %2d\n",n_lines,' ',n_words,' ',n_chars);
  return (0);
}  	       
