/* 
 * Simple Line Counter (lc). 
 * Counts chars and lines
 * 
 */

#include <stdio.h>

#define TAB   '\t'
#define SPACE ' '
#define CR    '\n'
#define IN    0

int main() 
{
     char c;
     int  n_chars = 0;
     int  n_lines = 0;

     c = getchar();
     while (c != EOF) {
       if ( (c == CR) || ((c == EOF)&&(n_chars!=0)) ) {
	 n_lines++;
       } 
       if (c != EOF) {
	 n_chars++;
       }
       c = getchar();
     }
     printf("%7d %7c %7d\n",n_lines,' ',n_chars);
     return (0);
}
      
/*** EOF ***/

    
