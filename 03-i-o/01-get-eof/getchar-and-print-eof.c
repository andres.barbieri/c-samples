#include <stdio.h> 


int main()
{
  int c = 0;
  while (c!=EOF)
    {
      c = getchar();
      if ( (c >= 21) && (c <= 126) )
	printf("[%c,0x%x]",c,c);
      else if (c == '\n')
	printf("[\\n,0x%x]",c);
      else
	printf("[\\sc,0x%x]",c);
    }
  return 0; // unreachable code
}

/*** EOF ***/
