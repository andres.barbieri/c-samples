#include <stdio.h> 


int main()
{
  int c;
  while (1)
    {
      c = getchar();
      if ( (c >= 21) && (c <= 126) )
	printf("[%c,0x%x]",c,c);
      else if (c == '\n')
	printf("[\\n,0x%x]",c);
      else
	printf("[\\sc,0x%x]",c);
    }
  return 0; // unreachable code
}

/*** EOF ***/
