/*
 * Compila en GNU/Linux
 *
 * http://pubs.opengroup.org/onlinepubs/7908799/xsh/termios.h.html
 *
 * Source: http://shtrom.ssji.net/skb/getc.html
 *         http://michael.dipperstein.com/keypress.html
 */

#include <stdio.h>
#include <unistd.h>
#include <termios.h>

#ifndef STDIN_FILENO
# define STDIN_FILENO  0
#endif

int main()
{
  struct termios old_tio, new_tio;
  int c;

  /* get the terminal settings for stdin */
  if (tcgetattr(STDIN_FILENO,&old_tio)<0) 
    {
      perror("tcgetattr()");
      return 1;
    }
  /* we want to keep the old setting to restore them a the end */
  new_tio=old_tio;

  /* disable canonical mode (buffered i/o) and local echo */
  //new_tio.c_lflag &= ~ICANON;
  //new_tio.c_lflag &= ~ECHO;
  new_tio.c_lflag &=(~ICANON & ~ECHO);

  // Is it needed ?
  //new_tio.c_cc[VMIN]  = 1;
  //new_tio.c_cc[VTIME] = 0;

  /* set the new settings immediately */
  if (tcsetattr(STDIN_FILENO,TCSANOW,&new_tio)<0)
    {
     perror("tcsetattr ICANON");
     return 1;
    }

  printf("End with a 'q'\n");  
  // Checking getchar result is not carried out
  do {
     c=getchar();
     printf("%d ",c);
  } while(c!='q');
  
  /* restore the former settings */
  // old.c_lflag |= ICANON;
  // old.c_lflag |= ECHO;
  if (tcsetattr(STDIN_FILENO,TCSANOW,&old_tio)<0)
    {
      perror ("tcsetattr ~ICANON");
      return 1;
    }

  return 0;
}

/** EOF **/
