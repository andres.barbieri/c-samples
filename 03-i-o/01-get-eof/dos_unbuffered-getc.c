/*
 * Solo compila en Microsoft DOS / MS C Compiler
 */
#include <stdio.h>
#include <conio.h> // MS specific library

int main() 
{ 
  char ch; 

  printf("End with a 'k'\n");  
  do { 
    ch=getch(); 
    printf("%c",ch); 
  } while(ch!='q'); 
  return 0;
}

