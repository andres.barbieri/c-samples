# Aprendiendo I/O b�sica en "C"

Ejemplos de programa que muestran como funciona la I/O b�sica en el lenguaje "C".
Estos ejemplos est�n orientados a I/O interactiva por terminal.

## Introducci�n

### Tipos de dispositivos:

* Comunicaci�n:

 * Input (Entrada):  keyboard, mouse, joystick, webcam 
 * Output (Salida): visual display (monitor), printer
 * Input/Output: NIC, modem, UART (port serial), USB

* Storage

 * Input: CD-ROM drive
 * Input/Output: hdd, tape

### Modos de hacer I/O Input/Output (Entrada/Salida):

* Orientado a bits (bajo nivel: hardware)
* Orientado a bytes/chars (bajo nivel: hardware, OS -sistema operativo-)
* Orientado a streams (Default de programas sobre el OS)
* Orientado a bloques, block-oriented (bajo nivel y alto nivel)

###  I/O Orientada a Streams (Stream-oriented)

I/O Orientada a Streams es una forma portable, flexible y "eficiente" de r/w datos.
El OS ofrece una interfaz orientada a streams para hacer I/O a los programas de usuario.
Esta implementada esta interfaz en la biblioteca: `libc`.
Se puede ver a un stream como una secuencia de datos, chars o bytes, y que est�n disponibles para ser accedidos para "r", "w" o "r/w".
Un stream es habitualmente un archivo o un dispositivo (impresora, teclado) manipulado mediante un buffer asignado por el OS y un puntero al mismo. Puede tambi�n asociarse con un socket de red, por ejemplo un TCP stream.
Cuando se r/w sobre un stream no se lo hace directamente sobre el archivo o dispositivo sino se lo hace a trav�s del buffer. Toda la I/O "streamed" se dice que es "buffered". Se hace a trav�s de porciones, "chunks" pasando por el almacenamiento temporal, buffer. 
Stream-oriented es el default de los programas que se ejecutan mediante un terminal de texto (terminal real o emulada). En este caso se utiliza Canonical I/O:

```console

HW    |  OS  | User Space
------+------+----------------------------
      |      |
DEV<->| DEV  |<--->|Buffer|<--->C program
      |DRIVER|
      |      |        

```
### Buffering

Se definen tres tipos de buffering de acuerdo a `man 3 setbuf`:

* Unbuffered
* Block buffered
* Line buffered

Si es unbuffered los datos van a dar al archivo, terminal o dispositivos tan pronto como sean escritos. En el modo block buffered se almacenan varios bytes en memoria temporal y se escirben como un bloque. En general se trabaja con tama�os de bloques fijos. En el modo/tipo line-buffered se almacenan caracteres y son enviados/recibidos luego de new-line o EOF. Com�nmente los archivos se manejan de forma block buffered. 

### Modo de acceso de texto, Stream-oriented (Canonical I/O)

Al acceder por terminal para hacer I/O, para el input se trabaja por default en un modo llamado "Canonical I/O" o directamente "Canonical" (a fines pr�cticos tiene que ver con la entrada, I). En este modo para hacer el acceso m�s eficiente se almacena de a l�nea en el buffer, `Line-Buffering`, en lugar de a char/byte. El sistema define la longitud m�xima del buffer. Las caracter�sticas son:

a) Se trabaja de a una l�nea por vez.
b) La entrada de datos del programa/usuario desde la terminal se se�aliza habitualmente con un EOL(End of Line): new-line, generado por <ENTER> o la secuencia EOL de la terminal, para indicar el fin de la l�nea y que los datos est�n disponibles. 
c) La salida es tambi�n de una l�nea por vez hacia la terminal.
d) Si no hay datos en el buffer de entrada el proceso de usuario se bloquea esperando la se�al de datos disponibles.
e) Si el buffer de salida esta lleno el proceso de usuario se bloquea esperando que se haga espacio.
f) Por default para la entrada esta habilitado el ECHO. Es decir lo que se tipea, si es un c�digo v�lido se env�a al buffer de salida (se muestra en pantalla).
g) El fin de la entrada completa (close) se se�aliza con EOF (End of File). EOF no es informaci�n que se encuentra en el stream de datos, es la forma de se�alizar al programa de usuario que no hay m�s datos para leer. 

```console
$ grep -A3 -B3 "define EOF" /usr/include/stdio.h 
 /* End of file character.
    Some things throughout the library rely on this being -1.  */
 #ifndef EOF
 # define EOF (-1)
 #endif
```

### Stream Predefinidos

En los sistemas UNIX, o m�s bien POSIX compatibles existen 3 streams predefinidos (en `stdio.h`):

* `stdin`
* `stdout` 
* `stderr` 

Por ejemplo se puede encontrar la siguiente definici�n (no es la misma para todos los sistemas):

```console
$ less /usr/include/stdio.h

/* Define outside of namespace so the C++ is happy.  */
struct _IO_FILE;
...
/* The opaque type of streams.  This is the definition used elsewhere.  */
typedef struct _IO_FILE FILE;
...
extern struct _IO_FILE *stdin;		/* Standard input stream.  */
extern struct _IO_FILE *stdout;		/* Standard output stream.  */
extern struct _IO_FILE *stderr;		/* Standard error output stream.  */
...
/* C89/C99 say they're macros.  Make them happy.  */
#define stdin stdin
#define stdout stdout
#define stderr stderr
...
```

En "C" existe una estructura FILE que representa la mayor�a de los streams y esta definida en `stdio.h`.
`stdin`, `stdout` y `stderr` son streams, en particular son punteros a archivos `*FILE pointers`.
En el caso de "C" un FILE pointer es un tipo a nivel de biblioteca, para representar un archivo de forma portable.
Mediante un FILE se agrega a la representaci�n ofrecida por OS del archivo, el buffering y otras caracter�sticas que hacen la I/O m�s f�cil.
En el caso de un sistema tipo UNIX un archivo a bajo nivel se accede mediante un descriptor de archivos, file descriptor (fd).
Un fd es un entero usado como manejador,"handler" a bajo nivel, identifica un stream abierto (file, socket o lo que fuese a nivel de kernel).  En este caso el FILE pointer contiene, el file descriptor.

Para el caso de `stdin`, `stdout` y `stderr`, est�n abiertos de forma autom�tica (no se requiere llamar a `fopen`).
La pantalla de la terminal es el default `stdout` y `stderr`, y el teclado de la terminal es el default `stdin`, es decir que por default est�n asociadas al teclado de la terminal y la salida de la terminal en pantalla.
Heredan del programa padre, shell: los 3 streams `stdin`, `stdout` y `stderr`. Con la siguiente l�nea se puede ver la relaci�n de los procesos.

```console
$ pstree -s `ps -elf | grep bgetch | grep -v grep | awk '{print $4}'`
init---lightdm---lightdm---init---gnome-terminal---bash---000-bgetch 

$ pstree -s `ps -elf | grep bgetch | grep -v grep | awk '{print $4}'`
init---login---bash---000-bgetch
```

![stdio](stdio.png)
[`fuente: https://en.wikipedia.org/wiki/Standard_streams`](https://en.wikipedia.org/wiki/Standard_streams)


Para terminal I/O, modo canonical, por default stdin y stdout son line-buffered y stderr se maneja unbuffered.

### POSIX Canonical I/O, EOF y EOL

Los sistemas POSIX soportan dos modos b�sicos de hacer entrada para los usuarios: canonical y non-canonical. El modo canonical es el modo normal de procesamiento que se utiliza con los streams en una terminal de texto y su funcionamiento es b�sicamente el que describi� en la secci�n `Modo de acceso de texto Stream-oriented (Canonical I/O)`. El procesamiento se hace por l�neas terminadas en EOL: new-line ('\n') o EOF. En este modo se provee la facilidad de hacer input-editing previo a enviar los datos al buffer. Se lee como m�ximo de a una l�nea.

Si la entrada se cierra, no tiene m�s datos o se recibe la secuencia EOF, si el buffer de entrada tiene datos, fuerza que los datos vayan al programa hasta el fin, EOF. El programa los podr� leer. El EOF como no es un char v�lido, no se lee por el programa. Si el buffer estaba vac�o se env�a al programa una "se�al" indicando que se llego al EOF y al programa que lee retornar� el control. La forma de enviar la se�al EOF de una entrada interactiva, por teclado/keyboard, habitualmente es una secuencia de Control. En los sistemas UNIX es: `CTRL+D`, `^D`.

Para el caso del EOL, si es un dato que se va a encontrar en el stream y se podr� seguir leyendo otros datos despu�s de este si se encontrasen. El c�digo del <ENTER> se agrega al buffer, por ejemplo en UNIX: '\n', 10, 0xA (Line Feed). El <ENTER> tambi�n se puede generar de forma interactiva con la secuencia `CTRL+M` en modo canonical. En el caso de otros sistemas el valor generado por el <ENTER> puede tener m�s de un char por ejemplo en los sistemas basados en MS Windows <ENTER> == "\r\n" (Carrier Return, Line Feed). El CR no es un terminador de l�nea para el modo de entrada canonical. 
 
De acuerdo a Advanced Programming in the UNIX Environment (Stevens), la mayor�a de los sistemas UNIX implementan el procesamiento canonical en un m�dulo llamado "Terminal Line Discipline". Se puede ver el m�dulo como una caja entre las funciones de r/w gen�ricas del kernel y el driver. En el texto se muesta una figura como:

```console
          User Space

 (user-program)
      ^
      |
------+----------------
      |   Kernel Space
 +--------------+
 |  generic r/w |
 +--------------+
      |
 +----+---------+
 |terminal disc |           
 +----+---------+
      |
 +----+---------+
 |device driver |           
 +----+---------+
      |
------+----------------
      |
   (device)
``` 

### Informaci�n Adicional

La estructura opaca `FILE` se puede ver implementada en versiones del sistema GNU/Linux de la siguiente manera:

```console
$ less /usr/include/libio.h
...
struct _IO_FILE {
  int _flags;           /* High-order word is _IO_MAGIC; rest is flags. */
#define _IO_file_flags _flags

  /* The following pointers correspond to the C++ streambuf protocol. */
  /* Note:  Tk uses the _IO_read_ptr and _IO_read_end fields directly. */
  char* _IO_read_ptr;   /* Current read pointer */
  char* _IO_read_end;   /* End of get area. */
  char* _IO_read_base;  /* Start of putback+get area. */
  char* _IO_write_base; /* Start of put area. */
  char* _IO_write_ptr;  /* Current put pointer. */
  char* _IO_write_end;  /* End of put area. */
  char* _IO_buf_base;   /* Start of reserve area. */
  char* _IO_buf_end;    /* End of reserve area. */
  /* The following fields are used to support backing up and undo. */
  char *_IO_save_base; /* Pointer to start of non-current get area. */
  char *_IO_backup_base;  /* Pointer to first valid character of backup area */
  char *_IO_save_end; /* Pointer to end of non-current get area. */

  struct _IO_marker *_markers;

  struct _IO_FILE *_chain;

  int _fileno; /* File Descriptor */
#if 0
  int _blksize;
#else
  int _flags2;
#endif
...
```

Los file descriptors en un sistema UNIX para los streams predefinidos/est�ndares son los siguientes:

```console
$ less /usr/include/unistd.h 
...
/* Standard file descriptors.  */
#define	STDIN_FILENO	0	/* Standard input.  */
#define	STDOUT_FILENO	1	/* Standard output.  */
#define	STDERR_FILENO	2	/* Standard error output.  */
...
```

A nivel de usuario se puede cambiar los buffers llamando a `setbuf()` o `setvbuf()`, esta �ltima sirve para cambiar el modo. El tama�o default se puede encontrar definido en el sistema por la macro `MAX_INPUT`. Si se llena se suele enviar el char BELL (sonido) a la terminal. En modo canonical el tama�o m�ximo de l�nea se define en `MAX_CANON`. Este �ltimo suele estar en 255B. En GNU/Linux se puede inspeccionar el archivo `/usr/include/linux/limits.h`

## Primeros pasos

Para comenzar a aprender I/O se va a utilizar el modo stream-oriented, canonical I/O, y se va a hacer sobre los 3 streams default que tiene asociado el programa, `stdin`, `stdout` y `stderr`. En particular sobre los dos primeros. Se utilizar�n los operadores de redirecci�n del shell ">", "<", "2>", ">>" , "&>". Por ejemplo para hacer redirecciones de la salida y del error se puede usar:

```console
$ cat /usr/include/unistd.h noexiste &> /tmp/dos

```

### Pre-requisitos

Sistema UNIX compatible (??).
Compilador GCC, make, dd, tr, cat, awk, pstree, less, grep, man ...

## Pruebas
 
Ver el las p�ginas del manual como es el uso de las funciones `putchar()` y `getchar()`. Para esto se requiere tener instalados las man page de desarrollo.

```console
$ man 3 getchar

```

Probar la entrada y salida b�sica de un char/byte. Primero generar 3 archivos uno vac�o otro con el byte 0xFF y otro con el char 'a'.
 
```console
$ touch dummy ### empty

$ dd if=/dev/zero bs=1 count=1 | tr "\000" "\377" > ff.bin ### FF

$ echo -n "a" > a.txt ### 'a'
```
Probar los ejecutables usando como entrada la terminal y los archivos generados. Probar tambi�n enviar la salida a otro archivo.

```console
$ make 

$ ./000-bgetch
$ ./000-bgetch < a.txt
$ ./000-bgetch < dummy
$ ./000-bgetch < ff.bin

$ ./000-bgetch < a.txt > a-copy.txt

$ ./000-bgetch-or-eof < dummy ; echo $?
$ ./000-bgetch-or-eof < ff.bin ; echo $?

$ ./000-bgetchint-or-eof < dummy ; echo $?
$ ./000-bgetchint-or-eof < ff.bin ; echo $?
```

Comparar el valor de EOF con el char -1 usando el programa 

```console
$ ./000-bgetchint-or-eof-print (press EOF keystroke)
FFFFFFFF

$ ./000-bgetchint-or-eof-print < ff.bin 
000000FF
```

Ver el programa que lee hasta EOF y sus diferentes implementaciones contenidas en el archivo fuente `00-untileof_n.c`. Investigar una de las implementaciones incluidas en el mismo de la funci�n no trabaja de forma adecuada y pensar un ejemplo en el cual fallar�a. Pensar en generar un archivo que contenga el valor EOF como dato v�lido.

```console
$ dd if=/dev/zero bs=1 count=10 | tr "\000" "\377" > eof.bin 

$ cat a.txt a.txt a.txt eof.bin a.txt a.txt > b.bin
```

Probar de forma interactiva y con el archivo generado

```console
$ ./00-untileof_n

$ ./00-untileof_n < b.txt 
```

Probar la lectura hasta EOL || EOF con el programa usando lines.txt y line.txt generado de la siguiente forma:

```console
$ cat lines.txt 
habia una vez en...
12345 678 90 
the end

$ head -1 lines.txt | xargs echo -n > line.txt

$ ./00-untileol_n

$ ./00-untileol_n < line.txt

$ ./00-untileol_n < lines.txt 
```

Para lecturas m�ltiples con buffering ver `01-bgetchs.c`  y `02-bgetchs-skipline.c`

Ver el funcionamiento de la lectura de strings, ver los programas `03-getstr.c` y `04-getstr.c`. 

Ver el funcionamiento de I/O non-canonical con los ejemplos de acuerdo a la plataforma. Para GNU/Linux se puede utilizar `linux-unbuffered-getc.c` o `linux-unbuffered-getc-v2.c`.

