/*
 * Programa para leer un char/byte y lo env�a a la salida con un \n si este no es EOF.
 * Encontrar errores.
 */

#include <stdio.h>

int main()
{
  char  c1;

  c1 = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  if (c1 != EOF)
    {
      //printf("%X\n",c1);
      putchar(c1);putchar('\n');
      return 0;
    }
  else
    {
      return 1;
    }
}

/** EOF **/
