#include <stdio.h>

//////////////////////////////////////////////////////
//
// Generates text file with different EOL acording
// -D directive value
//
//////////////////////////////////////////////////////

// gcc -D_DOS -Wall -o eol-fmt eol-fmt.c
// gcc -D_VMS -Wall -o eol-fmt eol-fmt.c
// gcc -D_UNIX -Wall -o eol-fmt eol-fmt.c

/***
\r = CR (Carriage Return)  Used as a new line character in Mac OS before X (System 9)
\n = LF (Line Feed)        Used as a new line character in Unix/Mac OS X
\r\n = CR + LF             Used as a new line character in Windows
\n\r = LF + CR             Used as a new line character in HP VMS 
***/

#define LINE "Hello"

#ifdef _DOS
#define EOL "\r\n"
#elif _VMS
#define EOL "\n\r"
#elif _UNIX
#define EOL "\n"
#else
#define EOL "\n"
#endif

int main()
{
  int res = 1;
  int i;
    for(i=0;((i<3)&&(res>0));i++)
    {
      res = printf("%s%s",LINE,EOL);
    }
  return res;
}

/** EOF **/
