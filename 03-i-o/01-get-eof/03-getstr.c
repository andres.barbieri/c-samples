/*
 * Programa de ejemplo de lecutra de string.
 * Encontrar que errores tiene.
 */
#include <stdio.h>

#define MAXLEN 5

int main()
{
  char str[MAXLEN];
  int  i;  
  fgets(str,MAXLEN,stdin);
  for(i=0;i<MAXLEN;i++)
    {
      printf("[%d]=%c %02X\n",i,str[i],str[i]);
    }
  return (0);
}
