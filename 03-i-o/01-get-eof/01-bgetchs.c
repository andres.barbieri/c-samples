/*
 * Programa que lee dos char/bytes y env'ia la suma a la salida.
 */

#include <stdio.h>

int main()
{
  int c1,c2;
  
  printf("Ingrese un char/byte: ");
  c1 = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  printf("Ingrese otro char/byte: ");
  c2 = getchar();
  if ((c1 != EOF)&&(c2 != EOF)) 
    {
      putchar('\n');
      putchar(c1);putchar('+');putchar(c2);putchar('=');
      putchar(c1+c2);
      //putchar((c1+c2)-'0'); Para digitos
      putchar('\n'); 
      return 0;
    }
  else
    {
      return -1;
    }
} 

/** EOF **/
