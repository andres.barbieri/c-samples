/*
 * Compila en sistemas con ncurses
 * gcc -Wall -o portable_unbuffered-getc portable_unbuffered-getc.c -lncurses
 * 
 * http://pubs.opengroup.org/onlinepubs/7908799/xcurses/curses.h.html
 */
#include <stdio.h>
#include <ncurses.h>

int main() 
{ 
  char ch;

  // The initscr() function determines the terminal type and initialises all implementation 
  // data structures. The environment variable specifies the terminal type. 
  // The initscr() function also causes the first refresh operation to clear the screen. 
  // If errors occur, initscr() writes an appropriate error message to standard error and exits. 
  initscr();
  //If delay is negative, blocking read is used (i.e., waits indefinitely for input).
  timeout(-1); 
  printf("End with a 'q'\n");  
  do { 
    ch=getch(); 
    printf("[%c]",ch); 
  } while(ch != 'q'); 
  // Suspend Curses session
  endwin();
  return 0;
}
