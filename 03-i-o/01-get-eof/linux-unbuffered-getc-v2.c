/*
 * Compila en GNU/Linux
 */

#include <stdio.h>
#include <unistd.h>
#include <termios.h>

int getch() {
        unsigned char buf = 0;
        struct termios old = {0};

        if (tcgetattr(0, &old) < 0)
	  {
	    perror("tcgetattr()");
	    return EOF;
	  }
        old.c_lflag &= ~ICANON;
        old.c_lflag &= ~ECHO;
        old.c_cc[VMIN]  = 1;
        old.c_cc[VTIME] = 0;
        if (tcsetattr(0, TCSANOW, &old) < 0)
	  {
	    perror("tcsetattr ICANON");
	    return EOF;
	  }
        if (read(0, &buf, 1) < 0)
	  {
                perror ("read()");
		return EOF;
	  }
        //printf("buf=%d\n",buf);
        old.c_lflag |= ICANON;
        old.c_lflag |= ECHO;

        if (tcsetattr(0, TCSADRAIN, &old) < 0)
	  {
                perror ("tcsetattr ~ICANON");
	  }
        return (int)(buf);
}

int main()
{
  int c;

  printf("End with a 'q'\n");  
  do {
     c=getch();
     printf("%d ",c);
     fflush(stdout);
  } while(c!='q');
  return 0;
}

/** EOF **/
