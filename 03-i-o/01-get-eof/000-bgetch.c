/*
 * Programa para leer un char/byte y lo env�a a la salida de forma incondicional con un \n. Test con ./000-bgetch | hexdump -C
 * Encontrar errores.
 */

#include <stdio.h>

int main()
{
  char c1;
  
  c1 = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  putchar(c1);
  putchar('\n');
  return 0;
}

/** EOF **/
