/*
 * Programa para leer un entro y lo env�a a la salida en formato Hex
 * Sirve para compara visualmente (int)EOF =(int)(-1) != (char)(-1)
 */

#include <stdio.h>

int main()
{
  int c1;

  c1 = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  printf("%08X\n",c1); // Asume sizeof(int)=4
  if (c1 != EOF)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

/** EOF **/
