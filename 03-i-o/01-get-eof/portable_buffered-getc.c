#include <stdio.h>

int main() 
{ 
  int c; 

  printf("End with a 'q'\n");  
  // Checking getchar result is not carried out
  do {
     c=getchar();
     printf("%d ",c);
  } while(c!='q');

  return 0;
}

