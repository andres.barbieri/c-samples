/*
 * Programa de ejemplo de lecutra de string
 */
#include <stdio.h>

#define MAXLEN 5

int main()
{
  char str[MAXLEN+1]; // space for string + \0
  char *res;
  
  res = fgets(str,MAXLEN+1,stdin); // Reads in at most MAXLEN chars
  if ( res!=NULL ) {
    int i = 0;
    while ((str[i] != 0)&&(i<MAXLEN))
      {
        if (str[i] != '\n')
	  printf("[%d]=%c %02X\n",i,str[i],str[i]);
	else
	  printf("[%d]=\\n %02X\n",i,str[i]);
	i++;
      }
    printf("[%d]=null=\\0=%02X\n",i,str[i]);    
    //printf("[%d]=?=%02X\n",(++i),str[i]);    
    return (0);
  }
  else return -1;
}
