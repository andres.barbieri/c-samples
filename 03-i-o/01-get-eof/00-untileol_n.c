#include <stdio.h> 

// reads until EOL and counts v1.0
int end_of_line()
{
  int i = 0;
  int c = getchar();
  i++;
  while (c != '\n' && c != EOF )
    {
      c = getchar();
      i++;
    }
  return ((c=='\n')?(++i):(i));
}

// reads until EOL  and counts v2.0
int  end_of_line2()
{
  int i = 0;
  int c;
  do {
    c = getchar();
    //printf("read %d\n",c);
    i++;
  } while (c != '\n' && c != EOF);
  return ((c=='\n')?(++i):(i));
}

// reads until EOL and counts v3.0
int end_of_line3()
{
  int i = 0;
  int c = '\0';
  for(;((c!=EOF) && (c!='\n'));c = getchar(),i++);
  return ((c=='\n')?(++i):(i));
}

// main
int main()
{
  //int n = end_of_line();
  //int n = end_of_line2();
  int n = end_of_line3();
  return printf("I have made [%d] readings, getting [%d] chars\n",n,n-1);
}

/** EOF **/
