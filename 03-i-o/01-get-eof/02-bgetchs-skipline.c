/*
 * Programa que lee dos char/bytes y env'ia la suma a la salida.
 * Hace limpieza de buffer.
 */

#include <stdio.h>


void skip_line()
{    
  int c = 0;
  if (!feof(stdin))
    {
      while ((c = getchar()) != '\n' && c != EOF);
    }
}

//
// Make sure to call this function after a succesful read
//
void simple_skip_line()
{
  int c = 0;

  while ((c != EOF)&&(c!='\n')) {
    c = getchar();
  }
}


int main()
{
  int c1,c2;
  
  printf("Ingrese un char/byte: ");
  c1 = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  skip_line();
  //simple_skip_line();
  printf("Ingrese otro char/byte: ");
  c2 = getchar();
  if ((c1 != EOF)&&(c2 != EOF)) 
    {
      putchar(c1);putchar('+');putchar(c2);putchar('=');
      putchar(c1+c2);
      //putchar((c1+c2)-'0'); Para digitos
      putchar('\n'); 
      return 0;
    }
  else
    {
      return -1;
    }
} 

/** EOF **/
