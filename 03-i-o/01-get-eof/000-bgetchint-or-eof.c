/*
 * Programa para leer un char/byte y lo env�a a la salida con un \n si este no es EOF.
 *
 */

#include <stdio.h>

int main()
{
  int c1;
  //char  c1;

  c1 = getchar(); // getchar() == getc(stdin), getc == MACRO, fgetc == func
  if (c1 != EOF)
    {
      putchar(c1);putchar('\n');
      return 0;
    }
  else
    {
      return 1;
    }
}

/** EOF **/
