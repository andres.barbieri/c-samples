/*
 * Algunas funciones para aplicar sobre streams de datos
 * y vaciar buffer si se esta utilizando. Por ejemplo se
 * pueden aplicar sobre stdin:
 *
 * skip_line(stdin)
 *
 * NOTES:
 * Los comentarios estan copiados en el idioma original
 *
 */

#include <stdio.h>

void skip_line(FILE *fp)
{    
  int c;
  if (!feof(fp))
    while ( ((c = getc(fp)) != '\n') && (c != EOF) );
}


void skip_line_or_eof(FILE *fp)
{    
  int c;
  if (!feof(fp))
    do {
      c = getc(fp);
    } while ((c!=EOF)&&(c!='\n'));
}


// fflush is defined only for output streams. 
// Since its definition of ``flush'' is to complete the writing of buffered c
// characters (not to discard them), discarding unread input would not be an  
// analogous meaning for fflush on input streams 
// It doesn't work on all platforms; e.g. it doesn't generally work on Linux. 
void skip_line_WRONG(FILE *fp)
{  
  fflush(fp);   
}

// fseek returns -1
// EBADF  The stream specified, in this case stdin, is not a seekable stream
void skip_line2_WRONG(FILE *fp)
{  
  fseek(fp,0,SEEK_END); 
}

// Problems araise when input buffer is empty
void simple_skip_line_stdin()
{    
  int c;

  do {
    c = getchar();
  } while ((c!=EOF)&&(c!='\n'));
}

