/*
 * Factorial Test program
 *
 */


#include "factorial.h"

int main() 
{
  int r, ok;

  r = factorial(3,&ok);
  print_error(r,ok);

  r = factorial(5,&ok);
  print_error(r,ok);

  r = factorial(-3,&ok);  
  print_error(r,ok);

  r = factorial(50,&ok);  
  print_error(r,ok);

  return 0;
}

/*** EOF ***/
