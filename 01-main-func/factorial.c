#include <stdio.h>
#include "fact.h"
#include "factorial.h"

void print_error(int r, int result_ok)
{
  switch (result_ok)
    {
    case FACT_ERR:
      {
	printf("ERROR\n");
	break;
      }
    case FACT_OK:
      {
	printf("OK %d\n",r);
	break;
      }
    default:
      {
	printf("UNDEFINED\n");
      }
    }
}
  
int factorial(int n, int /*@out@*/ *result_ok)
{
  if ((n>FACT_MAX_NUM)||(n<0))
    {
      *result_ok = FACT_ERR;
      return 0;
    }
  else
    {
      *result_ok = FACT_OK;
      return fact(n);
    }
}

    
