/** Factorial v1 **/

int fact(int n)
{
  int f = 1;
  for(int i=n;i>0;i--) {
    f*=i;
  }
  return f;
}

