# Aprendiendo compilaci�n por pasos de un programa "C"

Ejemplo de programa "C" que muestra compilaci�n por pasos y dependencias. En este caso se tienen 3 (tres) implementaciones posibles de la funci�n factorial en `f1.c`, `f2.c` y `f3.c`. Sirven para ilustrar como se puede cambiar de una a otra.

## Primeros pasos

Comparar la diferencia con `bigmain.c` y `main.c`. Funcionan los dos
Cu�l es la forma correcta de armar el c�digo? 

### Pre-requisitos

Compilador GCC, `make`, `sleep`, `file`, `objdump`, `cat`, `ldd` ...
B�sicamente un entorno de desarrollo adem�s de la herramienta `dot` para generar gr�ficos y la herramienta de chequeos est�ticos `splint`.

## Pruebas
 
Probar ejecutando los pasos en "sumatoria", es decir primero el pre-procesado, luego el pre-procesado + ensamblado, luego la generaci�n de los c�digos objetos (incluye los pasos anteriores), por �ltimo el ejecutable. Estos pasos no son �tiles nada m�s que para aprender como se puede generar el c�digo y en la pr�ctica no se utilizan as�. Los nombres de los target son completamente arbitrarios.

```
 make clean
 make step1
 make step12
 make step123
 make link
 ldd main
 ...
 ./main; echo $?
```

Los pasos �tiles se pueden ver con la siguiente ejecuci�n, donde se genera el c�digo objeto y finalmente se linkediciona (pasos t�picos):

```
 make clean
 make 
```

### Cambios para cambiar implementaci�n

Ver de usar la implementaci�n de `f2.c` o `f3.c` en lugar de `f1.c`. Qu� cambios se deber�an hacer ? Se debe recompilar todo el c�digo ?

## Otras cuestiones �tiles

El ejemplo tambi�n se acompa�a de el chequeo de dependencias de inclusiones con el flag `-MM`y el gr�fico `graph.png` que muestra como son generado a partir de la herramienta `dot`. Para el chequeo de dependencias y chequeo est�tico se puede ejecutar:

```
 make showdeps
 make check
```

Para ejecutar tambi�n se puede correr:

```
 make test
```

