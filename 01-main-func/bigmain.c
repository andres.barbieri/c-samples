/*
 * Factorial Test program
 *
 * Solo como referencia. No ser�a la forma correcta
 * de implementarlo.
 */


// Comment 

#include "f1.c"
#include "factorial.c"

int main() 
{
  int r, ok;

  r = factorial(3,&ok);
  print_error(r,ok);

  r = factorial(5,&ok);
  print_error(r,ok);

  r = factorial(-3,&ok);  
  print_error(r,ok);

  r = factorial(50,&ok);  
  print_error(r,ok);

  return 0;
}

/*** EOF ***/
