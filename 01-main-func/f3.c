/*** Factorial v3 ***/

int fact(int n)
{
  int f = 1;
  int i = n;
  while (i>0)
    {
      f=f*i;
      i--;
    }
  return f;
}

