#define FACT_MAX_NUM 24
#define FACT_ERR     -1
#define FACT_OK       0

void print_error(int r, int result_ok);

int factorial(int n, int  /*@out@*/ *result_ok);

