/*
 * Programa que hace Input de un archivo default o
 * pasado como par'ametro.
 * Probar con diferentes archivos como entrada.
 * Indicar en que instancia del programa termina.
 * 
 * ./2-file-reader 
 * ./2-file-reader  data.txt 
 * ./2-file-reader  dummy
 * ./2-file-reader  /dev/zero
 * ./2-file-reader  /dev/null
 * ./2-file-reader  /dev/urandom
 * ./2-file-reader  /dev/random
 * ./2-file-reader  /etc/issue
 * ./2-file-reader  /etc
 * ./2-file-reader  /etc/no-Existe
 * sudo ./2-file-reader /dev/input/mouse0
 *
 */ 

#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME "data.txt"
#define FMODE "r"

int main(int argc, char *argv[])
{
  FILE *fp;
  char buf;
  int  r;
  char *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  //fp = fopen(fname,FMODE); if (fp == NULL)
  //fp = fopen(fname,FMODE); if (!fp)
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for reading");
     exit(1);  
   }

  while ((r=fread(&buf,sizeof(char),1,fp))) //>0 || 0
    {
      if ((buf>31)&&(buf<127)) {
	fprintf(stdout,"r=%d(%c)",r, buf);
      } else {
	fprintf(stdout,"[%d]",buf);
      }
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  return fclose(fp);
}
