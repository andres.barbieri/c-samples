/* 
 * Programa que hace Output a un archivo default
 * o pasado como par'ametro escribiendo de a bloques
 */

#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME "data.txt"
#define FMODE "w"
//#define FMODE "a"
#define FBSIZE  8

int main(int argc, char *argv[])
{
  FILE *fp;
  char buf[FBSIZE];
  char *fname = FNAME;
  int r,i,j;

  if (argc >= 2)
    {
      fname = argv[1];
    } 
  
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for writing");
     exit(1);  
   }
  for(i=0;i<256;i+=FBSIZE)
    {
      for(j=0;j<FBSIZE;j++) buf[j] = i+j;
      if (!(r = fwrite(buf,sizeof(char),FBSIZE,fp))) // >0 || 0
	{
	   perror("Writing file");
	   exit(1);
	}
      else 
	{
	  printf("r=%d ",r);
	}
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  return fclose(fp); 
}
