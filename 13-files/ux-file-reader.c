/*
 * Programa que lee un archivo
 * usando la API de Unix
 */

#include <sys/types.h> // open
#include <sys/stat.h>  // open
#include <fcntl.h>     // open
#include <unistd.h>    // read, close
#include <stdio.h>     // printf
#include <stdlib.h>    // exit

#define FNAME "data.txt"

int main(int argc, char *argv[])
{
  int fd;
  char buf;
  int  r;
  char *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  fd = open(fname, O_RDONLY );
  if (fd == -1)
    {
      perror("Can't open file for reading");
      exit(1);
    }


  while ((r=read(fd, &buf, sizeof(char)* 1))) //>0 || 0
    {
      if ((buf>31)&&(buf<127)) {
	fprintf(stdout,"r=%d(%c)",r, buf);
      } else {
	fprintf(stdout,"[%d]",buf);
      }
    }
  return close(fd);
}
