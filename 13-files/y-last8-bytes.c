#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME "data.txt"
#define FMODE "r"
#define FBSIZE 8

int main(int argc, char *argv[])
{
  FILE *fp;
  char buf[FBSIZE];
  int  fsize = 0;
  int  r;
  char *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  //fp = fopen(fname,FMODE); if (fp == NULL)
  //fp = fopen(fname,FMODE); if (!fp)
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for reading");
     exit(1);  
   }

   /* find out the size of the file and reset pointer to beginning of file */
   fseek(fp, 0L, SEEK_END);
   fsize = ftell(fp);
   fseek(fp, 0L, SEEK_SET);

   /* make sure the file has enough bytes */
   if (fsize < FBSIZE) 
     {
       fprintf(stderr,"File too short\n");
       fclose(fp);
       exit(1);
   } else {
     /* read last FBSIZE bytes of a file */
     fseek(fp, -FBSIZE, SEEK_END);
     r = (int) fread(&buf, sizeof(char), FBSIZE, fp);
     if (r == FBSIZE)
       {
	 printf("Read %d bytes from %s, expected %d\n", r, fname, 1);
	 for(r=0;r<FBSIZE;r++) printf("[%c]",buf[r]);
	 printf("\n");
       }
   }
   return fclose(fp);
}
