/*
 * Programa que lee datos y espera
 * que esten en formato double
 * Correr primero x-doubles-writer si no existe data.bin
 */

#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME "data.bin"
#define FMODE "r"

int main(int argc, char *argv[])
{
  FILE  *fp;
  double buf;
  int    r;
  char   *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for reading");
     exit(1);  
   }

  while ((r=fread(&buf,sizeof(double),1,fp))) //>0 || 0
    {
	fprintf(stdout,"[%.2f]",buf);
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  return fclose(fp);
}
