#include <stdio.h>
#include <stdlib.h>

#define FNAME "data.txt"
#define FMODE "r"

int main(int argc, char *argv[])
{
  FILE *fp;
  char buf;
  int  fsize = 0;
  int  r;
  char *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  //fp = fopen(fname,FMODE); if (fp == NULL)
  //fp = fopen(fname,FMODE); if (!fp)
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for reading");
     exit(1);  
   }

   /* find out the size of the file and reset pointer to beginning of file */
   fseek(fp, 0L, SEEK_END);
   fsize = ftell(fp);
   fseek(fp, 0L, SEEK_SET);

   /* make sure the file is not empty */
   if (fsize == 0) 
     {
       fprintf(stderr,"Empty file\n");
       fclose(fp);
       exit(1);
   } else {
     /* read last byte of file */
     fseek(fp, -1, SEEK_END);
     r = (int) fread(&buf, sizeof(char), 1, fp);
     printf("Read %d bytes from %s, expected %d, readed=%d\n", r, fname, 1, buf);
   }
   return fclose(fp);
}
