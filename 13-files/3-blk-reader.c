/*
 * Programa que hace Input de un archivo default o
 * pasado como par'ametro leyendo de a bloques de FBSIZE
 * Probar con diferentes archivos como entrada.
 * Indicar en que instancia del programa termina.
 * 
 * ./3-blk-reader 
 * ./3-blk-reader  data.txt 
 * ./3-blk-reader  dummy
 * ./3-blk-reader  /dev/zero
 * ./3-blk-reader  /dev/null
 * ./3-blk-reader  /dev/urandom
 * ./3-blk-reader  /dev/random
 * ./3-blk-reader  /etc/issue
 * ./3-blk-reader  /etc
 * ./3-blk-reader  /etc/no-Existe
 * ...
 */ 

#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME  "data.txt"
#define FMODE  "r"
#define FBSIZE  8

int main(int argc, char *argv[])
{
  FILE *fp;
  char buf[FBSIZE];
  int  r;
  char *fname = FNAME;
  int  i;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  //fp = fopen(fname,FMODE); if (fp == NULL)
  //fp = fopen(fname,FMODE); if (!fp)
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for reading");
     exit(1);  
   }
  while ((r=fread(buf,sizeof(char),FBSIZE,fp))) //>0 || 0
    //while ((r=fread(buf,sizeof(buf),1,fp))) //>0 || 0
    {
      fprintf(stdout,"r=%d",r);
      for(i=0;i<r;i++)
	//for(i=0;i<r*FBSIZE;i++)
	{
	  if ((buf[i]>31)&&(buf[i]<127)) {
	    fprintf(stdout,"(%c)",buf[i]);
	  } else {
	    fprintf(stdout,"[%d]",buf[i]);
	  }
	}
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  return fclose(fp);
}
