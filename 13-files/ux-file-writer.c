/*
 * Programa que escribe un archivo
 * usando la API de Unix
 */

#include <sys/types.h> // open
#include <sys/stat.h>  // open
#include <fcntl.h>     // open
#include <unistd.h>    // write, close
#include <stdio.h>     // printf
#include <stdlib.h>    // exit

#define FNAME "data.txt"

int main(int argc, char *argv[])
{
  int fd;
  int buf;
  char *fname = FNAME;
  int r;

  if (argc >= 2)
    {
      fname = argv[1];
    } 
  
 // fd = open(FNAME,O_CREAT | O_APPEND);
  fd = open(fname, O_RDWR | O_CREAT);
  if (fd == -1)
    {
      perror("Can't open file for writing");
      exit(1);
    }

  for(buf=0;buf<256;buf++)
    {
      if (!(r = write(fd, (char*)&buf,sizeof(char)*1))) // >0 || 0
	{
	   perror("Writing file");
	   exit(1);
	}
    }
  return close(fd); 
}
