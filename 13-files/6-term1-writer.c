/*
 * Programa que hace Output a la salida est'andar
 *
 * Probar con:
 *
 * ./6-term1-writer | hexdump -c 
 * ./6-term1-writer > data-w.txt 
 * ./6-term1-writer > /dev/zero
 * ./6-term1-writer > /dev/null
 * ./6-term1-writer > /dev/full
 * ./6-term1-writer > /etc/issue
 * ./6-term1-writer > /tmp/data.txt
 *
 * Ver lo generado con:
 * hexdump -c data-w.txt 
 * 0000000  \0 001 002 003 004 005 006  \a  \b  \t  \n  \v  \f  \r 016 017
 * 0000010 020 021 022 023 024 025 026 027 030 031 032 033 034 035 036 037
 * 0000020       !   "   #   $   %   &   '   (   )   *   +   ,   -   .   /
 * 0000030   0   1   2   3   4   5   6   7   8   9   :   ;   <   =   >   ?
 * 0000040   @   A   B   C   D   E   F   G   H   I   J   K   L   M   N   O
 * 0000050   P   Q   R   S   T   U   V   W   X   Y   Z   [   \   ]   ^   _
 * 0000060   `   a   b   c   d   e   f   g   h   i   j   k   l   m   n   o
 * 0000070   p   q   r   s   t   u   v   w   x   y   z   {   |   }   ~ 177
 * ...
 *
 * Probar enviar la salida a otra terminal. Abrir una nueva,
 * averiguar la tty con:
 *
 * who am i | cut -d' ' -f4
 * pts/13
 *
 * ./6-term1-writer > /dev/pts/13
 *
 */

#include <stdio.h>
#include <stdlib.h> // exit

int main(int argc, char *argv[])
{
  FILE *fp = stdout;
  int buf;
  
  //fp = fopen(fp,"r"); implicit

  for(buf=0;buf<256;buf++)
    {
      // putchar(buf)
      if (!(fwrite((char*)&buf,sizeof(char),1,fp))) // >0 || 0
	{
	   perror("Writing file");
	   exit(1);
	}
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }

  // fclose(fp); implicit
  return 0;
}
