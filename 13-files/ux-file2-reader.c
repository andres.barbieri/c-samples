/*
 * Programa que lee un archivo
 * usando la API de Unix
 */

#include <sys/types.h> // open
#include <sys/stat.h>  // open
#include <fcntl.h>     // open
#include <unistd.h>    // read, close
#include <stdio.h>     // printf
#include <stdlib.h>    // exit

/*** For fileno ***/
#define _XOPEN_SOURCE 600

#define FNAME "data.txt"
#define FMODE "r"

int main(int argc, char *argv[])
{
  FILE *fp;
  int fd;
  char buf;
  int  r;
  char *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  //fp = fopen(fname,FMODE); if (fp == NULL)
  //fp = fopen(fname,FMODE); if (!fp)
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for reading");
     exit(1);  
   }
  fd = fileno(fp);

  while ((r=read(fd, &buf, sizeof(char)* 1))) //>0 || 0
    {
      if ((buf>31)&&(buf<127)) {
	fprintf(stdout,"r=%d(%c)",r, buf);
      } else {
	fprintf(stdout,"[%d]",buf);
      }
    }
  return close(fd);
}
