/*
 * Ver comentario 0-term0-reader.c
 *
 * Ver que sucede con el intercalado de stdout y stderr
 *
 * Comparar:
 *
 * ./1-term1-reader < /dev/random
 *
 * ./1-term2-reader < /dev/random
 *
 *
 */

#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE *fp = stdin; // stdio.h
  char buf;
  int  r;
  
  //fp = fopen(fp,"r"); implicit

  //while ((r=fread(&buf,sizeof(char),1,fp))>0)
  // warning: suggest parentheses around assignment used as truth value 

  // (r=fread(&buf,sizeof(char),1,fp)) !=  (r==fread(&buf,sizeof(char),1,fp))

  // Cambiamos getchar por fread()
  while ( (r=fread(&buf,sizeof(char),1,fp)) ) // >0 || 0
    {
     if ((buf>31)&&(buf<127)) {
       fprintf(stdout,"r=%d(%c)",r, buf);
     } else {
       fprintf(stdout,"[%d]",buf);
     }
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  // fclose(fp); implicit
  return 0;
}
