#include <unistd.h>   // read
#include <stdio.h>    //  printf

int main(int argc, char *argv[])
{
  int fd = 0;
  char buf;
  int  r;
  
  //fd = open(...); implicit
  while ( (r=read(fd, &buf,sizeof(char)*1))) // >0 || 0
    {
     if ((buf>31)&&(buf<127)) {
       fprintf(stdout,"r=%d(%c)",r, buf);
     } else {
       fprintf(stdout,"[%d]",buf);
     }
    }
  //close(fd); implicit
  return 0;
}
