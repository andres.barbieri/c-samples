/* 
 * Programa que hace Output a un archivo default
 * o pasado como par'ametro
 *
 * Probar con los siguientes par'ametros  
 *
 * data-w.txt 
 * /dev/zero
 * /dev/null
 * /dev/full
 * /etc/issue
 * /tmp/data.txt
 *
 * Por ejemplo probar:
 * ./7-file-writer /dev/full; echo $?
 * ./7-file-writer /dev/null; echo $?
 * ./7-file-writer /dev/zero; echo $?
 *
 * Cambiar de modo "w" a modo "a" y volver a probar
 *
 */
#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME "data.txt"
#define FMODE "w"
//#define FMODE "a"

int main(int argc, char *argv[])
{
  FILE *fp;
  int buf;
  char *fname = FNAME;
  int r;

  if (argc >= 2)
    {
      fname = argv[1];
    } 
  
  //fp = fopen(fname,FMODE); if (fp == NULL)
  //fp = fopen(fname,FMODE); if (!fp)
  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for writing");
     exit(1);  
   }
  for(buf=0;buf<256;buf++)
    {
      if (!(r = fwrite((char*)&buf,sizeof(char),1,fp))) // >0 || 0
	{
	   perror("Writing file");
	   exit(1);
	}
      //fflush(fp); // In order to get Full device 
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  return fclose(fp); 
}
