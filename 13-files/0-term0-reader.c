/*
 * Programa que hace Input de la entrada est'andar.
 * Probar con redirecciones de archivos como entrada.
 * Indicar en que instancia del programa termina.
 * Ver el efecto de intercalado de stdout con stderr
 * que no se secuencian de acuerdo al programa.
 * Por qu'e sucede?
 * 
 * ./0-term0-reader < data.txt 
 * ./0-term0-reader < dummy
 * ./0-term0-reader < /dev/zero
 * ./0-term0-reader < /dev/null
 * ./0-term0-reader < /dev/urandom
 * ./0-term0-reader < /dev/random
 * ./0-term0-reader < /etc/issue
 * ./0-term0-reader < /etc
 * ./0-term0-reader < /etc/no-Existe
 *
 *
 */ 

#include <stdio.h>

int main(int argc, char *argv[])
{
  FILE *fp = stdin; // stdin 
  //char buf;
  int  r;
  
  //fp = fopen(fp,"r"); implicit
  while ((r = getchar()) != EOF)
    {
      if ((r>31)&&(r<127)) {
	fprintf(stdout,"r=1(%c)",r);
      } else {
	fprintf(stdout,"[%d]",r);
      }
    }
  /*** Control Extra ***/
  if (feof(fp)) 
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  // fclose(fp); implicit
  return 0;
}
