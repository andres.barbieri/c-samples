/*
 * Programa que hace Input de la entrada est'andar.
 * Probar con redirecciones de archivos como entrada 
 * 
 * ./00-term0-reader < data.txt 
 * ./00-term0-reader < dummy
 * ./00-term0-reader < /dev/zero
 * ./00-term0-reader < /dev/null
 * ./00-term0-reader < /dev/urandom
 * ./00-term0-reader < /dev/random
 * ./00-term0-reader < /etc/issue
 *
 */ 
 
#include <stdio.h> // stdin getchar perror

int main(int argc, char *argv[])
{
  //FILE *fp = stdin; // stdio
  //char buf;
  int  r;
  
  //fp = fopen(fp,"r"); implicit

  //while ((r = fgetc(fp)) != EOF)
  while ((r = getchar()) != EOF)
    {
      if ((r>31)&&(r<127)) {
	fprintf(stdout,"r=1(%c)",r);
      } else {
	fprintf(stdout,"[%d]",r);
      }
    }

  // fclose(fp); implicit
  return 0;
}
