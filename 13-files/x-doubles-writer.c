/* 
 * Programa que escribe varios doubles en un archivo
 */

#include <stdio.h>
#include <stdlib.h> // exit

#define FNAME "data.bin"
//#define FMODE "a"     //Open for appending (writing at end of file) pos=$
#define FMODE "w"       //Open for writing, truncate

int main(int argc, char *argv[])
{
  FILE  *fp;
  double buf;
  char   *fname = FNAME;

  if (argc >= 2)
    {
      fname = argv[1];
    } 

  if (! (fp = fopen(fname,FMODE)) )
   {
     perror("Can't open file for writing");
     exit(1);  
   }

  for(buf=0.0;buf<2000.5;buf+=0.5)
    {
      printf("%d %f\n",(int) fwrite(&buf,sizeof(double),1,fp),buf); //>0 || 0
    }
  /*** Control Extra ***/
  if (feof(fp))
    {
      perror("End of file");
    }
  else
    {
      fprintf(stderr,"ferror=%d\n",ferror(fp));
      perror("Another error");
    }
  fclose(fp);
  return 0;
}
