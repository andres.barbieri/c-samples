#include <stdio.h>

int main(int argc, char *argv[])
{
  int a    = 10;
  int b    = 20;
  int flag = 0;
  char h[5] = "hola";
  char c[5] = "chau";
  
  for(int i=0;i<argc;i++)
    {
      printf("argv[%d]=%s\n",i,argv[i]);
    }
  while(a>5)
    {
      a++;
      b--;
      if (b==17) flag=1;
    }
  a=5; 
  c[a] = 'j'; c[a++] = 'y'; 
  //h[a] = 'j'; h[a++] = 'y'; 
  return 0;
}

/***
? gcc 0todebug.c 
? ./a.out 
argv[0]=./a.out
*** stack smashing detected ***: <unknown> terminated
Aborted (core dumped)

? ulimit -c unlimited
? ./a.out 
argv[0]=./a.out
*** stack smashing detected ***: <unknown> terminated
Aborted (core dumped)

? ls core
core

gcc -fno-stack-protector 0todebug.c 
./a.out 
argv[0]=./a.out
***/


