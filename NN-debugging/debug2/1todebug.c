#include <stdio.h>

int main(int argc, char *argv[])
{
  int a    = 10;
  int b    = 20;
  int flag = 0;
  char h[5] = "hola";
  char c[5] = "chau";
  
  for(int i=0;i<argc;i++)
    {
      printf("argv[%d]=%s\n",i,argv[i]);
    }
  while(a>5)
    {
      a++;
      b--;
      if (b==17) flag=1;
    }
  a=5; 
  //c[a] = 'j'; c[a++] = 'y'; 
  h[a] = 'j'; h[a++] = 'y';
  return 0;
}

/***
gcc 1todebug.c -g
gdb ./a.out

list
break 9
break 17
break 24
run uno dos
print argv[0]
print argv[1]
print a
print b
print h
print c
info break

next / step
print c

cont
print a
next
print a

info break
delete break 2
info break

watch flag
print flag
cont
print a 
print b 
print flag

cont
print c <---- Sobre-escrito
print h
cont 

***/
