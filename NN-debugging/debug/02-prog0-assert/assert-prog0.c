#ifdef DEBUG
# include <assert.h>
// #define NDEBUG        // Disable asserts
#endif

// gcc -Wall -pedantic-errors -DDEBUG -std=c99 -o assert-prog0 assert-prog0.c

int main()
{
  char  str[]  = "HOLA";
  char  str2[] = "XABC"; 
  int   a  = 10;
  float b = 20.0;
  int   c = 400;
  int   z = 70, ix;
  float tmp = 10.0;

  for(int i=0;i<(a+10);i++)
    {
      a--;
      b=b*2.0;
      b=b+(float)str[1];
      if (b>1200) 
	{
	  ix = 1;
	}
      else
	{
	  ix = 0;
	}
#ifdef DEBUG
      /***************************************************/
      assert(b-a);
      assert(str2[ix]);
      assert((str2[ix]-(z+(-a))));
      assert((str2[ix]-(z+(-a)+1)));
      /***************************************************/
#endif
      tmp = tmp * c / str2[ix];
      tmp = a*b/(b-a);
      c = c / (str2[ix]-(z+(-a)));
      tmp =  ((float)a / (str2[ix]-(z+(-a)-1)));
    }
  return 0;
}

