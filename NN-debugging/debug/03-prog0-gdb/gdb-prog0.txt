Generar cores:

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 62849
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 62849
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited
andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit -c unlimited 

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit -a
core file size          (blocks, -c) unlimited
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 62849
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 62849
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gcc -g -Wall -Werror -std=c99 -pedantic-errors prog0.c -o prog0

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 62849
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 62849
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit -c unlimited 

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit   
unlimited

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ulimit -a
core file size          (blocks, -c) unlimited
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 62849
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 62849
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ./prog0 
Floating point exception (core dumped)

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ls
core  gdb-prog0.txt  gdb-prog0.txt~  prog0  #prog0.c#  prog0.c

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gdb core     
GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
"/home/andres/source/c-samples/debugging/debug/03-prog0-gdb/core": not in executable format: File format not recognized
(gdb) quit

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gdb prog0 core  
GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
Reading symbols from /home/andres/source/c-samples/debugging/debug/03-prog0-gdb/prog0...done.
[New LWP 4324]

warning: Can't read pathname for load map: Input/output error.
Core was generated by `./prog0'.
Program terminated with signal 8, Arithmetic exception.
#0  0x0000000000400642 in main () at prog0.c:28
28	      c = c / (str2[ix]-(z+(-a)));
(gdb) quit

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gcc -Wall -Werror -std=c99 -pedantic-errors prog0.c -o prog0

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ ./prog0 
Floating point exception (core dumped)
andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gdb core 
GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
"/home/andres/source/c-samples/debugging/debug/03-prog0-gdb/core": not in executable format: File format not recognized
(gdb) quit

andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gdb prog0 core GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
Reading symbols from /home/andres/source/c-samples/debugging/debug/03-prog0-gdb/prog0...(no debugging symbols found)...done.
[New LWP 4348]

warning: Can't read pathname for load map: Input/output error.
Core was generated by `./prog0'.
Program terminated with signal 8, Arithmetic exception.
#0  0x0000000000400642 in main ()






Compile:

gcc -Wall -Werror -ansi -pedantic-errors prog0.c -o prog0
gcc -Wall -Werror -std=c99 -pedantic-errors prog0.c -o prog0

Cambiar por:

gcc -g -Wall -Werror -ansi -pedantic-errors prog0.c -o prog0
gcc -g -Wall -Werror -std=c99 -pedantic-errors prog0.c -o prog0

-Werror Can be supressed 


Add debugging info like source code

0) Start:

gdb prog0
(gdb)

Or you can load it typing

(gdb) file prog0
(gdb) quit

Listing Code:

gdb prog0
GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
Reading symbols from /home/andres/source/c-samples/debugging/debug/03-prog0-gdb/prog0...(no debugging symbols found)...done.
(gdb) 
(gdb) list
No symbol table is loaded.  Use the "file" command.
(gdb) quit

No -g

 gcc -g -Wall -Werror -std=c99 -pedantic-errors prog0.c -o prog0

 gcc -g -Wall -Werror -std=c99 -pedantic-errors prog0.c -o prog0
andres@chatnoir:~/source/c-samples/debugging/debug/03-prog0-gdb$ gdb prog0     
GNU gdb (Ubuntu/Linaro 7.4-2012.04-0ubuntu2.1) 7.4-2012.04
Copyright (C) 2012 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.  Type "show copying"
and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
For bug reporting instructions, please see:
<http://bugs.launchpad.net/gdb-linaro/>...
Reading symbols from /home/andres/source/c-samples/debugging/debug/03-prog0-gdb/prog0...done.
(gdb) 



(gdb) list


(gdb) list

(gdb) help list
List specified function or line.
With no argument, lists ten more lines after or around previous listing.
"list -" lists the ten lines before a previous ten-line listing.
One argument specifies a line, and ten lines are listed around that line.
Two arguments with comma between specify starting and ending lines to list.
Lines can be specified in these ways:
  LINENUM, to list around that line in current file,
  FILE:LINENUM, to list around that line in that file,
  FUNCTION, to list around beginning of that function,
  FILE:FUNCTION, to distinguish among like-named static functions.
  *ADDRESS, to list around the line containing that address.
With two args if one is empty it stands for ten lines away from the other arg.

(gdb) list
1	// gcc -Wall -pedantic-errors -DDEBUG -std=c99 -o prog0 prog0.c
2	
3	int main()
4	{
5	  char  str[]  = "HOLA";
6	  char  str2[] = "XABC"; 
7	  int   a  = 10;
8	  float b = 20.0;
9	  int   c = 400;
10	  int   z = 70, ix;
(gdb) list
11	  float tmp = 10.0;
12	
13	  for(int i=0;i<(a+10);i++)
14	    {
15	      a--;
16	      b=b*2.0;
17	      b=b+(float)str[1];
18	      if (b>1200) 
19		{
20		  ix = 1;
(gdb) list
21		}
22	      else
23		{
24		  ix = 0;
25		}
26	      tmp = tmp * c / str2[ix];
27	      tmp = a*b/(b-a);
28	      c = c / (str2[ix]-(z+(-a)));
29	      tmp =  ((float)a / (str2[ix]-(z+(-a)-1)));
30	    }
(gdb) list
31	  return 0;
32	}
(gdb) list 1,17
1	// gcc -Wall -pedantic-errors -DDEBUG -std=c99 -o prog0 prog0.c
2	
3	int main()
4	{
5	  char  str[]  = "HOLA";
6	  char  str2[] = "XABC"; 
7	  int   a  = 10;
8	  float b = 20.0;
9	  int   c = 400;
10	  int   z = 70, ix;
11	  float tmp = 10.0;
12	
13	  for(int i=0;i<(a+10);i++)
14	    {
15	      a--;
16	      b=b*2.0;
17	      b=b+(float)str[1];
(gdb) list 5
1	// gcc -Wall -pedantic-errors -DDEBUG -std=c99 -o prog0 prog0.c
2	
3	int main()
4	{
5	  char  str[]  = "HOLA";
6	  char  str2[] = "XABC"; 
7	  int   a  = 10;
8	  float b = 20.0;
9	  int   c = 400;
10	  int   z = 70, ix;
(gdb) list 10
5	  char  str[]  = "HOLA";
6	  char  str2[] = "XABC"; 
7	  int   a  = 10;
8	  float b = 20.0;
9	  int   c = 400;
10	  int   z = 70, ix;
11	  float tmp = 10.0;
12	
13	  for(int i=0;i<(a+10);i++)
14	    {
(gdb) 

Running program:

(gdb) run
Starting program: /home/andres/source/c-samples/debugging/debug/03-prog0-gdb/prog0 

Program received signal SIGFPE, Arithmetic exception.
0x0000000000400642 in main () at prog0.c:28
28	      c = c / (str2[ix]-(z+(-a)));

(gdb) quit
A debugging session is active.

	Inferior 1 [process 4374] will be killed.

Quit anyway? (y or n) y 


 kill -l
 1) SIGHUP	 2) SIGINT	 3) SIGQUIT	 4) SIGILL	 5) SIGTRAP
 6) SIGABRT	 7) SIGBUS	 8) SIGFPE	 9) SIGKILL	10) SIGUSR1
11) SIGSEGV	12) SIGUSR2	13) SIGPIPE	14) SIGALRM	15) SIGTERM
16) SIGSTKFLT	17) SIGCHLD	18) SIGCONT	19) SIGSTOP	20) SIGTSTP
21) SIGTTIN	22) SIGTTOU	23) SIGURG	24) SIGXCPU	25) SIGXFSZ
26) SIGVTALRM	27) SIGPROF	28) SIGWINCH	29) SIGIO	30) SIGPWR
31) SIGSYS	34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
63) SIGRTMAX-1	64) SIGRTMAX	


