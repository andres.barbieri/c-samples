#include <stdlib.h>

void f(void)
{
   int* x = malloc(10 * sizeof(int));
   x[9] =  0;
   x[10] = 0;  
   x[11] = 0; 
}             

int main(void)
{
   f();
   return 0;
}
