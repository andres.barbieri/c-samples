// Finding Memory Leaks With Valgrind
//Finding Invalid Pointer Use With Valgrind
//
//  gcc -g -O0 -Wall 2ptr0.c -o 2ptr0
// valgrind --leak-check=full ./2ptr0


// ==9863== Invalid write of size 4
// ==9863==  Address 0x51fc068 is 0 bytes after a block of size 40 alloc'd
// ==9863== 40 bytes in 1 blocks are definitely lost in loss record 1 of 1
// ==9892== ERROR SUMMARY: 2 errors from 2 contexts (suppressed: 0 from 0)

#include <stdlib.h>

void f(void)
{
   int* x = malloc(10 * sizeof(int));
   x[9] =  0;
   x[10] = 0;        // problem 1: heap block overrun
   x[12] = 0;
}                    // problem 2: memory leak -- x not freed

int main(void)
{
   f();
   return 0;
}
