#include <stdio.h>

#define MAX  8

void show_arr(unsigned char v[])
{
    for(int i=0;i<MAX;i++)
      {
	printf("(%d)",v[i]);
      }
    printf("\n");
}
int main()
{
   unsigned char a[MAX];
   unsigned char b[MAX];
   unsigned char c[MAX];
   
   for(int i=0;i<MAX;i++)
     {
       a[i]=0xa;
       b[i]=0xb;
       c[i]=0xc;
     }

   c[11] = 0;
   b[11] = 0;
   a[11] = 0;

   show_arr(a);
   show_arr(b);
   show_arr(c);
  
   return 0;
}                    

