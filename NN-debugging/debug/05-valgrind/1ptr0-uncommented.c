#include <stdlib.h>
#include <stdio.h>

int f(int n)
{
  int* x; 
  if (n >= 0)
    {
      x = malloc(n * sizeof(int));
      fprintf(stderr,"alloc\n");
      x[n-1] = 0;
      return f(n-1);
    }
  else
    return 0;
}                    

int main(void)
{
   f(50);
   return 0;
}


