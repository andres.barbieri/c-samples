void f(void)
{
   int x[10];


   x[10] = 0;        // problem 1: stack block overrun
   x[11] = 0;        // problem 1: stack block overrun
   x[12] = 0;        // problem 1: stack block overrun
   //x[200000] = 0;
}                    

int main(void)
{
   f();
   return 0;
}
