#include <stdio.h>

void f(void)
{
   unsigned char b;
   unsigned char x[10];
   unsigned char a;
   
   a = 10;
   b = 20; 
   x[9] =  0;
   x[10] = 0;        // problem 1: stack block overrun
   x[11] = 0;        // problem 1: stack block overrun
   x[12] = 0;        // problem 1: stack block overrun
   printf("%d %d %d %d\n",x[9],x[10],a,b);
}                    

int main(void)
{
   f();
   return 0;
}
