// Finding Memory Leaks With Valgrind
//
//  gcc -g -O0 -Wall 0ptr0.c -o 0ptr0
// valgrind --leak-check=full ./0ptr0
// ==5027== 40 bytes in 1 blocks are definitely lost in loss record 1 of 1
// ==5027==   total heap usage: 1 allocs, 0 frees, 40 bytes allocated
// ==5027== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)

#include <stdlib.h>

void f(void)
{
   int* x = malloc(10 * sizeof(int));
   x[9] = 0;
}                    // problem 1: memory leak -- x not freed

int main(void)
{
   f();
   return 0;
}
// Usually freed automatically when the program ends, at program's finishing
