#include "A.h"

int main()
{
  A_t a = UNO;

  show_A(a);
  a = DOS;
  show_A(a);

  return 0;
}
