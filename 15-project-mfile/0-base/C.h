// C

#include "A.h"

#define MAX 10

typedef struct C { 
  A_t c[MAX];
} C_t;

void init_C(C_t *elem);
void set_C(C_t *elem, A_t x, int ix);  
void show_C(const C_t elem);

/*** EOF ***/
