#include "B.h"
#include "A.h"

int main()
{
  B_t b;

  show_B(b);
  b.x = UNO;
  b.y = TRES;
  show_B(b);

  return 0;
}
