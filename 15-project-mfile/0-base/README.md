# Proyecto de ejemplo que contiene varios archivos fuente

## La idea es mostrar los errores y ver paso a paso como trabajar en un proyecto en "C"

En este caso la compilaci�n se realiza con un script `compile.sh`. Como se vera este no es el mecanismo m�s recomendado, pero se incluye como primer paso para ir avanzando en los conceptos y ver como se va mejorando.

En este caso tambi�n las inclusiones no se realizan de forma adecuada, por lo cual nos encontraremos con errores de declaraciones duplicadas.

Por ejemplo:

```console
# sh compile.sh
...
In file included from B.h:3:0,
                 from uBC.c:2:
A.h:8:15: error: redeclaration of enumerator 'UNO'
 typedef enum {UNO,DOS,TRES} A_t;
               ^
```

Las redefiniciones de pueden dar debido a que un .h incluye otro .h y este se vuelve a inclu�r en el archivo fuente del `main()`. Caso de `uB.c` o el caso de inclusi�n por dos caminos, caso de `uBC.c`

## Modo de uso

```console
sh compile.sh 
sh compile.sh compile
sh compile.sh clean

sh compile.sh 2>/dev/null
```
En el archivo `graph.png` se muestran las dependencias de inclusi�n. 
