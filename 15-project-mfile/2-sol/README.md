# Proyecto de ejemplo que contiene varios archivos fuente

## La idea es mostrar los errores y ver paso a paso como trabajar en un proyecto en "C"

En este caso la compilaci�n se realiza con un script `compile.sh`. Como se vera este no es el mecanismo m�s recomendado, pero se incluye como primer paso para ir avanzando en los conceptos y ver como se va mejorando.

En este caso las inclusiones se realizan con el pragma once para evitar las redefiniciones. Esta t�cnica no es t�n usada como la anterior, pues no es t�n portable entre compiladores. No es est�ndar pero esta bastante difundida la directiva.

Las redefiniciones de pueden dar debido a que un .h incluye otro .h y este se vuelve a inclu�r en el archivo fuente del `main()`. Caso de `uB.c` o el caso de inclusi�n por dos caminos, caso de `uBC.c`

## Modo de uso

```console
sh compile.sh 
sh compile.sh compile
sh compile.sh clean

sh compile.sh 2>/dev/null
```
En el archivo `graph.png` se muestran las dependencias de inclusi�n. 
