#include "C.h"
#include "B.h"

int main()
{
  C_t c;
  B_t b;

  init_C(&c); 
  show_C(c);
  set_C(&c,DOS,0);
  show_C(c);

  b.x = UNO;
  b.y = DOS;
  show_B(b);
  
  return 0;
}
