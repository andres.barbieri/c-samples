#!/bin/bash

CC=/usr/bin/gcc
RM=/bin/rm
AR=ar
LS=ls
MV=mv

FLAG=-Wall

EXE1=uA
EXE2=uB
EXE3=uC
EXE4=uBC

if [ $# -lt 1 ]
then
    OPT="compile"
else
    OPT=$1
fi

case "$OPT" in
        compile)
	$0 mklib

	echo $CC -Wall -o $EXE1 $EXE1.c A/A.c $FLAG -IA -Llib -lABC
	$CC -Wall -o $EXE1 $EXE1.c A/A.c $FLAG -IA -Llib -lABC

	echo $CC -Wall -o $EXE2 $EXE2.c B/B.c A/A.c $FLAG -IA -IB -Llib -lABC
	$CC -Wall -o $EXE2 $EXE2.c B/B.c A/A.c $FLAG -IA -IB -Llib -lABC

	echo $CC -Wall -o $EXE3 $EXE3.c C/C.c A/A.c $FLAG -A -IC -Llib -lABC
	$CC -Wall -o $EXE3 $EXE3.c C/C.c A/A.c $FLAG -IA -IC -Llib -lABC

	echo $CC -Wall -o $EXE4 $EXE4.c C/C.c B/B.c A/A.c $FLAG -IA -IB -IC -Llib -lABC
	$CC -Wall -o $EXE4 $EXE4.c C/C.c B/B.c A/A.c $FLAG -IA -IB -IC -Llib -lABC
        ;;

        mklib)
	$CC -c A/A.c
	$CC -c B/B.c -IA
	$CC -c C/C.c -IA
	$AR -cvq libABC.a A.o B.o C.o
	$MV libABC.a lib
	$LS lib
	;;

        clean)
	echo $RM -f $EXE1 $EXE2 $EXE3 $EXE4 core *.o lib/*.a
	$RM -f $EXE1 $EXE2 $EXE3 $EXE4 core *.o lib/*.a
	;;

        splash)
	$RM -f *~
	;;

        *)
            echo $"Usage: $0 {compile|clean|splash|mklib}"
            exit 1
esac


exit 0

### EOF ###
