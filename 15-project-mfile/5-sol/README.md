# Proyecto de ejemplo que contiene varios archivos fuente

## La idea es mostrar los errores y ver paso a paso como trabajar en un proyecto en "C"

En este caso la compilaci�n se realiza cambiando el script `compile.sh` por un `Makefile` que es m�s adecuado. El inconveniente que en esta soluci�n esta todo dentro de la misma carpeta y no se genera librer�a/biblioteca.

## Modo de uso

```console
make
make clean
```

