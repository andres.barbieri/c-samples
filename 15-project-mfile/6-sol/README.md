# Proyecto de ejemplo que contiene varios archivos fuente

## La idea es mostrar los errores y ver paso a paso como trabajar en un proyecto en "C"

En este caso la compilaci�n se realiza cambiando el script `compile.sh` por un `Makefile` que es m�s adecuado. A la soluci�n anterior se le agrega la separaci�n en carpetas y la generaci�n de librer�a/biblioteca de vinculaci�n est�tica y din�mica. 

## Modo de uso

```console
make
make clean
make cleanlib

make -f Makefile.shared
make -f Makefile.shared test
```

