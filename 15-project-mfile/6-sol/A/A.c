#include <stdio.h>
#include "A.h"

void show_A(const A_t elem)
{
  switch (elem)
   {
   case UNO: printf(UNO_S);
     break;
   case DOS: printf(DOS_S);
     break;
   case TRES: printf(TRES_S);
     break;
   default: printf(INV_S);
   }
}

