#!/bin/bash

cd A
rm *~
rm *.o a.out core
cd ../B
rm *~
rm *.o a.out core
cd ../C
rm *~
rm *.o a.out core
cd ..
rm uA uB uC uBC a.out core
ls A
ls B
ls C
