#!/bin/bash

gcc -fPIC -c A/A.c
gcc -fPIC -c B/B.c -IA
gcc -fPIC -c C/C.c -IA
gcc -shared -o lib/libABC.so A/A.o B/B.o C/C.o 
mv libABC.so lib
ls lib

