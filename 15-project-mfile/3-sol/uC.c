#include "C.h"
#include "A.h"

int main()
{
  C_t c;
  A_t a = UNO;

  init_C(&c); 
  show_C(c);
  set_C(&c,DOS,0);
  show_C(c);
  show_A(a);
  return 0;
}
