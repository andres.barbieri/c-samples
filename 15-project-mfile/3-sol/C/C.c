// C

#include "C.h"

void set_C(C_t *elem, A_t x, int ix)
{
  elem->c[ix] = x;
}

void init_C(C_t *elem)
{
  int i;
  for(i=0;i<MAX;i++)
    {
      elem->c[i]= UNO;
    }
}

void show_C(const C_t elem)
{
  int i;
  for(i=0;i<MAX;i++)
    {
      show_A(elem.c[i]);
    }
}

/*** EOF ***/
