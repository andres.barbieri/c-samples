# Proyecto de ejemplo que contiene varios archivos fuente

## La idea es mostrar los errores y ver paso a paso como trabajar en un proyecto en "C"

En este caso la compilaci�n se realiza con un script `compile.sh`. Como se vera este no es el mecanismo m�s recomendado, pero se incluye como primer paso para ir avanzando en los conceptos y ver como se va mejorando.

En este caso se resuelven las inclusiones con guardas y adem�s de dividen los fuentes en carpetas.

## Modo de uso

```console
sh compile.sh 
sh compile.sh compile
sh compile.sh clean

sh compile.sh 2>/dev/null
```
En el archivo `graph.png` se muestran las dependencias de inclusi�n. 
