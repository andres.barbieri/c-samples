// A

#ifndef _A_H
#define _A_H
#define UNO_S  "UNO"
#define DOS_S  "DOS"
#define TRES_S "TRES"
#define INV_S  "INVALIDO"

typedef enum {UNO,DOS,TRES} A_t;

void show_A(const A_t elem);
#endif

/*** EOF ***/
