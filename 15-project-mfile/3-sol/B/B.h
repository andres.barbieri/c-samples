// B

#ifndef _B_H
#define _B_H
#include <A.h>

typedef struct B {
  A_t x,y;
} B_t;
  
void show_B(const B_t elem);

#endif
/*** EOF ***/
