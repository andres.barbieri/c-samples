#!/bin/bash

CC=/usr/bin/gcc
RM=/bin/rm
FLAG=-Wall

EXE1=uA
EXE2=uB
EXE3=uC
EXE4=uBC

if [ $# -lt 1 ]
then
    OPT="compile"
else
    OPT=$1
fi

case "$OPT" in
        compile)
	echo $CC -Wall -o $EXE1 $EXE1.c A/A.c $FLAG -IA
	$CC -Wall -o $EXE1 $EXE1.c A/A.c $FLAG -IA

	echo $CC -Wall -o $EXE2 $EXE2.c B/B.c A/A.c $FLAG -IA -IB
	$CC -Wall -o $EXE2 $EXE2.c B/B.c A/A.c $FLAG -IA -IB

	echo $CC -Wall -o $EXE3 $EXE3.c C/C.c A/A.c $FLAG -A -IC
	$CC -Wall -o $EXE3 $EXE3.c C/C.c A/A.c $FLAG -IA -IC

	echo $CC -Wall -o $EXE4 $EXE4.c C/C.c B/B.c A/A.c $FLAG -IA -IB -IC 
	$CC -Wall -o $EXE4 $EXE4.c C/C.c B/B.c A/A.c $FLAG -IA -IB -IC 
        ;;

        clean)
	echo $RM -f $EXE1 $EXE2 $EXE3 $EXE4 core *.o
	$RM -f $EXE1 $EXE2 $EXE3 $EXE4 core *.o
	;;

        splash)
	$RM -f *~
	;;

        *)
            echo $"Usage: $0 {compile|clean|splash}"
            exit 1
esac


exit 0

### EOF ###
