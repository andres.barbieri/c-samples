# Proyecto de ejemplo para mostrar guardas de inclusión

La idea es mostrar su utilidad. Compliar comentando y descomentando desde header.h y ver su utilidad. Ver que los MACROS y prototipos no generar doble-inclusiones, pero si las definiciones de tipos enumerativos o struct, por ejemplo.

## Errores posibles

Sin guardas

```
gcc -c main.c

consoleIn file included from header2.h:4:0,
                 from main.c:2:
header.h:12:16: error: redefinition of ‘struct st’
 typedef struct st{                   // Problema de redefine
                ^~
In file included from main.c:1:0:
header.h:12:16: note: originally defined here
 typedef struct st{                   // Problema de redefine
                ^~
In file included from header2.h:4:0,
                 from main.c:2:
header.h:14:14: error: conflicting types for ‘struct_t’
   float b; } struct_t;
              ^~~~~~~~
In file included from main.c:1:0:
header.h:14:14: note: previous declaration of ‘struct_t’ was here
   float b; } struct_t;
              ^~~~~~~~
In file included from header2.h:4:0,
                 from main.c:2:
header.h:16:15: error: redeclaration of enumerator ‘UNO’
 typedef enum {UNO, DOS} enum_t;      // Problema de redefine
               ^~~
In file included from main.c:1:0:
header.h:16:15: note: previous definition of ‘UNO’ was here
 typedef enum {UNO, DOS} enum_t;      // Problema de redefine
               ^~~
In file included from header2.h:4:0,
                 from main.c:2:
header.h:16:20: error: redeclaration of enumerator ‘DOS’
 typedef enum {UNO, DOS} enum_t;      // Problema de redefine
                    ^~~
In file included from main.c:1:0:
header.h:16:20: note: previous definition of ‘DOS’ was here
 typedef enum {UNO, DOS} enum_t;      // Problema de redefine
                    ^~~
In file included from header2.h:4:0,
                 from main.c:2:
header.h:16:25: error: conflicting types for ‘enum_t’
 typedef enum {UNO, DOS} enum_t;      // Problema de redefine
                         ^~~~~~
In file included from main.c:1:0:
header.h:16:25: note: previous declaration of ‘enum_t’ was here
 typedef enum {UNO, DOS} enum_t;      // Problema de redefine
                         ^~~~~~
```

