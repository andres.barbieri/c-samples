//#ifndef HEADER_H
//#define HEADER_H 1

#define SOMETHING (1)

#define OTHER "other"

#define SOMETHINGELSE (2)            // No hay problema

typedef int intnew_t;                // No hay problema

typedef struct st{                   // Problema de redefine
  intnew_t a;                        // se soluciona con guarda  
  float b; } struct_t;

typedef enum {UNO, DOS} enum_t;      // Problema de redefine
                                     // se soluciona con guarda

//const intnew_t constintnew = 10;   // Problema de redefine
                                     // no se soluciona con guarda    

int x();                             // No hay problema 

int y();

int z();

//#endif /* header.h */
