#include <stdio.h>

// Notar la ausencia del ";" al final

//#define STR      "1234567890"; MAL

#define STR      "1234567890"
#define STR_LEN  10

int main()
{
  char str1[STR_LEN+1];
  //char *str2 = STR; 
  int i;

  // Depende de la correcta def. de las MACROS
  for(i=0;(i<STR_LEN);i++)
    {
      str1[i]  = STR[i];
      //str1[i]  = str2[i];
    }
  str1[i] = 0; // Ahora parece ser segura ?
  printf("%d %d %s\n",i,STR_LEN,str1);
  return 0;
} 
