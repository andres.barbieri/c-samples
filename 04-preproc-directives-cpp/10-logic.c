/*
 * Compilar con:
 *
 * gcc -o 10-logic 10-logic.c -std=c99 
 * gcc -o 10-logic 10-logic.c -Dudef_AND -std=c99  
 * gcc -o 10-logic 10-logic.c -Dudef_OR -std=c99  
 * gcc -o 10-logic 10-logic.c -Dudef_NNNNN -std=c99  
 */


#include <stdio.h>

#include "logic.h"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)


#define FMT "%d %s %d = %d\n"

#if  defined udef_AND
  #define OP AND
#elif defined udef_OR
  #define OP OR
#elif defined udef_XOR
  #define OP XOR
#elif defined udef_NOR
  #define OP NOR
#else
  #define OP NAND
#endif

int main()
{
  for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
	{
	  printf(FMT,i,TOSTRING(OP),j,OP(i,j));
	}
    }
  return 0;
}
