/*
 * Compilar con:
 *
 * gcc -o 08-logic 08-logic.c -std=c99 
 * gcc -o 08-logic 08-logic.c -DOP=AND -std=c99 
 * gcc -o 08-logic 08-logic.c -DOP=OR -std=c99 
 * gcc -o 08-logic 08-logic.c -DOP=NOEXISTE -std=c99 
 *
 * Ver los resultados
 */

#include <stdio.h>

#include "logic.h"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#ifndef OP
#define OP  NAND
#endif

#define FMT "%d %s %d = %d\n"

int main()
{
  for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
	{
	  printf(FMT,i,TOSTRING(OP),j,OP(i,j));
	}
    }
  return 0;
}
