// gcc -DTOP=20 04-macro-topn-plus.c -o 04-macro-topn-plus

#include <stdio.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#ifndef TOP
#define TOP 10
#endif

int main()
{
  int i;  
  int j = sizeof(TOSTRING(TOP));
  for(i=0;i<=TOP;i++)
    {
      printf("%*d\n",j,i);
    }
  return 0;
} 
