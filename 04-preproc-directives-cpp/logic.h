#ifndef uLOGIC_H
#define uLOGIC_H
/* 
 * Macros para funciones l'ogicas
 */

// Pueden dar efectos laterales (side-effects) estas definiciones ?

#define XOR(x,y) (((!x)&&(y))||((x)&&(!y)))
#define AND(x,y) ((x)&&(y))
#define OR(x,y) ((x)||(y))
#define NAND(x,y) (!((x)&&(y)))
#define NOR(x,y) (!((x)||(y)))

#endif /* logic.h */
