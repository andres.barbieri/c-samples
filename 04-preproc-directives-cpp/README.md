# Aprendiendo "C" Preprocessor

Ejemplos de programa que muestran ejemplos de uso del preprocesador de "C". 

## Introducci�n

El preprocesador trabaja de forma separada del complilador, no es parte de este, pero es necesario en una etapa previa a la compilaci�n. Es parte del est�ndar del lenguaje "C". El complilador lo invoca de forma autom�tica al inicio. Se lo puede ver como una herramienta de substituci�n de texto. Se lo referencia como CPP ("C" Preprocessor. Las directivas/comandos del preprocesador comienzan con el s�mbolo hash, n�meral "#".

### Pre-requisitos

Sistema UNIX compatible (??).
Compilador GCC, cpp, grep.

## Pruebas
 
Si bien el preprocesador tiene como objetivo trabajar con programas "C", "C++" y alg�n otro "dialecto", se lo puede utilizar con archivos de texto que reemplacen la sintaxis.

### Primeros Pasos

Se muestran ejemplos sobre archivos que no son de "C". La extensi�n .pcpp es simplemente de prueba. Se usan las directivas `#include`, `#define` y condicional con `#ifndef #endif`.
 
```console
$ cpp 00-includer1.pcpp
$ cpp 00-includer2.pcpp | grep nesty ## Problema de recursi�n "infinita"  
$ cpp 00-includer3.pcpp
```

Luego unos ejemplos cl�sicos con macros de constantes, ver el c�digo: `01-macros.c`, `02-macros.c` y `03-macros.c` comparar y ver los posibles problemas:

```console
$ make

$ ./01-macros
...
$ ./03-macros
```

Luego uso de -D y define condicionales con `04-macro-topn.c`. Ver versi�n `04-macro-topn-plus.c`

Para el c�digo `04-maxminmul.c`, `05-maxminmul.c` y `06-maxminmul.c` se muestran ejemplos de macros con operaciones y los efectos laterales que se pueden tener de acuerdo a su definici�n.

Para los c�digos `07-logic.c`, `08-logic.c`, `09-logic.c` y `10-logic.c`,  ver las directivas condicionales y definiciones entradas por el usuario a tiempo de compilaci�n (pre-procesamiento). Adem�s se muestra pasar un token/id de la macro a string con el operador "#".

```console
$ gcc -o 08-logic 08-logic.c -std=c99 
$ gcc -o 08-logic 08-logic.c -DOP=AND -std=c99 
$ gcc -o 08-logic 08-logic.c -DOP=OR -std=c99 
$ gcc -o 08-logic 08-logic.c -DOP=NOEXISTE -std=c99 

$ gcc -o 09-logic 09-logic.c -std=c99 
$ gcc -o 09-logic 09-logic.c -Dudef_AND -std=c99  
$ gcc -o 09-logic 09-logic.c -Dudef_OR -std=c99  
$ gcc -o 09-logic 09-logic.c -Dudef_NNNNN -std=c99  

$ gcc -o 10-logic 10-logic.c -std=c99 
$ gcc -o 10-logic 10-logic.c -Dudef_AND -std=c99  
$ gcc -o 10-logic 10-logic.c -Dudef_OR -std=c99  
$  gcc -o 10-logic 10-logic.c -Dudef_NNNNN -std=c99  
```
Para la definiciones de headers se puede ver ejemplos en los encabezados, headers:

* `header.h`
* `header_impl.h`
* `header_ok.h`

El primero no tiene las guardas de inclusi�n: "Include/Header/macro Guard", por lo que es propenso a problemas con inclusi�n m�ltiples. `header_impl.h` esta Ok, pero tiene el inconveniente que el formato de los tokens comienza con "_" y luego una may�sculas. Este formato o el formato de macros comenzando con doble "_", "__" est�n reservados para el uso de la implementaci�n y pueden generar conflictos. Esto es una convenci�n y no algo estricto del lenguaje. Los tokens/ids con "_" o con "__" son v�lidos como otros tokens.


Finalmente se ilustra el "Stringizing" del preprocesador con el c�digo `stringfication.c`.


