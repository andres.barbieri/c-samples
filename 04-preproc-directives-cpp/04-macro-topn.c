// gcc -DTOP=20 04-macro-topn.c -o 04-macro-topn

#include <stdio.h>

#ifndef TOP
#define TOP 10
#endif

int main()
{
  int i;  
  for(i=0;i<=TOP;i++)
    {
      printf("%02d\n",i);
    }
  return 0;
} 
