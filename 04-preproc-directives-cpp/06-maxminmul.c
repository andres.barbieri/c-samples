#include <stdio.h>

#define FIVE (5)
#define SIX  (6)


// computes either a or b twice, with bad results if the operand has side 
// effects. 

//#define MYMIN(a,b) (((a)<(b))?(a):(b))

// Avoid double evaluation, compound statment
#define MYMIN_INT(a,b) \
   ({ int (a) _a = (a); \
      int (b) _b = (b); \
     _a < _b ? _a : _b; })

#define MYMIN(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

//#define MYMAX(a,b) (((a)>(b))?(a):(b))

#define MYMAX(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

//#define MYMULT(x, y) x * y
//#define MYMULT(x, y) ((x)*(y))

#define MYMULT(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       ((_a)*(_b)); })


#define MYSUM(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
       ((_a)+(_b)); })

int main()
{
  int f = FIVE;
  int s = SIX;

  printf("MIN(%d,%d)=%d\n",FIVE,SIX,MYMIN(FIVE,SIX));
  //printf("MIN(%d,%d)=%d\n",FIVE,SIX,MYMIN(++FIVE,++SIX));
  printf("MIN(%d,%d)=%d\n",f,s,MYMIN(++f,++s));
  printf("2*MYSUM(%d,%d)=%d\n",f,s,2*MYSUM(f,s));
  printf("2*MYSUM(%d,%d)=%d\n",7,6,2*MYSUM(7,6));
  printf("2*MYSUM(%d,%d)=%d\n",7,6,2*(MYSUM(7,6)));
  return 0;
}

