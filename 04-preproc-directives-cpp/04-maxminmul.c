#include <stdio.h>

#define FIVE 5
#define SIX  6

// Notar la ausencia de ";" al final de la expresion
//#define MYMIN(a,b) (a<b?a:b);

#define MYMIN(a,b) (a<b?a:b)
                    
#define MYMAX(a,b) (a>b?a:b)

#define MYMULT(x, y) x * y

#define MYSUM(x, y) x + y

// Ver efectos laterles como la doble evaluaci'on o 
// incorrecta evaluaci'on
int main()
{
  int f = FIVE;
  int s = SIX;
  int tmpf, tmps;

  printf("MIN(%d,%d)=%d\n",FIVE,SIX,MYMIN(FIVE,SIX));
  // Operacion sobre literal/token
  // printf("MIN(%d,%d)=%d\n",FIVE,SIX,MYMIN(++FIVE,++SIX)); 
  //tmpf = f+1; tmps = s+1;
  //printf("MIN(%d,%d)=%d\n",tmpf,tmps,MYMIN(++f,++s));
  printf("MIN(%d,%d)=%d\n",f,s,MYMIN(++f,++s));
  printf("2*MYSUM(%d,%d)=%d\n",f,s,2*MYSUM(f,s));
  printf("2*MYSUM(%d,%d)=%d\n",7,6,2*MYSUM(7,6));
  printf("2*MYSUM(%d,%d)=%d\n",7,6,2*(MYSUM(7,6)));
  return 0;
}

