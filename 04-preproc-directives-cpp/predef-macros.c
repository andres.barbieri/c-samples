#include <stdio.h>

int main() {

   printf("File :%s\n", __FILE__ );  // archivo actual fuente
   printf("Date :%s\n", __DATE__ );  // dia
   printf("Time :%s\n", __TIME__ );  // horario
   printf("Line :%d\n", __LINE__ );  // linea del prog. actual
   printf("ANSI :%d\n", __STDC__ );  // 1 si std ansi
   return 0;
}
