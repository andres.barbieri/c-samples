#include <stdio.h>

// Notar la ausencia del ";" al final

//#define STR      "1234567890"; MAL

#define STR      "1234567890"
//#define STR_LEN  10
#define STR_SIZE  sizeof(STR)

int main()
{
  char str1[STR_SIZE];
  int i;

  // Depende de la correcta def. de las MACROS
  for(i=0;(i<STR_SIZE);i++)
    {
      /* string literal is of array type. 
         It is converted to a pointer to char in the expression 
         and is like an any pointer to char. */
      str1[i] = STR[i];
      /* is equivalent to
      {
        char *str2 = STR;
	str1[i]  = str2[i];
      }
      */
    }
  printf("%d %d %s\n",i, (int)STR_SIZE,str1);
  return 0;
} 
