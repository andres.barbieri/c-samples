// https://gcc.gnu.org/onlinedocs/cpp/Stringizing.html
/***
 * There is no way to convert a macro argument into a character constant.
 * If you want to stringize the result of expansion of a macro argument, 
 *  you have to use two levels of macros.
 *
 *
 * #define STRINGIFY(x) #x
 * #define TOSTRING(x) STRINGIFY(x)
 * #DEFINE ALGO 1
 *  
 * STRINGIFY(ALGO) -> "ALGO"
 *
 * TOSTRING(ALGO)  -> STRINGIFY(1)
 *    STRINGIFY(1) -> "1"
 *
 * x is stringized when it is used in STRINGIFY, so it is not macro-expanded first. 
 * But x is an ordinary argument to TOSTRING, so it is completely macro-expanded before TOSTRING itself is 
 * expanded (see Argument Prescan). Therefore, by the time STRINGIFY gets to its argument, it has already 
 * been macro-expanded.
 */

#include <stdio.h>

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define CONCAT(x,y) x##y
#define FIVE 5
#define TWO  2

int main()
{
  printf("%s\n",STRINGIFY(FIVE));
  printf("%s=%d\n",STRINGIFY(5+2),5+2);
  printf("%s=%d\n",STRINGIFY(FIVE+TWO),FIVE+TWO);
  printf("%s=%d\n",TOSTRING(FIVE+TWO),FIVE+TWO);
  printf("%s %d\n",TOSTRING(FIVE) "+" TOSTRING(TWO) "!=",CONCAT(5,2) );
  return (0);
}
