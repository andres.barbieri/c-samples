#include <stdio.h>

#include "logic.h"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)


#define OP  NAND
#define FMT "%d %s %d = %d\n"

int main()
{
  for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
	{
	  printf(FMT,i,TOSTRING(OP),j,OP(i,j));
	}
    }
  return 0;
}
