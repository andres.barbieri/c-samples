#include <stdio.h>
#include <stdlib.h>

#define SEED 35791248

void func(int x,int y,int *r)
{
  *r = x + y;
}

double pi_constant(void)
{
  int niter=10000000;
  double x,y;
  int i,count=0; /* # of points in the 1st quadrant of unit circle */
  double z;
  double pi;

  srand(SEED);
  count=0;
  for ( i=0; i<niter; i++) {
    x = (double)rand()/RAND_MAX;
    y = (double)rand()/RAND_MAX;
    z = x*x+y*y;
    if (z<=1) count++;
  }
    pi=(double)count/niter*4;
    return pi;
}

int main()
{
  int r = 0;
  func(10,20,&r);
  printf("r=%d\n",r);
  printf("%.10f\n", pi_constant());
  return 0;
}
