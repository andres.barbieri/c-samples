#include <stdio.h>

int func(int x, int y)
{
  return x+y;
}

//int eval(int x, int y, int (*f)(a,b))
//int eval(int x, int y, int (*f)())
int eval(int x, int y, int (*f)(int,int))
{
  return ((*f)(x,y));
}

int main()
{
  // Declaración - Declaration
  int           (*p)() = NULL;     // puntero a una funci'on

  // Asignación - Assignment (Direcci'on)

  // Pointers contents in Text
  p = func;
  printf("Code/Text: value of p=%p\n",(void*) p);
  //printf("Code/Text: value of *p=%x\n",*p);

  // Invoking
  printf("%d\n",(p(10,40)));
  printf("%d\n",eval(10,40,p));
  return 0;
} 
