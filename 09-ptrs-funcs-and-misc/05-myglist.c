/*
 * Ejemplo de una lista gen'erica "casera"
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef NULL
#define NULL (0)
#endif

#define MYGLIST_CPY_ELEM 1

typedef enum {TCHAR,TINT,TDOUBLE,TSTRING} typechar_t;

/***
#define TCHAR   'c'
#define TINT    'i'
#define TDOUBLE 'd'
#define TSTRING 's'
***/

void f_int(void *i, size_t s);

void f_char(void *c, size_t s);
  
void f_double(void *d, size_t s);

void f_string(void *s, size_t l);

void f_default(void *s, size_t l);

typedef struct node {
  void    *elem;
  size_t  size;
  //int     type_char;
  typechar_t type_char;
  struct node *next;
} node_t;

typedef node_t *list_t;

int add_front(list_t *l, void *e, size_t s, typechar_t c)
//int add_front(list_t *l, void *e, size_t s, int c)
{
  list_t aux;
  
  aux = *l;
  *l = malloc(sizeof(node_t));
  if (*l == NULL) 
    {
      return -1;
    }
  /*** Copy Data ***/
#ifdef MYGLIST_CPY_ELEM
  (*l)->elem = malloc(s);
  if ((*l)->elem == NULL)
    {
      free (*l);
      return -2;
    }
  memcpy((*l)->elem, e, s);
#else
  (*l)->elem = e;
#endif
  (*l)->type_char = c;
  (*l)->size      = s;  
  (*l)->next      = aux;
  return 0;
}

int delete_front(list_t *l)
{
  list_t aux;
  if (l != NULL)
    {
      aux = *l;
#ifdef MYGLIST_CPY_ELEM
      free((*l)->elem);
#endif
      (*l) = aux->next;
      free(aux);
      return 0;
    }
  return -1;
}

int delete_all(list_t *l)
{
  list_t aux;
  while (*l != NULL)
    { 
      aux = *l;
#ifdef MYGLIST_CPY_ELEM
      free((*l)->elem);
#endif
      *l = (*l)->next;
      free(aux);
    }
  return 0;
}

int iterate(const list_t l)
{
  list_t aux;
  aux = l;
  while (aux !=NULL)
    {
      switch (aux->type_char)
	{
	case TCHAR:
	  f_char(aux->elem, aux->size);
	  break;
	case TINT:
	  f_int(aux->elem, aux->size);
	  break;
	case TDOUBLE:
	  f_double(aux->elem, aux->size);
	  break;
	case TSTRING:
	  f_string(aux->elem, aux->size);
	  break;
	default:
	  f_default(aux->elem, aux->size);
	}
      aux = aux->next;
    }
  return 0;
}


void f_int(void *i, size_t s)
{
  printf("%d(%lu) ",*((int*)i), s);
}

void f_char(void *c, size_t s)
{
  printf("%c(%lu) ",*((char*)c), s);
}
  
void f_double(void *d, size_t s)
{
  printf("%lf(%lu) ",*((double*)d), s);
}
 
void f_string(void *s, size_t l)
{
  printf("%s(%lu) ",((char*)(s)), l);  
}

void f_default(void *s, size_t l)
{
  printf("an Object(%lu) ", l);  
}

int main()
{
  list_t l = NULL;
  int i;
  double d;
  char j;

  for(i=0;i<10;i++) add_front(&l,(void*) &i, sizeof(i), TINT);
  for(j='a';j<'j';j++) add_front(&l,(void*) &j, sizeof(j), TCHAR);
  add_front(&l,(void*) "HOLA", sizeof("HOLA"), TSTRING);
  add_front(&l,(void*) "QUE TAL", sizeof("QUE TAL"), TSTRING);
  for(d=0.0;d<3.0;d++) add_front(&l,(void*) &d, sizeof(d),TDOUBLE);
  add_front(&l,(void*) l, sizeof(l), 'l');
  iterate(l); printf("*\n");
  delete_front(&l);
  delete_front(&l);
  iterate(l); printf("*\n");
  delete_front(&l);
  delete_front(&l);
  delete_front(&l);
  iterate(l); printf("*\n");
  delete_front(&l);
  delete_all(&l);
  iterate(l); printf("*\n");
  return 0;
}
