/*
 * Ejemplo de una lista "casera"
 *
 * Test mylist::delete_front_wrong() with valgrind
 *
 * Usar delete_front_wrong() en lugar de delete_front() y ver que sucede
 * probar corriendo con valgrind. Comentar y descomentar
 * en el main. El Hint esta en el comentario de la func. delete_from_wrong()
 *
 * valgrind --leak-check=full ./04-mylist
 *
 */

#include <stdlib.h>
#include <stdio.h>

#ifndef NULL
#define NULL (0)
#endif

typedef struct node {
  int    elem;
  struct node *next;
} node_t;

typedef node_t *list_t;

int add_front(list_t *l, int e)
{
  list_t aux;
  
  aux = *l;
  *l = malloc(sizeof(node_t));
  //*l->elem = e; //error: request for member 'elem' in something not a structure or union
  (*l)->elem = e;
  (*l)->next = aux;
  return 0;
}

int add_back(list_t *l, int e)
{ 
  if (*l == NULL)
    {
      *l = malloc(sizeof(node_t));
      (*l)->elem = e;
      (*l)->next = NULL;
    }
  else
    {
      add_back( &((*l)->next), e);
    }
  return 0;
}

int element_front(list_t l, int *e)
{
 
  if (l != NULL)
    {
      *e = l->elem;
      return 0;
    }
  return -1;
}

int delete_front(list_t *l)
{
  list_t aux;
  if (l != NULL)
    {
      aux = *l;
      (*l) = aux->next;
      free(aux);
      return 0;
    }
  return -1;
} 

int delete_front_wrong(list_t *l)
{
  list_t aux;
  if (l != NULL)
    {
      aux = *l;
      free(*l);       // We have to dispose memory after reading
      *l = aux->next; // ==5525== Invalid read of size 8 - Maybe it works
      return 0;
    }
  return -1;
}

int delete_all(list_t *l)
{
  list_t aux;
  // Podemos encontrar habitualmente la siguiente expresi'on
  // en un c'odigo a la "C" ya que NULL==(0)
  // while (*l) {
  while (*l != NULL)
    { 
      aux = *l;
      *l = (*l)->next;
      free(aux);
    }
  *l = NULL;
  return 0;
}

int iterate(const list_t l, int func(int))
{
  list_t aux;
  aux = l;
  while (aux !=NULL)
    {
      aux->elem = (func(aux->elem));
      aux = aux->next;
    }
  return 0;
}


int print(int e)
{
  printf(" %d ",e);
  return e;
}

int inc(int e)
{
  return e+1;
}

int main()
{
  list_t l;
  int i;
  l = NULL;

  iterate(l,print);printf("*\n");
  for(i=0;i<10;i++) add_front(&l,i);
  add_back(&l,15);
  if (element_front(l,&i) == 0) printf("element = %d\n",i);
  iterate(l,print);printf("*\n");
  iterate(l,inc);
  iterate(l,print);printf("*\n");
  delete_front(&l); // or delete_front_wrong(&l);
  iterate(l,print);printf("*\n");
  delete_front(&l); //or delete_front_wrong(&l);
  iterate(l,print);printf("*\n");
  delete_all(&l);
  return 0;
}
