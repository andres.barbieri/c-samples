// valgrind --leak-check=full ./a.out
  
#include <stdio.h>
#include <stdlib.h>

struct node_t {
  int data;
  struct node_t *next;
};

struct node_dob_t {
  int data;
  struct node_t *next;
  struct node_t *prev;
};

typedef struct node_t *list_t;

typedef struct node_dob_t *list_dob_t;

typedef struct node3_t { int data; struct node3_t *next; } *list3_t;

int main()
{
  list_t l;
  struct node_t *l2;
  list3_t l3;

  l = NULL;

  l2 = l;

  l = malloc(sizeof(struct node_t));
  l->data = 10;
  l->next = NULL;
  
  l2 = l;
  printf("l2->data=%d, l2->next=%p\n",l2->data,l2->next);

  
  //*l2 = *l; // Must alloc memory first Invalid write of size 8
  //printf("l2->data=%d, l2->next=%p\n",l2->data,l2->next); (core)

  l2 = malloc(sizeof(struct node_t)); 
  *l2 = *l;
  printf("l2->data=%d, l2->next=%p\n",l2->data,l2->next);
  
  l3 = l; // warning: assignment to ‘list3_t’ {aka ‘struct node3_t *’} from incompatible pointer type ‘list_t’ {aka ‘struct node_t *’}

  
  //*l3 = *l;  // error: incompatible types when assigning to type ‘struct node3_t’ from type ‘struct node_t’
  // *l3 = ((struct node3_t)*l); // error: conversion to non-scalar type requested
  printf("%p %p %p %p\n",&(l3->data),&(l->data),&(l3->next),&(l->next));
  l3->data = l->data;  // A=A
  l3->next = l->next;  // B=B warning: assignment to ‘struct node3_t *’ from incompatible pointer type ‘struct node_t *
  l3->next = (struct node3_t*)l->next;

  printf("l3->data=%d, l3->next=%p\n",l3->data,l3->next); 
  free(l);free(l2);
  return 0;
}
  
