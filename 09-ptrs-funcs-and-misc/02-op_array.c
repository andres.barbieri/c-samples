#include <stdio.h>
#include <stdlib.h>


#define MIN(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_x>_y)?(_y):(_x)); })

#define ADD(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_y)+(_x)); })

#define MUL(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_y)*(_x)); })

#define AND(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       ((_y)&&(_x)); })

#define XOR(a,b) \
   ({ __typeof__ (a) _x = (a); \
       __typeof__ (b) _y = (b); \
       (((!_x)&&(_y))||((_x)&&(!_y))); })

int add(int x, int y)
{
  return x+y;
}

int mul(int x, int y)
{
  return x*y;
}

int and(int x, int y)
{
  return x&&y;
}

int min(int x, int y)
{
  return  ((x>y)?(y):(x));
}

int xor(int x, int y)
{
  return  XOR(x,y);
}

void show_a(int a[], int len_a)
{
  printf("[");
  for(int i=0;(i<len_a)&&(a!=NULL);i++)
    {
      printf("%6d ",a[i]);
    }   
  printf("]\n");
}

//int op_a(int a[],int len_a,int b[], int len_b, int **c, int *len_c, int (*f)(a,b))
//int op_a(int a[],int len_a,int b[], int len_b, int **c, int *len_c, int (*f)())
int op_a(int a[],int len_a,int b[], int len_b, int **c, int *len_c, int (*f)(int,int))
{
  *len_c=MIN(len_a,len_b);
  *c = (int*) malloc((*len_c)*sizeof(int));
  if ((*c) == NULL) return -1;
  for(int i=0;i<*len_c;i++)
   {
     (*c)[i]=((*f)(a[i],b[i]));
   }
  return 0;
}

int main()
{
  int a[] = {1,2,3,4,5,6};
  int b[] = {3,4,4,5,5,5,5,5,5,55,};
  int *c  = NULL;
  int lc;
  int (*f)() = NULL;

  //f = add; // OK 
  //f = min; // OK
  f = mul; // OK
  //f = and; // OK
  //f = xor; // OK
  //f = XOR; // WRONG 'XOR' undeclared (first use in this function)
  //f = NULL; // WRONG
  printf("    c=");show_a(c, (sizeof(c)/sizeof(int)));
  op_a(a, (sizeof(a)/sizeof(int)), 
        b, (sizeof(b)/sizeof(int)),
        &c, &lc, f);
  printf("    a=");show_a(a, (sizeof(a)/sizeof(int)));
  printf("    b=");show_a(b, (sizeof(b)/sizeof(int)));
  printf("---------------------------------------------------------------------------\n");
  printf("    c=");show_a(c,lc);
  free(c); c = NULL;
  //
  op_a(a, (sizeof(a)/sizeof(int)), 
       b, (sizeof(b)/sizeof(int)),
       &c, &lc, min);
  printf("min c=");show_a(c,lc); 
 free(c); c = NULL;
 //
  op_a(a, (sizeof(a)/sizeof(int)), 
       b, (sizeof(b)/sizeof(int)),
       &c, &lc, add);
  printf("add c=");show_a(c,lc); 
  free(c); c = NULL;
 //
  op_a(a, (sizeof(a)/sizeof(int)), 
       b, (sizeof(b)/sizeof(int)),
       &c, &lc, and);
  printf("and c=");show_a(c,lc); 
  free(c); c = NULL;
 //
  op_a(a, (sizeof(a)/sizeof(int)), 
       b, (sizeof(b)/sizeof(int)),
       &c, &lc, xor);
  printf("xor c=");show_a(c,lc); 
  free(c); c = NULL;
  return 0;
}
