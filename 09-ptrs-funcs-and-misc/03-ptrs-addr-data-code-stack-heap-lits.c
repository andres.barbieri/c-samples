#include <stdlib.h>
#include <stdio.h>


/*
 *  High
 *
 *  | 1st arg (param) | 
 *  | Stack Pointer   | 
 *  | Return Address  | 
 *
 *  Low
 */
 
static     int  *fp;

static     glb = 10; // warning: type defaults to 'int'
int glb2       = 20;


void func(int param1, int param2)
{
  static  int lcl = 0x10;
  int     b       = 0x0B;
  printf("Static Local:              %16p %10X\n", &lcl,lcl);
  printf("Stack  Local:              %16p %10X\n", &b,b);
  printf("Stack  Param1 Copy:        %16p %10X\n", &param1, param1);   
  printf("Stack  Param2 Copy:        %16p %10X\n", &param2, param2); 
  for(lcl=0;lcl<12;lcl++)
    {          
      printf("Func. Upper Stack          %16p %16p %10X\n",((void*)fp)-((void*)(&param2+lcl)), (&param2+lcl), *(&param2+lcl));  
    }
  //return 0;
}
int main(int argc, char *argv[])
{
  int  arr[1];
  const int x = 10;
  static int main_lcl = 10;
  char *a1   = "HOLA"; // is a string - literals allocated memory in read-only section.
  char *a2   = NULL;
 
  a2 = malloc(sizeof(char));
#ifdef _X86_32__
  __asm__("movl %%ebp, %[fp]" :  /* output */ [fp] "=r" (fp))
#else
  __asm__("mov %%rbp, %[fp]" :   /* output */ [fp] "=r" (fp));
#endif 
  printf("Main FP:                   %16p\n",(void*) fp);
  func(0x1, 0x2);
  printf("Code/Text:                 %16p func\n",func);
  printf("Literals:                  %16p %s\n",a1,a1);
  printf("Literals:                  %16p %s\n",&("HOLA"),"HOLA");
  printf("Literals:                  %16p %s\n",&("HOLO"),"HOLO");
  printf("Data/Globals:              %16p\n",&glb);
  printf("Data/Globals:              %16p\n",&glb2);
  printf("Data/Statics               %16p\n",&main_lcl);
  printf("Heap:                      %16p *a2\n",a2);
  printf("Stack:                     %16p arr\n",arr);
  printf("Constants:                 %16p &x\n",&x);
  printf("Argv:                      %16p\n",argv);
  printf("Inspecting Upper Stack\n");
  for(main_lcl=0;main_lcl<10;main_lcl++)
    {
      printf("Argv:                      %16p %s\n",&(argv[main_lcl]),argv[main_lcl]);
      fflush(stdout);
    }
  free(a2);
  return 0;
} 
