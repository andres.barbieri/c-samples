// https://blogs.oracle.com/ksplice/entry/the_ksplice_pointer_challenge
// 
// The Ksplice Pointer Challenge
// By wdaher on Oct 18, 2011
// 
// Back when Ksplice was just a research project at MIT, we all spent a 
// lot of time around the student computing group, SIPB. While there, 
// several precocious undergrads kept talking about how excited they were 
// to take 6.828, MIT's operating systems class.
// 
// "You really need to understand pointers for this class," we cautioned them. 
// "Reread K&R Chapter 5, again." Of course, they insisted that they understood 
// pointers and didn't need to. So we devised a test.
// 
// Ladies and gentlemen, I hereby do officially present the Ksplice Pointer 
// Challenge, to be answered without the use of a computer:
// 
// What does this program print?
// 
// This looks simple, but it captures a surprising amount of complexity. 
// Let's break it down.

#include <stdio.h>
int main() {
  int x[5];
  // Address(x)
  printf("%p\n", x);
  // Adress(x)+sizeof(base type) = Address(x) + sizeof(int)
  printf("%p\n", x+1);
  // Address(of a pointer) = Address(x)
  // x == &x An array is a pointer that lives in the pointer cell
  // x is a pointer that points to itself
  printf("%p\n", &x);
  // Pointer arithmetic tells us that &x+1 is going to be the address of x + sizeof(x). 
  // What's sizeof(x)? Well, it's an array of 5 ints. On this system, 
  // each int is 4 bytes, so it should be 20 bytes, or 0x14.
  // So Address(x)+0x14
  printf("%p\n", &x+1);

  printf("Displacement: %lu\n",  (unsigned long)(x+1) - (unsigned long)(x) );
  printf("Displacement: %lu\n",  (unsigned long) (&x+1) - (unsigned long) (&x));
  return 0;
}




