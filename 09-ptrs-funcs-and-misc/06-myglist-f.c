/*
 * Ejemplo de una lista gen'erica "casera"
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef NULL
#define NULL (0)
#endif

#define MYGLIST_CPY_ELEM 1

typedef struct node {
  void    *elem;
  size_t  size;
  void    (*f)(void *, size_t);
  struct node *next;
} node_t;

typedef node_t *list_t;

int add_front(list_t *l, void *e, size_t s, void (*f)(void *, size_t))
{
  list_t aux;
  
  aux = *l;
  *l = malloc(sizeof(node_t));
  if (*l == NULL) 
    {
      return -1;
    }
  /*** Copy Data ***/
#ifdef MYGLIST_CPY_ELEM
  (*l)->elem = malloc(s);
  if ((*l)->elem == NULL)
    {
      free (*l);
      return -2;
    }
  memcpy((*l)->elem, e, s);
#else
  (*l)->elem = e;
#endif
  (*l)->f    = f;
  (*l)->size = s;  
  (*l)->next = aux;
  return 0;
}

int delete_front(list_t *l)
{
  list_t aux;
  if (l != NULL)
    {
      aux = *l;
#ifdef MYGLIST_CPY_ELEM
      free((*l)->elem);
#endif
      (*l) = aux->next;
      free(aux);
      return 0;
    }
  return -1;
}

int delete_all(list_t *l)
{
  list_t aux;
  while (*l != NULL)
    { 
      aux = *l;
#ifdef MYGLIST_CPY_ELEM
      free((*l)->elem);
#endif
      *l = (*l)->next;
      free(aux);
    }
  return 0;
}

int iterate_f(const list_t l)
{
  list_t aux;
  aux = l;
  while (aux !=NULL)
    {
      aux->f((aux->elem),aux->size);
      aux = aux->next;
    }
  return 0;
}


void f_int(void *i, size_t s)
{
  printf("%d(%lu) ",*((int*)i), s);
}

void f_char(void *c, size_t s)
{
  printf("%c(%lu) ",*((char*)c), s);
}
  
void f_double(void *d, size_t s)
{
  printf("%lf(%lu) ",*((double*)d), s);
}
 
void f_string(void *s, size_t l)
{
  printf("%s(%lu) ",((char*)(s)), l);  
}


int main()
{
  list_t l = NULL;
  int i;
  double d;
  char j;

  for(i=0;i<10;i++) add_front(&l,(void*) &i, sizeof(i), f_int);
  for(j='a';j<'j';j++) add_front(&l,(void*) &j, sizeof(j), f_char);
  add_front(&l,(void*) "HOLA", sizeof("HOLA"), f_string);
  add_front(&l,(void*) "QUE TAL", sizeof("QUE TAL"), f_string);
  for(d=0.0;d<3.0;d++) add_front(&l,(void*) &d, sizeof(d),f_double);
  iterate_f(l); printf("*\n");
  delete_front(&l);
  delete_front(&l);
  iterate_f(l); printf("*\n");
  delete_front(&l);
  delete_front(&l);
  delete_front(&l);
  iterate_f(l); printf("*\n");
  delete_front(&l);
  delete_all(&l);
  iterate_f(l); printf("*\n");
  return 0;
}
