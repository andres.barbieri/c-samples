main()
{
    char *a1   = "HOLA"; // is a string- literals allocated memory in read-only section.
    char  a2[] = "HOLA"; // is a array char where memory will be allocated in stack.

    a1[0] = 'X'; //Not allowed. It is an undefined Behaviour. For me, it Seg Faults. 
    a2[0] = 'Y'; //Valid. 

    return 0;
} 
