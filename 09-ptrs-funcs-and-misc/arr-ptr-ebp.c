#include <stdio.h>

static int  *fp;

int main()
{
  int  arr[1];

#ifdef _X86_32__
  __asm__("movl %%ebp, %[fp]" :  /* output */ [fp] "=r" (fp))
#else
  __asm__("mov %%rbp, %[fp]" :  /* output */ [fp] "=r" (fp));
#endif 
  printf("%p %p %p %p \n",arr,&arr[0],&arr,(void*) fp);
  return 0;
}
