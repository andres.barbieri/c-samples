#include <stdio.h>

int main()
{
// Para de-referenciarlo debemos conocer su tipo

int val1 = 5;
int val2;
void *ptr;
 
 ptr = &val1; // Copy address of var

//val2 = *ptr; //error: void value not ignored as it ought to be
 val2 = *((int*) ptr);

 return 0;
}
