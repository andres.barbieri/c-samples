// https://blogs.oracle.com/ksplice/entry/the_ksplice_pointer_challenge
// 
// The Ksplice Pointer Challenge
// By wdaher on Oct 18, 2011
// 
// Back when Ksplice was just a research project at MIT, we all spent a 
// lot of time around the student computing group, SIPB. While there, 
// several precocious undergrads kept talking about how excited they were 
// to take 6.828, MIT's operating systems class.
// 
// "You really need to understand pointers for this class," we cautioned them. 
// "Reread K&R Chapter 5, again." Of course, they insisted that they understood 
// pointers and didn't need to. So we devised a test.
// 
// Ladies and gentlemen, I hereby do officially present the Ksplice Pointer 
// Challenge, to be answered without the use of a computer:
// 
// What does this program print?
// 
// This looks simple, but it captures a surprising amount of complexity. 
// Let's break it down.

#include <stdio.h>
int main() {
  int x[5];
  printf("%p\n", x);
  printf("%p\n", x+1);
  printf("%p\n", &x);
  printf("%p\n", &x+1);
  return 0;
}




