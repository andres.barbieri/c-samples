#include <stdio.h>

#define INT_MASK    "%d"
#define DOUBLE_MASK "%f"
#define CHAR_MASK   "%c"

int main()
{

  int    a = 10;
  double b = 12.34445;
  char   c = 'a';
  
  struct {
    void   *ptr;
    size_t len;
    char   *msk;
  } gen_ptr;

  gen_ptr.ptr = &a;
  gen_ptr.len = sizeof(a);
  gen_ptr.msk = INT_MASK;
  printf(gen_ptr.msk,*((int*)gen_ptr.ptr));
  printf("\n");

  gen_ptr.ptr = &b;
  gen_ptr.len = sizeof(b);
  gen_ptr.msk = DOUBLE_MASK;
  printf(gen_ptr.msk,*((double*)gen_ptr.ptr));
  printf("\n");

  gen_ptr.ptr = &c;
  gen_ptr.len = sizeof(c);
  gen_ptr.msk = CHAR_MASK;
  printf(gen_ptr.msk,*((char*)gen_ptr.ptr));
  printf("\n");

  return 0;
}
