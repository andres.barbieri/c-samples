#include <stdio.h>
#include <string.h>

int main()
{
  struct struct_name {
    // int i = 10; Not valid
    int  i;
    char c;
  } v1,v2,v3;

  struct struct_name v4;

  typedef struct {
    int  i;
    char c;
  } struct_name_t;

  struct_name_t v5,v6;
  struct_name_t v7 = { .c = 'j', .i = 100 };
  struct_name_t v8 = { .c = 'A' };

  typedef struct {
    union
      {
       int    i;
       double f;
      };
    char otro[2];
    char *name;
  } record_v_t;
 
  record_v_t v;

  typedef struct {
    int    uno;
    char   dos[20];
   double dd;
  } record_t;

  record_t x;
  record_t *y;  


  v1.i = 10;
  v1.c = 'a';

  // Built-in asssingment for compatible types
  v2 = v3 = v4 = v1;
  v5 = v6 = v7;
  // Incompatible types
  //v5 = v1; // incompatible types when assigning to type 'struct_name_t'
  //v5 = (struct_name_t) v1; error: conversion to non-scalar type requested

  // Incompatible types
  memcpy(&v5, &v1, sizeof(struct_name_t));
  v6.i = v1.i;
  v6.c = v1.c;

  // No built-in comparison
  // printf("v1 == v2 == %d\n",(v1 == v2)); invalid operands to binary ==
  // printf("v6 == v7 == %d\n",(v6 == v7)); invalid operands to binary ==
  printf("v1.i == v2.i == %d\n",(v1.i == v1.i));
  printf("v1.c == v2.c == %d\n",(v1.c == v1.c));
  printf("%d %c\n",v1.i,v1.c);
  printf("%d %c\n",v2.i,v2.c);  
  printf("%d %c\n",v3.i,v3.c);  
  printf("%d %c\n",v4.i,v4.c);  
  printf("%d %c\n",v5.i,v5.c);  
  printf("%d %c\n",v6.i,v6.c);  
  printf("%d %c\n",v7.i,v7.c);  
  printf("%d %c\n",v8.i,v8.c);  

  // Union access
  v.i = 20;
  v.f = 234.44;
  v.otro[0] = 'A';
  // Alloc and then
  //v.name[0] = v.otro[0];

  // Record access
  x.uno = 10;
  x.dos[3] = 'a';

  // Alloc and then ...
  // y->dd = 45.455; 
  return 0;   
}
