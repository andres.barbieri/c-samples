/***
 * The Lost Art of Structure Packing
 * http://www.catb.org/esr/structure-packing/
 *
 * It forces the generation of more expensive and slower code. Usually you can save as much memory ...
 * gcc -o union-struct union-struct.c -fpack-struct
 * gcc -o union-struct union-struct.c ## without -fpack-struct
 *
 ***/

#include <stdio.h>

int main()
{

  struct s_t {int  a; double b; };
  union  u_t { int a; double b; };

  typedef struct ss_t {int  a; double b; } st_t;
  typedef struct s_t stt_t;

  struct s_t s,t;
  union u_t  u;

  st_t       s2;
  stt_t      s3;
  
  s.a = 10; s.b = 20.0;
  t = s; // Copy between same types is allowed
  //s2 = s; //  error: incompatible types when assigning
  //s2 = (st_t) s; // error: conversion to non-scalar type requested
  s3 = s; // Copy between alias types is allowed
  s3.a++;
  u.a = 10; u.b = 20.0;

  printf("sizeof(s)=%2ld %2d %f \n",sizeof(s),s.a,s.b);
  printf("sizeof(u)=%2ld %2d %f \n",sizeof(u),u.a,u.b);
  u.a = 10; u.b = 20.10;
  printf("sizeof(u)=%2ld %2d %f \n",sizeof(u),u.a,u.b);
  printf("sizeof(int)=%2ld sizeof(double)=%2ld\n",sizeof(int),sizeof(double));  
  return 0;
}
