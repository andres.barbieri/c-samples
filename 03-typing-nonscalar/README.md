# Aprendiendo algo de tipos no escalares (non-scalar) en  "C"

Ejemplos de programas en "C" que muestran los tipos non-scalar, structs y unions 

## Pruebas
 
Aún no hay un `make`, si lo ubiera correr luego ejecutar los ejemplos:

```console
 make clean
 make
```
## Contenido individual:

* `00-union-struct.c`
* `01-struct.c`
